package creadores;

import colocables.edificios.Cobrable;
import colocables.unidades.Unidad;
import jugadores.Equipo;

public abstract class CreadorDeUnidades implements Cobrable {
    protected Equipo equipo;
    public abstract Unidad crearUnidad();

    public void asignarEquipo(Equipo equipo){
        this.equipo = equipo;
    }

}
