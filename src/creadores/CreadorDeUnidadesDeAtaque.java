package creadores;

import colocables.edificios.Cobrable;
import colocables.unidades.Unidad;

public abstract class CreadorDeUnidadesDeAtaque extends CreadorDeUnidades implements Cobrable {

    public abstract Unidad crearUnidad();

}
