package creadores;

import colocables.unidades.Cartera;
import colocables.unidades.Espadachin;
import colocables.unidades.NoHaySuficienteOroException;

public class CreadorDeEspadachines extends CreadorDeUnidadesDeAtaque {
    private int costo = 50;

    @Override
    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException {
        cartera.restarOro(costo);
    }


    @Override
    public Espadachin crearUnidad() {
        Espadachin unEspadachin = new Espadachin();
        unEspadachin.asignarEquipo(this.equipo);
        this.equipo.agregar(unEspadachin);
        return unEspadachin;
    }
}
