package creadores;


import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;

public class CreadorDeArmaDeAsedio extends CreadorDeUnidadesDeAtaque{
    private int costo = 200;


    @Override
    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException {
        cartera.restarOro(costo);
    }

    @Override
    public ArmaDeAsedio crearUnidad() {
        ArmaDeAsedio unArmaDeAsedio = new ArmaDeAsedio();
        unArmaDeAsedio.asignarEquipo(this.equipo);
        this.equipo.agregar(unArmaDeAsedio);
        return unArmaDeAsedio;
    }
}
