package creadores;

import colocables.unidades.Aldeano;
import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;

public class CreadorDeAldeanos extends CreadorDeUnidades {

    private int costo = 25;
    Cartera miCartera;

    public Aldeano crearUnidad() {
        Aldeano unAldeano = new Aldeano();
        unAldeano.conocerCartera(miCartera);
        unAldeano.asignarEquipo(this.equipo);
        this.equipo.agregar(unAldeano);
        return unAldeano;
    }

    @Override
    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException {
        cartera.restarOro(costo);
    }

    public void conocerCartera(Cartera cartera) {
        miCartera = cartera;
    }
}
