package creadores;

import colocables.unidades.Arquero;
import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;

public class CreadorDeArqueros extends CreadorDeUnidadesDeAtaque {
    private int costo = 75;

    @Override
    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException {
        cartera.restarOro(costo);
    }


    @Override
    public Arquero crearUnidad() {
        Arquero unArquero = new Arquero();
        unArquero.asignarEquipo(this.equipo);
        this.equipo.agregar(unArquero);
        return unArquero;
    }
}
