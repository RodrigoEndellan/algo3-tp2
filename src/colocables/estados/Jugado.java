package colocables.estados;

import colocables.unidades.Aldeano;

public class Jugado implements Estado {
    @Override
    public boolean validarQueSeaLibre() {
        return false;
    }

    @Override
    public Estado terminarTurno() {
        return new Libre();
    }

    @Override
    public boolean estoyActivo() {
        return true;
    }

    public Estado terminarTurno(Aldeano aldeano){
        aldeano.sumarOro(20);
        return new Libre();
    }

}
