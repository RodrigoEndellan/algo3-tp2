package colocables.estados.tiposDeMontura;

import colocables.unidades.MovimientoInvalidoException;



public class Montado implements TipoDeMontura {

    private int turnosMontado;

    @Override
    public void permitirAtaque() {
        if(turnosMontado>=1){
            return;
        }
        throw new MovimientoInvalidoException();
    }

    @Override
    public void permitirMovimiento() {
        throw new MovimientoInvalidoException();
    }

    @Override
    public void avanzarUnTurno() {
        this.turnosMontado++;
    }

    @Override
    public TipoDeMontura cambiarMontura(TipoDeMontura tipoDeMontura) {
        return tipoDeMontura.nuevaMontura();
    }

    @Override
    public TipoDeMontura nuevaMontura() {
        return new Desmontado();
    }

}
