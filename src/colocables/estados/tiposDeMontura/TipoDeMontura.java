package colocables.estados.tiposDeMontura;

public interface TipoDeMontura {


    void permitirAtaque();

    void permitirMovimiento();

    void avanzarUnTurno();

    TipoDeMontura cambiarMontura(TipoDeMontura tipoDeMontura);

    TipoDeMontura nuevaMontura();

}
