package colocables.estados.tiposDeMontura;


import colocables.unidades.MovimientoInvalidoException;



public class Desmontado implements TipoDeMontura {

    private int turnosMontado;


    //TODO tiene que tirar otra excepcion
    @Override
    public void permitirAtaque() {

        throw new MovimientoInvalidoException();
    }

    @Override
    public void permitirMovimiento() {
        if(turnosMontado>=1){
            return;
        }
        throw new MovimientoInvalidoException();
    }

    @Override
    public void avanzarUnTurno() {
        this.turnosMontado++;
    }

    @Override
    public TipoDeMontura cambiarMontura(TipoDeMontura tipoDeMontura) {
        return tipoDeMontura.nuevaMontura();
    }

    @Override
    public TipoDeMontura nuevaMontura() {
        return new Montado();
    }


}
