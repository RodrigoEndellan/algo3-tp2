package colocables.estados;

import colocables.unidades.Aldeano;
public class Libre implements Estado {

    @Override
    public Estado terminarTurno(){
        return this;
    }

    @Override
    public boolean estoyActivo() {
        return true;
    }

    public boolean validarQueSeaLibre() {
        return true;
    }

    public Estado terminarTurno(Aldeano aldeano){
        aldeano.sumarOro(20);
        return this;
    }

}
