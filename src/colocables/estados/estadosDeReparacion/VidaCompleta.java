package colocables.estados.estadosDeReparacion;

import colocables.edificios.Edificio;
import colocables.unidades.Aldeano;
import colocables.unidades.Unidad;

public class VidaCompleta extends EstadoReparacion {


    @Override
    public EstadoReparacion reparar(Aldeano aldeano) throws NoNecesitaReparacionExeption {
        throw new NoNecesitaReparacionExeption();
    }

    @Override
    public EstadoReparacion estasBajoAtaque(Edificio unEdificio) {
        return (new Daniado());
    }

    @Override
    public EstadoReparacion estasBajoAtaque(Unidad unaUnidad) {
        return (new Daniado());
    }
}
