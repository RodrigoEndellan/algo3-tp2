package colocables.estados.estadosDeReparacion;

import colocables.edificios.Edificio;
import colocables.unidades.Aldeano;
import colocables.unidades.Unidad;

public class Daniado extends EstadoReparacion {


    @Override
    public EstadoReparacion reparar(Aldeano aldeano) {
        return (new EnReparacion(aldeano));
    }

    @Override
    public EstadoReparacion estasBajoAtaque(Edificio unEdificio) {
        return this;
    }

    @Override
    public EstadoReparacion estasBajoAtaque(Unidad unaUnidad) {
        return this;
    }

}