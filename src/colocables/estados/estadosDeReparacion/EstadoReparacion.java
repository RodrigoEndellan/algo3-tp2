package colocables.estados.estadosDeReparacion;


import colocables.edificios.Edificio;
import colocables.estados.Construyendo;
import colocables.estados.Estado;
import colocables.estados.Jugado;
import colocables.estados.Libre;
import colocables.estados.Muerto;
import colocables.estados.Reparando;
import colocables.unidades.Aldeano;
import colocables.unidades.Unidad;

public abstract class EstadoReparacion {

    public abstract EstadoReparacion reparar(Aldeano aldeano) throws EstaSiendoReparadaPorUnAldeanoException , NoNecesitaReparacionExeption;

    public EstadoReparacion avisarSiMeMori(Estado estado){
        return this;
    }

    public EstadoReparacion avisarSiMeMori(Muerto estado){
        return this;
    }

    public EstadoReparacion avisarSiMeMori(Construyendo estado){
        return this;
    }

    public EstadoReparacion avisarSiMeMori(Libre estado){
        return this;
    }

    public EstadoReparacion avisarSiMeMori(Jugado estado){
        return this;
    }

    public EstadoReparacion avisarSiMeMori(Reparando estado){
        return this;
    }


    public abstract EstadoReparacion estasBajoAtaque(Edificio unEdificio);

    public abstract EstadoReparacion estasBajoAtaque(Unidad unaUnidad);
}
