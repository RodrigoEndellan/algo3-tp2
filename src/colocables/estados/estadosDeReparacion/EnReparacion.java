package colocables.estados.estadosDeReparacion;


import colocables.edificios.Edificio;
import colocables.estados.Muerto;
import colocables.unidades.Aldeano;
import colocables.unidades.Unidad;

public class EnReparacion extends EstadoReparacion {

    private Aldeano unidadQueRepara;

    public EnReparacion(Aldeano aldeano){
        unidadQueRepara = aldeano;
    }


    @Override
    public EstadoReparacion reparar(Aldeano aldeano) throws EstaSiendoReparadaPorUnAldeanoException {
        if(aldeano != unidadQueRepara){
       throw new EstaSiendoReparadaPorUnAldeanoException();
       }
       return this;
    }

    @Override
    public  EstadoReparacion avisarSiMeMori(Muerto estado){

        unidadQueRepara.estasLibreDestruyeronLoQueReparabas();

        return (new Daniado());
    }

    @Override
    public EstadoReparacion estasBajoAtaque(Edificio unEdificio) {
        return this;
    }

    @Override
    public EstadoReparacion estasBajoAtaque(Unidad unaUnidad) {
        return this;
    }
}
