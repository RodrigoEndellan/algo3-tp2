package colocables.estados;

import colocables.unidades.Aldeano;
public interface Estado {

    public boolean validarQueSeaLibre();
    
    public Estado terminarTurno();
    public Estado terminarTurno(Aldeano aldeano);

    public boolean estoyActivo();

}
