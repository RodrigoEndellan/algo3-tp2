package colocables.estados;

import colocables.unidades.Aldeano;

public class Muerto implements Estado{
    @Override
    public boolean validarQueSeaLibre() {
        return false;
    }

    @Override
    public Estado terminarTurno() {
        return this;
    }

    @Override
    public Estado terminarTurno(Aldeano aldeano) {
        return this.terminarTurno();
    }

    @Override
    public boolean estoyActivo() {
        return false;
    }

}

