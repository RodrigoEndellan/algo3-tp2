package colocables.estados;

import colocables.edificios.Edificio;
import colocables.unidades.Aldeano;

public class Construyendo implements Estado {

    private Edificio edificio;

    public Construyendo(Edificio edificioASetear) {
        edificio = edificioASetear;
    }

    public Estado terminarTurno(){
        return edificio.construir();
    }

    @Override
    public boolean estoyActivo() {
        return true;
    }

    public boolean validarQueSeaLibre() {
        return false;
    }

    public Estado terminarTurno(Aldeano aldeano){
        return this.terminarTurno();
    }

}
