package colocables.estados;

import colocables.edificios.Edificio;
import colocables.unidades.Aldeano;

public class Reparando implements Estado {

    private Edificio edificio;
    private Aldeano aldeano;

    public Reparando(Edificio edificioASetear , Aldeano aldeanoASetear) {
        edificio = edificioASetear;
        aldeano = aldeanoASetear;
    }


/* TODO: HABRIA QUE CONTEMPLAR MAS ADELANTE CON ALGUNA PRUEBA, EL CASO DE QUE EL ALDEANO QUE ESTA REPARANDO
   TODO: EL EDIFICIO MUERA. */

    public Estado terminarTurno(){
        return (edificio.aumentarVida(this));

    }

    @Override
    public boolean estoyActivo() {
        return true;
    }

    public boolean validarQueSeaLibre() {
        return false;
    }

    public Estado terminarTurno(Aldeano aldeano){

        return this.terminarTurno();
    }


}

