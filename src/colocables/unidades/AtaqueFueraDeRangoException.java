package colocables.unidades;

public class AtaqueFueraDeRangoException extends RuntimeException{
    public AtaqueFueraDeRangoException() {
        // TODO Auto-generated constructor stub
    }

    public AtaqueFueraDeRangoException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public AtaqueFueraDeRangoException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public AtaqueFueraDeRangoException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public AtaqueFueraDeRangoException(String message, Throwable cause, boolean enableSuppression,
                                       boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }
}
