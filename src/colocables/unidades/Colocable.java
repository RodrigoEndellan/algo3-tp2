package colocables.unidades;

import colocables.edificios.Edificio;
import geometria.Posiciones;
import mapa.Mapa;

public interface Colocable  {

	public Posiciones obtenerPosiciones();
	public void conocerMapa(Mapa mapa);
	public void quitarseDelMapa();
	public void darPosiciones(Posiciones posiciones);
	public void recibirDanioDeUn(Edificio unEdificio);
	public void recibirDanioDeUn(Unidad unaUnidad);
}
