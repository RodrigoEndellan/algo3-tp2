package colocables.unidades;

public class CreacionInvalidaException extends Exception {

	public CreacionInvalidaException() {
		// TODO Auto-generated constructor stub
	}

	public CreacionInvalidaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CreacionInvalidaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CreacionInvalidaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CreacionInvalidaException(String message, Throwable cause, boolean enableSuppression,
                                     boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
