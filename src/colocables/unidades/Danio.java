package colocables.unidades;

public class Danio {

    int danioUnidad;
    int danioEdificio;

    public Danio(){
        this.danioUnidad = 0;
        this.danioEdificio = 0;
    }

    public void setearDanioUnidad(int danioUnidad){
        this.danioUnidad = danioUnidad;
    }

    public void setearDanioEdificio(int danioEdificio){
        this.danioEdificio = danioEdificio;
    }

    public int obtenerDanioUnidad(){
        return danioUnidad;
    }

    public int obtenerDanioEdificio(){
        return danioEdificio;
    }
}
