package colocables.unidades;

public class MovimientoInvalidoException extends RuntimeException {

	public MovimientoInvalidoException() {
		// TODO Auto-generated constructor stub
	}

	public MovimientoInvalidoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MovimientoInvalidoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MovimientoInvalidoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MovimientoInvalidoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
