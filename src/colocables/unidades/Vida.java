package colocables.unidades;

import colocables.estados.Estado;
import colocables.estados.Libre;
import colocables.estados.Muerto;

public class Vida {

    private int vidaActual;
    private int vidaMaxima;
    private Estado estado;
    //private EstadoColocable estadoColocable;
    public Vida(int vida){
        this.vidaActual = vida;
        this.vidaMaxima = vida;
        this.estado = new Libre();

    }


    public int obtenerVidaActual() {
        return this.vidaActual;
    }

    public void disminuirEnPuntos( int danio) {
        this.vidaActual = this.vidaActual - danio;
        if(this.vidaActual<=0){
            this.estado = new Muerto();
        }
    }

    public void aumentarPuntosDeVida(int vida) {
        this.vidaActual = this.vidaActual + vida;
    }

    public int obtenerVidaMaxima(){
        return this.vidaMaxima;
    }

    public Estado actualizarEstado(Estado estado){

        if(this.estado.estoyActivo()){
            this.estado = estado;
        }
        return this.estado;
    }


}
