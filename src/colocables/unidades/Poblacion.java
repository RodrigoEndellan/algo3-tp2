package colocables.unidades;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Poblacion {
    int poblacionMaxima;

    //TODO por que tiene una posicion hardcodeada
    Set<Unidad> poblacion;

    public Poblacion(int poblacionMaxima) {
        this.poblacionMaxima = poblacionMaxima;
        this.poblacion = new HashSet<Unidad>();
    }

    public void agregar(Unidad unidad) {
        if (poblacion.size() == poblacionMaxima) {
            throw new PoblacionMaximaAlcanzadaException();
        }
        poblacion.add(unidad);
    }

    public boolean incluye(Unidad unidad){
        return poblacion.contains(unidad);
    }

    public int obtenerCantidad() {
        return poblacion.size();
    }

    public void remover(Unidad unidad) {
            poblacion.remove(unidad);
        unidad.quitarseDelMapa();
    }

    public void terminarTurno() {
        Iterator<Unidad> iter = poblacion.iterator();
        while (iter.hasNext()) {
            Unidad unidad = iter.next();
            unidad.terminarTurno();
        }
    }



    public int obtenerCantidadMax() {
        return poblacionMaxima;
    }
}
