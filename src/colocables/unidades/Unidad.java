package colocables.unidades;

import colocables.edificios.Edificio;
import colocables.estados.Jugado;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.PosicionInvalidaException;

/*
 * TODO Agregar supuesto.
 * Supuesto: Tamanio de unidad siempre deberia ser uno, por eso se llama unidad.
 */

public abstract class Unidad extends ColocableAtacable implements Movible {

	int costo;


	public void recibirDanioDeUn(Edificio unEdificio) {
		if(this.equipo.incluye(unEdificio)){
			throw new MovimientoInvalidoException();
		}
		recibirDanio(unEdificio.obtenerDanioAUnidad());
	}

	public void recibirDanioDeUn(Unidad unaUnidad) {
		if(this.equipo.incluye(unaUnidad)){
			throw new MovimientoInvalidoException();
		}
		this.recibirDanio(unaUnidad.obtenerDanioAUnidad());
	}

	private void recibirDanio(int danio){
		this.vida.disminuirEnPuntos(danio);
		this.estado = this.vida.actualizarEstado(this.estado);
		if(!estado.estoyActivo()){
			this.equipo.remover(this);
		}
	}

	/*
	 * PRE: Se le pasa una posicion valida que este a distancia x e y 
	 * menor a 1.
	 * POST: Mueve hacia esa nueva posicion.
	 */
	public void mover(Posicion nuevaPosicion) {
		
		if(!estado.validarQueSeaLibre()){
			throw new MovimientoInvalidoException();
		}
		
		if(!(this.posiciones.distancia( nuevaPosicion ) == 1)) {
			throw new MovimientoInvalidoException();
		}
		Posiciones posicionesNuevas = new Posiciones();
		posicionesNuevas.agregar(nuevaPosicion);
		try{
			this.mapa.cambiarPosicion(this, posicionesNuevas);
		}catch (PosicionInvalidaException e) {
			throw new MovimientoInvalidoException();
		}
		posiciones = posicionesNuevas;
		estado = new Jugado();
	}


}
