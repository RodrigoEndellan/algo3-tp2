package colocables.unidades;

import colocables.estados.Libre;

public class Arquero extends UnidadDeAtaque {


	
	public Arquero() {
		this.vida = new Vida(75);
		this.costo = 75;
		this.danio.setearDanioUnidad(15);
		this.danio.setearDanioEdificio(10);
		this.rango = 3;
		estado = new Libre();
	}

	@Override
	public void terminarTurno() {
		estado = new Libre();
	}

}
