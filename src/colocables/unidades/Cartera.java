package colocables.unidades;

public class Cartera {
    private int oro;

    public Cartera(){
        oro = 0;
    }

    public int obtenerOro() {
        return oro;
    }

    public void sumarOro(int oroParaSumar){
        oro += oroParaSumar;
    }

    public void restarOro(int oroParaRestar) throws NoHaySuficienteOroException{
        if (oro < oroParaRestar){
            throw new  NoHaySuficienteOroException();
        }
        oro -= oroParaRestar;
    }
    
    public void setOro(int oroASetear) {
    	this.oro = oroASetear;
    }
}
