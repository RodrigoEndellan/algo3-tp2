package colocables.unidades;

import geometria.Posicion;

public interface Movible {
	public void mover(Posicion posicion);
	/*public void moverArriba() throws MovimientoInvalidoException;
	public void moverAbajo() throws MovimientoInvalidoException;
	public void moverDerecha() throws MovimientoInvalidoException;
	public void moverIzquierda() throws MovimientoInvalidoException;
	public void moverArribaDerecha() throws MovimientoInvalidoException;
	public void moverArribaIzquierda() throws MovimientoInvalidoException;
	public void moverAbajoDerecha() throws MovimientoInvalidoException;
	public void moverAbajoIzquierda() throws MovimientoInvalidoException;*/
}
