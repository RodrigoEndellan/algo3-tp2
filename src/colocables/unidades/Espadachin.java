package colocables.unidades;

import colocables.estados.Libre;

public class Espadachin extends UnidadDeAtaque {


	public Espadachin() {
		this.vida = new Vida(100);
		this.costo = 50;
		this.rango = 1;
		this.danio.setearDanioUnidad(25);
		this.danio.setearDanioEdificio(15);
		this.estado = new Libre();
	}

	@Override
	public void terminarTurno() {
		estado = new Libre();
	}
}
