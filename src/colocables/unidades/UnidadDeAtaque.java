package colocables.unidades;

import colocables.estados.Jugado;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.Iterador;

public abstract class UnidadDeAtaque extends Unidad {


    int rango;

    public void atacar(Colocable colocable) {
        if(!estado.validarQueSeaLibre()){
            throw new MovimientoInvalidoException();
        }
        Posiciones posiciones = colocable.obtenerPosiciones();
        Iterador itPosiciones = posiciones.getIterator();
        while(itPosiciones.hasNext()){
            Posicion posicion = itPosiciones.obtenerActual();
            if(this.posiciones.distancia(posicion)<=this.rango){
                this.estado = new Jugado();
                colocable.recibirDanioDeUn(this);
                return;
            }
            itPosiciones.next();
        }
        throw new AtaqueFueraDeRangoException();
    }

}
