package colocables.unidades;

import colocables.estados.Jugado;
import colocables.estados.Libre;
import colocables.estados.tiposDeMontura.Desmontado;
import colocables.estados.tiposDeMontura.TipoDeMontura;
import geometria.Posicion;


public class ArmaDeAsedio extends UnidadDeAtaque {
	TipoDeMontura tipoDeMontura;
	public ArmaDeAsedio() {
		this.vida = new Vida(150);
		this.costo = 200;
		this.rango = 5;
		this.danio.setearDanioEdificio(75);
		this.tipoDeMontura = new Desmontado();
		this.tipoDeMontura.avanzarUnTurno();
		this.estado = new Libre();

	}

	@Override
	public void atacar(Colocable colocable){
		this.tipoDeMontura.permitirAtaque();
		super.atacar(colocable);
		this.tipoDeMontura.cambiarMontura(this.tipoDeMontura);
	}

	@Override
	public void mover(Posicion posicion){
		this.tipoDeMontura.permitirMovimiento();
		super.mover(posicion);
	}

	public void terminarTurno(){
		this.tipoDeMontura.avanzarUnTurno();
		this.estado = new Libre();
	}

	public void cambiarMontura() {
		 this.tipoDeMontura = this.tipoDeMontura.cambiarMontura(this.tipoDeMontura);
		 this.estado = new Jugado();
	}
}
