package colocables.unidades;

import colocables.edificios.Edificio;
import colocables.edificios.NoSeConstruyeEnMismaPosicionException;
import colocables.edificios.NoSeConstruyeEnPosicionAlejadaException;
import colocables.estados.Construyendo;
import colocables.estados.EstaOcupadoException;
import colocables.estados.Libre;
import colocables.estados.estadosDeReparacion.EstaFueraDeRango;

/* CLASE ALDEANO
 * 	-No ataca
 * 	-Puede construir y reparar
 */

import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import constructores.ConstructorDeEdificios;
import constructores.NoHayPosicionesValidasException;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.Iterador;


public class Aldeano extends Unidad{

	private Cartera cartera;

	public Aldeano() {
		this.vida = new Vida(50);
		costo = 25;
		estado = new Libre();
	}

	public void conocerCartera(Cartera carteraParaSetear) {
		cartera = carteraParaSetear;
	}

	private void validarPosicionDeConstruccion(Posicion posicionParaConstruir)
			throws NoSeConstruyeEnMismaPosicionException, NoSeConstruyeEnPosicionAlejadaException {
		int distancia = posiciones.distancia(posicionParaConstruir);
		if(distancia == 0) { throw new NoSeConstruyeEnMismaPosicionException();}
		if(distancia > 1) { throw new NoSeConstruyeEnPosicionAlejadaException();}
	}

	public void terminarTurno(){
		estado = estado.terminarTurno(this);
	}

    public Edificio construir(ConstructorDeEdificios constructor, Posicion posicion) 
    		throws 	EstaOcupadoException,
					NoSeConstruyeEnMismaPosicionException, 
					NoSeConstruyeEnPosicionAlejadaException {

		this.validarPosicionDeConstruccion(posicion);
		if(!estado.validarQueSeaLibre()){
			throw new EstaOcupadoException();
		}

		Edificio edificio = constructor.construir(posicion);
		while (true) {
			try {
				mapa.colocar(edificio, edificio.obtenerPosiciones());
				break;
			} catch (mapa.PosicionInvalidaException e) {
				try {
					constructor.reasignarPosicion(edificio);
				} catch (NoHayPosicionesValidasException e1) {
					throw new mapa.PosicionInvalidaException();
				}
			}
		}
		edificio.conocerMapa(mapa);
		estado = new Construyendo(edificio);
		return edificio;

	}

	// Aldeano recibe el mensaje de reparar un edificio
	// Precondicion: aldeano con el turno libre. edificio solo puede repararse una sola vez por turno y un aldeano a la vez
	// Postcondicion: se asigno la tarea al aldeano, para aplicarse terminado el turno
	public void reparar(Edificio edificio) throws EstaFueraDeRango, EstaSiendoReparadaPorUnAldeanoException, NoNecesitaReparacionExeption {

		Posiciones posiciones = edificio.obtenerPosiciones();
		Iterador itPosiciones = posiciones.getIterator();
		while(itPosiciones.hasNext()){
			Posicion posicion = itPosiciones.obtenerActual();
			if(this.posiciones.distancia(posicion)<= 1){
				try {
					this.estado = edificio.reparar(this);
					return;
				}
				catch (EstaSiendoReparadaPorUnAldeanoException e){
					throw new EstaSiendoReparadaPorUnAldeanoException();
				}
				catch (NoNecesitaReparacionExeption a){
					throw new NoNecesitaReparacionExeption();
				}
			}
			itPosiciones.next();
		}

		throw new EstaFueraDeRango();
	}

	public void estasLibreDestruyeronLoQueReparabas() {
		this.estado = new Libre();
	}

	public void sumarOro(int i) {
		this.cartera.sumarOro(i);
	}
}
