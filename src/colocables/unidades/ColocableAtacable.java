package colocables.unidades;

import colocables.estados.Estado;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

public abstract class ColocableAtacable implements Colocable {

    protected Posiciones posiciones;
    protected Mapa mapa;
    protected Vida vida;
    protected Danio danio;
    protected Estado estado;
    protected Equipo equipo;



    public int obtenerDanioAUnidad(){
        return danio.obtenerDanioUnidad();
    }

    public int obtenerDanioAEdificio(){
        return danio.obtenerDanioEdificio();
    }

    public ColocableAtacable(){

        this.danio = new Danio();
    }

    public int obtenerVidaActual() {
        return this.vida.obtenerVidaActual();
    }
    
    public int obtenerVidaMaxima(){
    	return this.vida.obtenerVidaMaxima();
    }

    public abstract void terminarTurno();

    public void conocerMapa(Mapa mapa) {
        this.mapa = mapa;
    }

    public void darPosiciones(Posiciones posicionesDadas) {
        this.posiciones = posicionesDadas;
    }

    public Posiciones obtenerPosiciones() {
        return this.posiciones;
    }

    public Estado obtenerEstado() {
        return this.estado;
    }

    public void asignarEquipo(Equipo equipo){
        this.equipo = equipo;
    }

    public void quitarseDelMapa(){
        this.mapa.remover(this);
    }
}
