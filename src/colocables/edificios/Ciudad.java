package colocables.edificios;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class Ciudad {

    Set<Edificio> edificios;

    public Ciudad(){

        this.edificios = new HashSet<Edificio>();
    }
    public void agregar(Edificio edificio) {
        edificios.add(edificio);
    }

    public boolean incluye(Edificio edificio) {
        return edificios.contains(edificio);
    }

    public void remover(Edificio edificio) {
        edificios.remove(edificio);
        edificio.quitarseDelMapa();

    }


    public void terminarTurno() {
        Iterator<Edificio> it = edificios.iterator();
        while (it.hasNext()) {
            Edificio edificioActual = it.next();
            edificioActual.terminarTurno();
        }
    }

}

