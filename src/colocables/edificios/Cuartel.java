package colocables.edificios;

import colocables.estados.Construyendo;
import colocables.estados.Estado;
import colocables.estados.Libre;
import colocables.estados.estadosDeReparacion.VidaCompleta;
import colocables.unidades.Vida;
import mapa.Mapa;

public class Cuartel extends Edificio {


    private int turnosParaConstruir;

    public Cuartel(){
        estado = new Construyendo(this);
        turnosParaConstruir = 3;
        this.vida = new Vida(250);
        estadoReparacion = new VidaCompleta();
    }

    @Override
    public void conocerMapa(Mapa mapa) {
        this.mapa = mapa;
    }

    public Estado construir() {
        turnosParaConstruir = turnosParaConstruir - 1;
        if (turnosParaConstruir == 0){
            estado = new Libre();
        }
        return estado;
    }

    @Override
    public Estado aumentarVida(Estado estadoActualDelAldeano){
        vida.aumentarPuntosDeVida(50);
        if (vida.obtenerVidaActual() >= vida.obtenerVidaMaxima()){
            estadoReparacion = new VidaCompleta();
            vida = new Vida(250);
            return (new Libre());
        }

        return estadoActualDelAldeano;
    }

}
