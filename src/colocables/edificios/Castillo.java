package colocables.edificios;

import java.util.HashMap;
import java.util.Map;

import colocables.estados.Estado;
import colocables.estados.Libre;
import colocables.estados.estadosDeReparacion.VidaCompleta;
import colocables.unidades.Colocable;
import colocables.unidades.MovimientoInvalidoException;
import colocables.unidades.Unidad;
import colocables.unidades.Vida;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.Iterador;
import mapa.PosicionInvalidaException;

public class Castillo extends Edificio {


    int rango;
    public Castillo(){
        this.vida = new Vida(1000);
        estadoReparacion = new VidaCompleta();
        estado = new Libre();
        this.danio.setearDanioEdificio(20);
        this.danio.setearDanioUnidad(20);
        this.rango = 3;

    }

    public void terminarTurno() {
        estado = new Libre();
        this.atacar();
    }

    @Override
    public Estado construir() {
        return null;
    }


    @Override
    public Estado aumentarVida(Estado estadoActualDeAldeano){
        vida.aumentarPuntosDeVida(15);

        if (vida.obtenerVidaActual() >= vida.obtenerVidaMaxima()){
            estadoReparacion = new VidaCompleta();
            vida = new Vida(1000);
            return (new Libre());
        }
        return estadoActualDeAldeano;
    }
 //TODO refactorizar TODO
    public void atacar() {
        Posiciones areaDeAtaque = this.posiciones.obtenerPosicionesDeArea(this.rango);
        HashMap<Colocable, Posicion> targets = obtenerTargets(areaDeAtaque);
        for (Map.Entry<Colocable, Posicion> target : targets.entrySet()) {
            this.atacar(target.getKey());
        }

    }

    private HashMap<Colocable,Posicion> obtenerTargets(Posiciones areaDeAtaque){

        HashMap<Colocable,Posicion> targets = new HashMap<>();
        Iterador iterador = areaDeAtaque.getIterator();
        while (iterador.hasNext()){
            Posicion posicionActual = iterador.obtenerActual();
            try {
                if(this.mapa.estaOcupado(posicionActual)) {
                    if(!targets.containsKey(this.mapa.obtener(posicionActual))){
                        targets.put(this.mapa.obtener(posicionActual),posicionActual);
                    }
                }
            }
            catch (PosicionInvalidaException e){

            }
            iterador.next();
        }
        return targets;
    }


    public void atacar(Colocable colocable) {
        Posiciones posiciones = colocable.obtenerPosiciones();
        Iterador itPosiciones = posiciones.getIterator();
        while(itPosiciones.hasNext()){
            Posicion posicion = itPosiciones.obtenerActual();
            if(this.posiciones.distancia(posicion)<=this.rango){
                try{

                	colocable.recibirDanioDeUn(this);
                }catch(MovimientoInvalidoException e){                	
                }
                return;
            }
            itPosiciones.next();
        }
    }

    @Override
    public void recibirDanioDeUn(Unidad unidad) {
    	super.recibirDanioDeUn(unidad);
    	if(!this.estado.estoyActivo()) {
    		throw new JuegoTerminadoException();
    	}
    }
}

