package colocables.edificios;

import colocables.estados.EstaOcupadoException;
import colocables.estados.Estado;
import colocables.estados.Jugado;
import colocables.estados.Reparando;
import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.EstadoReparacion;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import colocables.unidades.Aldeano;
import colocables.unidades.ColocableAtacable;
import colocables.unidades.MovimientoInvalidoException;
import colocables.unidades.Unidad;
import creadores.CreadorDeUnidades;
import geometria.Posicionador;

public abstract class Edificio extends ColocableAtacable {

    int tamanio;
    public EstadoReparacion estadoReparacion;

    public abstract Estado construir();
    public abstract Estado aumentarVida(Estado estadoActualDelAldeano);

    public void recibirDanioDeUn(Edificio unEdificio){
        if(this.equipo.incluye(unEdificio)){
            throw new MovimientoInvalidoException();
        }
        recibirDanio(unEdificio.obtenerDanioAEdificio());
        this.estadoReparacion = estadoReparacion.estasBajoAtaque(unEdificio);
    }

    public void recibirDanioDeUn(Unidad unaUnidad){
        if(this.equipo.incluye(unaUnidad)){
            throw new MovimientoInvalidoException();
        }
        recibirDanio(unaUnidad.obtenerDanioAEdificio());
        this.estadoReparacion = estadoReparacion.estasBajoAtaque(unaUnidad);

    }

    private void recibirDanio(int danio){
        this.vida.disminuirEnPuntos(danio);
        this.estado = this.vida.actualizarEstado(this.estado);
        if(!this.estado.estoyActivo()) {
            this.equipo.remover(this);
        }
        this.estadoReparacion = this.estadoReparacion.avisarSiMeMori(estado);
    }


    public Unidad crearUnidadCon(CreadorDeUnidades creador) throws EstaOcupadoException {
        if(!estado.validarQueSeaLibre()){
            throw new EstaOcupadoException();
        }
        Unidad unaUnidad = creador.crearUnidad();
        Posicionador.posicionar(this.mapa,this.posiciones, unaUnidad);
        estado = new Jugado();
        return unaUnidad;
    }

    public void terminarTurno() {
        estado = estado.terminarTurno();
    }

     // recibe el mensaje reparar por un aldeano:
     // - si esta con la vida maxima no se repara
     // - si esta daniado se repara
     // - si ya lo esta reparando un aldeano, devuelve libre, para que el aldeano pueda hacer otra accion
    public Estado reparar(Aldeano aldeano) throws EstaSiendoReparadaPorUnAldeanoException, NoNecesitaReparacionExeption {

        try {estadoReparacion = estadoReparacion.reparar(aldeano);
        } catch (EstaSiendoReparadaPorUnAldeanoException e ){
            throw new EstaSiendoReparadaPorUnAldeanoException();
        }
        catch (NoNecesitaReparacionExeption e){
            throw new NoNecesitaReparacionExeption();
        }


        return (new Reparando(this, aldeano) );
    }

}
