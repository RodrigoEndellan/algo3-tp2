package colocables.edificios;

import colocables.estados.Construyendo;
import colocables.estados.Estado;
import colocables.estados.Libre;
import colocables.estados.estadosDeReparacion.VidaCompleta;
import colocables.unidades.Vida;

public class PlazaCentral extends Edificio {

    private int turnosParaConstruir;


    public PlazaCentral() {
        tamanio = 4;
        estado = new Construyendo(this);
        turnosParaConstruir = 3;
        estadoReparacion = new VidaCompleta();
        this.vida = new Vida(450);
    }

    public Estado construir() {
        turnosParaConstruir = turnosParaConstruir - 1;
        if (turnosParaConstruir == 0) {
            estado = new Libre();
        }
        return estado;
    }

    @Override
    public Estado aumentarVida(Estado estadoActualDelAldeano) {
        vida.aumentarPuntosDeVida(25);
        if (vida.obtenerVidaActual() >= vida.obtenerVidaMaxima()){
            estadoReparacion = new VidaCompleta();
            vida = new Vida(450);
            return (new Libre());

        }
        return estadoActualDelAldeano;
    }

}

