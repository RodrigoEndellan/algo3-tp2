package colocables.edificios;

import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;

public interface Cobrable {

    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException;
}
