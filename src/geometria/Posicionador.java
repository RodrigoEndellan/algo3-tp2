package geometria;

import colocables.unidades.Colocable;
import colocables.unidades.MovimientoInvalidoException;
import mapa.Iterador;
import mapa.Mapa;
import mapa.PosicionInvalidaException;

public class Posicionador {
    public static boolean
    posicionar(Mapa mapa, Posicion posicionOcupada, Colocable unaUnidad) {
        int x = posicionOcupada.x() - 1;

        while(x<=posicionOcupada.x()+1){
            int y = posicionOcupada.y() - 1;
            //if(!mapa.estaOcupado(new Posicion(x,y))) {
            try {
                Posiciones _posiciones = new Posiciones();
                _posiciones.agregar(new Posicion(x, y));
                mapa.colocar(unaUnidad, _posiciones);
                return true;
                }
                catch (PosicionInvalidaException e){}
            while(y<=posicionOcupada.y()+1){
                //if(!mapa.estaOcupado(new Posicion(x,y))){
                try {
                    Posiciones _posiciones = new Posiciones();
                    _posiciones.agregar(new Posicion(x, y));
                    mapa.colocar(unaUnidad, _posiciones);
                    return true;
                    }catch (PosicionInvalidaException e){}
                y++;
            }
            x++;
        }
        return false;
    }

    public static void
    posicionar(Mapa mapa, Posiciones posiciones, Colocable unaUnidad) {
        Iterador iteradorPosiciones = posiciones.getIterator();
        while(iteradorPosiciones.hasNext()){
            Posicion unaPosicion = iteradorPosiciones.obtenerActual();
            if(Posicionador.posicionar(mapa,unaPosicion,unaUnidad)){
                return;
            }
            iteradorPosiciones.next();
        }
        throw  new MovimientoInvalidoException();
    }
}
