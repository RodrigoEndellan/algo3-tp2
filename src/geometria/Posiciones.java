package geometria;


import java.util.ArrayList;

import mapa.Iterador;

public class Posiciones {
    ArrayList<Posicion> posiciones = new ArrayList<Posicion>();;


    public Posiciones(){
    }

    public Iterador getIterator(){
        return new posicionesIterator();
    }

    public void agregar(Posicion posicion){
        this.posiciones.add(posicion);
    }

    public boolean contiene(Posicion posicion){
        return this.posiciones.contains(posicion);
    }

    /*
     * PRE: Se le pasa una posicion valida
     * POST: Calcula la menor distancia entre un conjunto de posiciones y
     * una posicion determinada.
     */
    public int distancia(Posicion posicion) {
        int distancia = (this.posiciones.get(0)).distancia(posicion);
        for(int i = 1; i < this.posiciones.size(); i++){
            int aux = (this.posiciones.get(i)).distancia(posicion);
            if (aux < distancia){
                distancia = aux;
            }
        }
        return distancia;
    }

    
    public int distancia(Posiciones posicionesADistancia) {
        
    	Iterador iter = posicionesADistancia.getIterator();
    	Posicion posicion = iter.obtenerActual();
    	int distancia = this.distancia(posicion);
    	iter.next();
    	while( iter.hasNext() ){
        	posicion = iter.obtenerActual();
            int aux = this.distancia(posicion);
            if (aux < distancia) {
                distancia = aux;
            }
            
            iter.next();
        }
        return distancia;
    }
    public Posiciones obtenerPosicionesDeArea(int rango) {
        Iterador it = this.getIterator();
        Posiciones Area = new Posiciones();
        int rangoArea = rango*rango;
        while (it.hasNext()){
            Posicion posicionActual = it.obtenerActual();
            int x = posicionActual.x() - rango;
            int y = posicionActual.y() - rango;
            while(posicionActual.distancia(new Posicion(x,y))<rangoArea-1){
                Area.agregar(new Posicion(x,y));
                while (posicionActual.distancia(new Posicion(x,y))<rangoArea-1){
                    Area.agregar(new Posicion(x,y));
                    y++;
                }
                x++;
                y = posicionActual.y() - rango;
            }
            it.next();
        }
        return Area;
    }

    private class posicionesIterator implements Iterador{

        int index;
        @Override
        public boolean hasNext() {
            if(index < posiciones.size()){
                return true;
            }
            return false;
        }

        @Override
        public int next() {
            if(this.hasNext()){
                return this.index++;
            }
            return -1;
        }

        public Posicion obtenerActual(){
            return posiciones.get(index);
        }
    }

	public Posicion esquinaInferiorIzquierda(){
        Posicion resultado = posiciones.get(0);
        for(int i = 1; i < posiciones.size(); i++){
            Posicion pos = posiciones.get(i);
            if(resultado.y() < pos.y()){ continue;}
            if(resultado.x() < pos.x()){ continue;}
            resultado = pos;
        }
        return resultado;
    }

    public Posicion esquinaSuperiorDerecha(){
        Posicion resultado = posiciones.get(0);
        for(int i = 1; i < posiciones.size(); i++){
            Posicion pos = posiciones.get(i);
            if(resultado.y() > pos.y()){ continue;}
            if(resultado.x > pos.x()){ continue;}
            resultado = pos;
        }
        return resultado;
    }

	public Posicion obtener(int i) {
		return this.posiciones.get(i);
	}
}