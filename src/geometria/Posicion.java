package geometria;

public class Posicion {
	int x;
	int y;
	
	public Posicion(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int x() {
		return this.x;
	}
	
	public int y() {
		return this.y;
	}
	
	
	/*
	 * TODO Buscar por que funciona esto
	 * No tengo idea de por que funciona esto, se ve feo.
	 */
	@Override
	public boolean equals(Object objeto) {
		Posicion posicionAComparar = (Posicion) objeto;
		return ((this.x == posicionAComparar.x()) && (this.y == posicionAComparar.y()));
	}

    public int distancia(Posicion posicionAConstruir) {
		int distanciaX = this.x - posicionAConstruir.x();
		int distanciaY = this.y - posicionAConstruir.y();
		if( distanciaX < 0) { distanciaX = distanciaX * -1 ;}
		if( distanciaY < 0) { distanciaY = distanciaY * -1 ;}
		if( distanciaX > distanciaY) return distanciaX;
		return distanciaY;
	}
}
