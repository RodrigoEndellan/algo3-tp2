package constructores;

import colocables.edificios.Castillo;
import colocables.edificios.Edificio;
import colocables.unidades.Cartera;
import geometria.Posicion;

public class ConstructorDeCastillo extends ConstructorDeEdificios {

    int[] numeros = {-3, -2, -1, 0, 1, 2, 3};

    public ConstructorDeCastillo(){
    }

    @Override
    public Castillo construir(Posicion posicion) {
        Castillo castillo = new Castillo();
        super.posicionesDesdeLaReferencia(posicion, numeros);
        try {
            reasignarPosicion(castillo);
        } catch (NoHayPosicionesValidasException e) {
            throw new mapa.PosicionInvalidaException();
        }
        this.equipo.agregar(castillo);
        castillo.asignarEquipo(this.equipo);
        return castillo;
    }



    @Override
    public void reasignarPosicion(Edificio castillo) throws NoHayPosicionesValidasException {
       super.reasignarPosicion(castillo, numeros);
    }

    @Override
    public void cobrar(Cartera cartera) {
    }

}
