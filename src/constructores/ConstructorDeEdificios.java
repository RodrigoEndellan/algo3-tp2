package constructores;

import java.util.ArrayList;

import colocables.edificios.Ciudad;
import colocables.edificios.Cobrable;
import colocables.edificios.Edificio;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;

public abstract class   ConstructorDeEdificios implements Cobrable {
    protected Equipo equipo;

    public abstract Edificio construir(Posicion posicion);
    public Ciudad ciudad;
    protected int indicePosicionDada = 0;
    protected ArrayList<Posiciones> posicionesPosibles;

    public void asignarEquipo(Equipo miEquipo){
        equipo = miEquipo;
    }


    protected void posicionesDesdeLaReferencia(Posicion posicion, int[] numeros){
        int inicioJ = 0;
        int inicioK = 0;
        int medio = (numeros.length - 1) / 2;
        posicionesPosibles = new ArrayList<Posiciones>();
        for(int i = 0; i < 4 ; i++) {
            Posiciones posiciones = new Posiciones();
            for (int j = inicioJ; j < inicioJ + ((numeros.length + 1) /2); j++) {
                for (int k = inicioK; k < inicioK + ((numeros.length +1)/2); k++) {
                    int x = posicion.x() + numeros[j];
                    int y = posicion.y() + numeros[k];
                    posiciones.agregar(new Posicion(x, y));
                }
            }
            inicioJ = (inicioJ + medio) % (medio * 2);
            inicioK = (inicioK + inicioJ) % (medio * 2);
            posicionesPosibles.add(posiciones);
        }
    }

    protected void reasignarPosicion(Edificio edificio, int[] numeros) throws NoHayPosicionesValidasException {
        if(indicePosicionDada == 4){
            throw new NoHayPosicionesValidasException();
        }
        Posiciones pos =  posicionesPosibles.get(indicePosicionDada);
        indicePosicionDada += 1;
        edificio.darPosiciones(pos);
    }

    public abstract void reasignarPosicion(Edificio edificio) throws NoHayPosicionesValidasException;
}
