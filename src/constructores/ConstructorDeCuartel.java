package constructores;

import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;
import geometria.Posicion;

public class ConstructorDeCuartel extends ConstructorDeEdificios {

    int[] numeros = {-1, 0, 1};
    private int costo = 50;

    @Override
    public Cuartel construir(Posicion posicion) {
        Cuartel cuartel = new Cuartel();
        posicionesDesdeLaReferencia(posicion, numeros);
        try {
            reasignarPosicion(cuartel);
        } catch (NoHayPosicionesValidasException e) {
            throw new mapa.PosicionInvalidaException();
        }
        this.equipo.agregar(cuartel);
        cuartel.asignarEquipo(equipo);

        return cuartel;
    }

    @Override
    public void reasignarPosicion(Edificio cuartel) throws NoHayPosicionesValidasException {
        super.reasignarPosicion(cuartel, numeros);
    }

    @Override
    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException {
        cartera.restarOro(costo);
    }
}
