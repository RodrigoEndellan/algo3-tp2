package constructores;

import colocables.edificios.Edificio;
import colocables.edificios.PlazaCentral;
import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;
import geometria.Posicion;

public class ConstructorDePlazaCentral extends ConstructorDeEdificios {

    private int costo = 100;
    int[] numeros = {-1, 0, 1};

    public PlazaCentral construir(Posicion posicion){
        PlazaCentral plazaCentral = new PlazaCentral();
        posicionesDesdeLaReferencia(posicion, numeros);
        try {
            reasignarPosicion(plazaCentral);
        } catch (NoHayPosicionesValidasException e) {
            throw new mapa.PosicionInvalidaException();
        }
        this.equipo.agregar(plazaCentral);
        plazaCentral.asignarEquipo(this.equipo);
        return plazaCentral;
    }

    @Override
    public void reasignarPosicion(Edificio plaza) throws NoHayPosicionesValidasException {
        super.reasignarPosicion(plaza, numeros);
    }

    @Override
    public void cobrar(Cartera cartera) throws NoHaySuficienteOroException {
        cartera.restarOro(costo);
    }
}
