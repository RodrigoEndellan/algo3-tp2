package jugadores;

import colocables.edificios.Ciudad;
import colocables.edificios.Edificio;
import colocables.unidades.Poblacion;
import colocables.unidades.Unidad;

public class Equipo  {
    Poblacion poblacion;
    Ciudad ciudad;


    public void agregarPoblacion(Poblacion poblacion){
        this.poblacion = poblacion;
    }
    public void agregarCiudad(Ciudad ciudad){
        this.ciudad = ciudad;
    }

    public void agregar(Unidad unidad) {
        this.poblacion.agregar(unidad);
    }

    public boolean incluye(Unidad unaUnidad) {
        return this.poblacion.incluye(unaUnidad);
    }

    public boolean incluye(Edificio unEdificio) {
        return ciudad.incluye(unEdificio);
    }

    public void agregar(Edificio unEdificio) { this.ciudad.agregar(unEdificio);
    }

    public void remover(Edificio unEdificio) {
        this.ciudad.remover(unEdificio);
    }

    public void remover(Unidad unidad) {
        this.poblacion.remover(unidad);
    }

    public void terminarTurno() {

        this.ciudad.terminarTurno();
        this.poblacion.terminarTurno();
    }

    public int obtenerCantidadMaxPoblacion(){

        return this.poblacion.obtenerCantidadMax();
    }

}

