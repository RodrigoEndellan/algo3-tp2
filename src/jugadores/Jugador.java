package jugadores;

import colocables.edificios.Castillo;
import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.edificios.NoSeConstruyeEnMismaPosicionException;
import colocables.edificios.NoSeConstruyeEnPosicionAlejadaException;
import colocables.edificios.PlazaCentral;
import colocables.estados.EstaOcupadoException;
import colocables.estados.estadosDeReparacion.EstaFueraDeRango;
import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import colocables.unidades.Aldeano;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Cartera;
import colocables.unidades.Colocable;
import colocables.unidades.NoHaySuficienteOroException;
import colocables.unidades.Poblacion;
import colocables.unidades.PoblacionMaximaAlcanzadaException;
import colocables.unidades.Unidad;
import colocables.unidades.UnidadDeAtaque;
import constructores.ConstructorDeCuartel;
import constructores.ConstructorDeEdificios;
import constructores.ConstructorDePlazaCentral;
import creadores.CreadorDeAldeanos;
import creadores.CreadorDeArmaDeAsedio;
import creadores.CreadorDeArqueros;
import creadores.CreadorDeEspadachines;
import creadores.CreadorDeUnidades;
import geometria.Posicion;

public class Jugador {


    Cartera cartera;
    public Poblacion poblacion;
    public Ciudad ciudad;
    public Equipo equipo;
    public Jugador() {
        this.poblacion = new Poblacion(50);
        this.ciudad = new Ciudad();
        this.equipo = new Equipo();
        equipo.agregarCiudad(ciudad);
        equipo.agregarPoblacion(poblacion);
    }


    public void construirCuartel(Aldeano aldeano, Posicion posicion) throws
            NoSeConstruyeEnPosicionAlejadaException, NoSeConstruyeEnMismaPosicionException,
            EstaOcupadoException, NoHaySuficienteOroException, UnidadNoPerteneceJugadorExcepcion {
        ConstructorDeCuartel constructorDeCuartel = new ConstructorDeCuartel();
        constructorDeCuartel.asignarEquipo(equipo);
        construirEdificio(constructorDeCuartel, aldeano, posicion);
    }

    public void construirPlazaCentral(Aldeano aldeano, Posicion posicion) throws
            NoSeConstruyeEnPosicionAlejadaException, NoSeConstruyeEnMismaPosicionException,
            EstaOcupadoException, NoHaySuficienteOroException, UnidadNoPerteneceJugadorExcepcion {
        ConstructorDePlazaCentral constructorDePlazaCentral = new ConstructorDePlazaCentral();
        constructorDePlazaCentral.asignarEquipo(equipo);
        construirEdificio(constructorDePlazaCentral, aldeano, posicion);
    }


    private void construirEdificio(ConstructorDeEdificios constructor, Aldeano aldeano, Posicion posicion) throws
            NoSeConstruyeEnPosicionAlejadaException, NoSeConstruyeEnMismaPosicionException,
            EstaOcupadoException, NoHaySuficienteOroException, UnidadNoPerteneceJugadorExcepcion {
        if(!equipo.incluye(aldeano)){
            throw new UnidadNoPerteneceJugadorExcepcion();
        }
        constructor.cobrar(cartera);
        aldeano.construir(constructor, posicion);
    }

    public void crearAsedio(Castillo castillo) throws EstaOcupadoException, NoHaySuficienteOroException, EdificioNoPerteneceJugadorExcepcion {
        CreadorDeArmaDeAsedio creadorDeAsedio = new CreadorDeArmaDeAsedio();
        creadorDeAsedio.asignarEquipo(equipo);
        crearUnidad(creadorDeAsedio, castillo);
    }

    public void crearAldeano(PlazaCentral plazaCentral)
            throws EstaOcupadoException, NoHaySuficienteOroException, EdificioNoPerteneceJugadorExcepcion {
        CreadorDeAldeanos creadorDeAldeanos = new CreadorDeAldeanos();
        creadorDeAldeanos.asignarEquipo(equipo);
        creadorDeAldeanos.conocerCartera(cartera);
        crearUnidad(creadorDeAldeanos, plazaCentral);
    }

    public void crearEspadachin(Cuartel cuartel)
            throws EstaOcupadoException, NoHaySuficienteOroException, EdificioNoPerteneceJugadorExcepcion {
        CreadorDeEspadachines creadorDeEspadachines = new CreadorDeEspadachines();
        creadorDeEspadachines.asignarEquipo(equipo);
        crearUnidad(creadorDeEspadachines, cuartel);
    }

    public void crearArquero(Cuartel cuartel)
            throws EstaOcupadoException, NoHaySuficienteOroException, EdificioNoPerteneceJugadorExcepcion {
        CreadorDeArqueros creadorDeArqueros = new CreadorDeArqueros();
        creadorDeArqueros.asignarEquipo(equipo);
        crearUnidad(creadorDeArqueros, cuartel);
    }

    private void crearUnidad(CreadorDeUnidades creador, Edificio edificio) throws
            NoHaySuficienteOroException, EstaOcupadoException, EdificioNoPerteneceJugadorExcepcion {
        if(!equipo.incluye(edificio)){
            throw new EdificioNoPerteneceJugadorExcepcion();
        }
        int oroAnterior = cartera.obtenerOro();
        creador.cobrar(cartera);
        try {
            edificio.crearUnidadCon(creador);
        }catch (PoblacionMaximaAlcanzadaException e){
            cartera.setOro(oroAnterior);
            throw new PoblacionMaximaAlcanzadaException();
        }catch (EstaOcupadoException e){
            cartera.setOro(oroAnterior);
            throw new EstaOcupadoException();
        }
    }

    //TODO: implementar metodo crearAsedio cuando este implementado


    public void mover(Colocable unidad, Posicion pos) throws UnidadNoPerteneceJugadorExcepcion {
        Unidad _unidad = (Unidad)unidad;
        if(!equipo.incluye(_unidad)){
            throw new UnidadNoPerteneceJugadorExcepcion();
        }
        _unidad.mover(pos);
    }

    public void terminarTurno(){
        this.equipo.terminarTurno();
    }

    public int cantOro() {
        return cartera.obtenerOro();
    }

	public void conocerCartera(Cartera carteraAConocer){
        cartera = carteraAConocer;
    }

    public void reparar(Aldeano miColocable, Colocable obtener) throws EstaSiendoReparadaPorUnAldeanoException, EstaFueraDeRango, NoNecesitaReparacionExeption {
        miColocable.reparar((Edificio)obtener);
    }


    public int cantPoblacion() {
	    return poblacion.obtenerCantidad();
    }

    public int cantPoblacionMax() {
        return equipo.obtenerCantidadMaxPoblacion();
    }

    public void atacar(UnidadDeAtaque unidadDeAtaque, Colocable colocable){
           unidadDeAtaque.atacar(colocable);

    }

    public void cambiarMontura(ArmaDeAsedio arma) throws UnidadNoPerteneceJugadorExcepcion{
    if(!equipo.incluye(arma)){
        throw new UnidadNoPerteneceJugadorExcepcion();
    }
        arma.cambiarMontura();
    }

    /*
     * PRE: Esto deberia funcionar con un colocable
     * POST: Devovler si el colocable pertenece al jugador o no.
     *
     * TODO: Refactorizar usando algun patron de disenio, esto esta
     * medio feo
     */
    public boolean incluye(Unidad unidad) {
    	return equipo.incluye(unidad);
    }

    public boolean incluye(Edificio edificio) {
    	return equipo.incluye(edificio);
    }
}

