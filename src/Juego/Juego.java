package Juego;

import java.util.List;
import java.util.Random;

import Juego.inicializadores.Inicializador;
import colocables.edificios.Castillo;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.edificios.JuegoTerminadoException;
import colocables.edificios.NoSeConstruyeEnMismaPosicionException;
import colocables.edificios.NoSeConstruyeEnPosicionAlejadaException;
import colocables.edificios.PlazaCentral;
import colocables.estados.EstaOcupadoException;
import colocables.estados.estadosDeReparacion.EstaFueraDeRango;
import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.NoEsUnEdificioException;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import colocables.unidades.Aldeano;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Colocable;
import colocables.unidades.ColocableAtacable;
import colocables.unidades.NoHaySuficienteOroException;
import colocables.unidades.UnidadDeAtaque;
import geometria.Posicion;
import jugadores.EdificioNoPerteneceJugadorExcepcion;
import jugadores.Jugador;
import jugadores.UnidadNoPerteneceJugadorExcepcion;
import mapa.Mapa;

public class Juego {

    protected Mapa mapa;
    protected List<Jugador> jugadores;
    Jugador jugadorDeTurno;
    protected int indiceJugador;
    protected int cantJugadores = 2; //hardcodeado por ahora

    public Juego(Inicializador inicializador) {

        inicializador.inicializar();
        jugadores = inicializador.obtenerJugadores();
        mapa = inicializador.obtenerMapa();

        Random rand = new Random();
        indiceJugador = rand.nextInt(cantJugadores);
        jugadorDeTurno = jugadores.get(indiceJugador);
    }

    public void mover(Colocable colocable, Posicion pos) throws UnidadNoPerteneceJugadorExcepcion{
        
           jugadorDeTurno.mover(colocable, pos);
         

    }

    public void terminarTurno(){
        jugadorDeTurno.terminarTurno();
        indiceJugador = (indiceJugador + 1) % cantJugadores;
        jugadorDeTurno = jugadores.get(indiceJugador);
    }

    public Mapa obtenerMapa(){return mapa;};

    public void reparar(Aldeano miAldeano, Colocable obtener) throws EstaSiendoReparadaPorUnAldeanoException, EstaFueraDeRango, NoNecesitaReparacionExeption {
        jugadorDeTurno.reparar(miAldeano, obtener);
    }

    public void atacar(UnidadDeAtaque unidadDeAtaque, ColocableAtacable colocableAtacable){
        try {
        	jugadorDeTurno.atacar(unidadDeAtaque,colocableAtacable);
        } catch (JuegoTerminadoException e) {
        	throw new JugadorGanador(this.jugadorDeTurno);
        }
    }


    //TODO trabajar con excepciones
    public void construirPlazaCentral(Colocable miColocable, Posicion miPosicion)
            throws NoHaySuficienteOroException, EstaOcupadoException, NoSeConstruyeEnPosicionAlejadaException,
            NoSeConstruyeEnMismaPosicionException, UnidadNoPerteneceJugadorExcepcion {
        jugadorDeTurno.construirPlazaCentral((Aldeano) miColocable, miPosicion);
    }

    public void construirCuartel(Colocable miColocable, Posicion posicion) throws NoHaySuficienteOroException,
            EstaOcupadoException, NoSeConstruyeEnPosicionAlejadaException, NoSeConstruyeEnMismaPosicionException,
            UnidadNoPerteneceJugadorExcepcion {
        jugadorDeTurno.construirCuartel((Aldeano) miColocable, posicion);
    }

    public void crearAsedio(Castillo castillo) throws NoHaySuficienteOroException,
            EdificioNoPerteneceJugadorExcepcion, EstaOcupadoException {
        jugadorDeTurno.crearAsedio(castillo);
    }

    public void crearEspadachin(Cuartel cuartel) throws NoHaySuficienteOroException,
            EdificioNoPerteneceJugadorExcepcion, EstaOcupadoException {
        jugadorDeTurno.crearEspadachin(cuartel);
    }

    public void crearArquero(Cuartel cuartel) throws NoHaySuficienteOroException,
            EdificioNoPerteneceJugadorExcepcion, EstaOcupadoException {
        jugadorDeTurno.crearArquero(cuartel);
    }

    public void crearAldeano(PlazaCentral plazaCentral) throws NoHaySuficienteOroException,
            EdificioNoPerteneceJugadorExcepcion, EstaOcupadoException {
        jugadorDeTurno.crearAldeano(plazaCentral);
    }

    public List<Jugador> obtenerJugadores(){
        return jugadores;
    }
    
    public Jugador obtenerJugadorDeTurno() {
    	return jugadorDeTurno;
    }

    public void cambiarMontura(ArmaDeAsedio arma) throws UnidadNoPerteneceJugadorExcepcion{
        jugadorDeTurno.cambiarMontura(arma);
    }
}
