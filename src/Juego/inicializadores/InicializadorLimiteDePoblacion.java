package Juego.inicializadores;

import java.util.ArrayList;

import colocables.edificios.PlazaCentral;
import colocables.unidades.Cartera;
import colocables.unidades.PoblacionMaximaAlcanzadaException;
import constructores.ConstructorDePlazaCentral;
import constructores.NoHayPosicionesValidasException;
import creadores.CreadorDeAldeanos;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Jugador;
import mapa.Mapa;
import mapa.PosicionInvalidaException;

public class InicializadorLimiteDePoblacion extends Inicializador{

    @Override
    public void inicializar() {
        this.mapa = new Mapa(16);
        Jugador jugador1 = new Jugador();
        Jugador jugador2 = new Jugador();

        Cartera carteraJugador1 = new Cartera();
        Cartera carteraJugador2 = new Cartera();

        jugador1.conocerCartera(carteraJugador1);
        jugador2.conocerCartera(carteraJugador2);

        ConstructorDePlazaCentral constructorDePlazaCentral = new ConstructorDePlazaCentral();
        constructorDePlazaCentral.asignarEquipo(jugador1.equipo);
        PlazaCentral plazaCentral = constructorDePlazaCentral.construir(new Posicion(0, 0));
        plazaCentral.asignarEquipo(jugador1.equipo);
        while (true) {
            try {
                mapa.colocar(plazaCentral, plazaCentral.obtenerPosiciones());
                break;
            } catch (PosicionInvalidaException e) {
                try {
                    constructorDePlazaCentral.reasignarPosicion(plazaCentral);
                } catch (NoHayPosicionesValidasException e1) {
                    throw new PosicionInvalidaException();
                }
            }
        }

        for (int i = 0; i < 3; i++) {
            plazaCentral.construir();
            plazaCentral.terminarTurno();
        }

        for (int i = 0; i < 16; i++) {
            for(int j = 4; j < 16; j++) {
                CreadorDeAldeanos creador = new CreadorDeAldeanos();
                creador.conocerCartera(carteraJugador1);
                creador.asignarEquipo(jugador1.equipo);
                Posiciones posicionesAldeano = new Posiciones();
                posicionesAldeano.agregar(new Posicion(i, j));
                try {
                    mapa.colocar(creador.crearUnidad(), posicionesAldeano);
                }catch(PoblacionMaximaAlcanzadaException e){
                    break;
                }
            }
        }
        this.jugadores = new ArrayList<Jugador>();

        jugadores.add(jugador1);
        jugadores.add(jugador2);
    }

}
