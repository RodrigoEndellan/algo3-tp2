package Juego.inicializadores;

import java.util.ArrayList;

import colocables.edificios.PlazaCentral;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Arquero;
import colocables.unidades.Cartera;
import colocables.unidades.Espadachin;
import constructores.ConstructorDePlazaCentral;
import constructores.NoHayPosicionesValidasException;
import creadores.CreadorDeArmaDeAsedio;
import creadores.CreadorDeArqueros;
import creadores.CreadorDeEspadachines;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Jugador;
import mapa.Mapa;
import mapa.PosicionInvalidaException;

public class InicializadorDemostracionAtaque extends Inicializador{

    @Override
    public void inicializar() {
        this.mapa = new Mapa(4);
        Jugador jugador1 = new Jugador();
        Jugador jugador2 = new Jugador();

        Cartera carteraJugador1 = new Cartera();
        Cartera carteraJugador2 = new Cartera();

        jugador1.conocerCartera(carteraJugador1);
        jugador2.conocerCartera(carteraJugador2);

        CreadorDeEspadachines creadorDeEspadachines = new CreadorDeEspadachines();
        creadorDeEspadachines.asignarEquipo(jugador1.equipo);
        Espadachin espadachin = creadorDeEspadachines.crearUnidad();
        Posiciones posicionesEspadachin = new Posiciones();
        posicionesEspadachin.agregar(new Posicion(0, 0));
        espadachin.darPosiciones(posicionesEspadachin);
        mapa.colocar(espadachin, posicionesEspadachin);

        CreadorDeArmaDeAsedio creadorDeArmaDeAsedio = new CreadorDeArmaDeAsedio();
        creadorDeArmaDeAsedio.asignarEquipo(jugador1.equipo);
        ArmaDeAsedio arma = creadorDeArmaDeAsedio.crearUnidad();
        Posiciones posicionesArma = new Posiciones();
        posicionesArma.agregar(new Posicion(0, 1));
        arma.darPosiciones(posicionesArma);
        mapa.colocar(arma, posicionesArma);

        CreadorDeArqueros creadorDeArqueros = new CreadorDeArqueros();
        creadorDeArqueros.asignarEquipo(jugador1.equipo);
        Arquero arquero = creadorDeArqueros.crearUnidad();
        Posiciones posicionesArquero = new Posiciones();
        posicionesArquero.agregar(new Posicion(0,2));
        arquero.darPosiciones(posicionesArquero);
        mapa.colocar(arquero, posicionesArquero);

        ConstructorDePlazaCentral constructorDePlazaCentral = new ConstructorDePlazaCentral();
        constructorDePlazaCentral.asignarEquipo(jugador2.equipo);
        PlazaCentral plazaCentral = constructorDePlazaCentral.construir(new Posicion(3, 3));
        plazaCentral.asignarEquipo(jugador2.equipo);
        while(true){
            try{
                mapa.colocar(plazaCentral, plazaCentral.obtenerPosiciones());
                break;
            }catch (PosicionInvalidaException e ){
                try {
                    constructorDePlazaCentral.reasignarPosicion(plazaCentral);
                } catch (NoHayPosicionesValidasException e1) {
                    throw new PosicionInvalidaException();
                }
            }
        }

        for(int i = 0; i < 3; i++){
            jugador2.terminarTurno();
        }
        carteraJugador2.setOro(150);

        this.jugadores = new ArrayList<Jugador>();

        jugadores.add(jugador1);
        jugadores.add(jugador2);
    }

}
