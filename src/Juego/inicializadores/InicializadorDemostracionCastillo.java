package Juego.inicializadores;

import java.util.ArrayList;

import colocables.edificios.Castillo;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Cartera;
import colocables.unidades.Espadachin;
import constructores.ConstructorDeCastillo;
import constructores.NoHayPosicionesValidasException;
import creadores.CreadorDeArmaDeAsedio;
import creadores.CreadorDeEspadachines;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Jugador;
import mapa.Mapa;
import mapa.PosicionInvalidaException;

public class InicializadorDemostracionCastillo extends Inicializador {

	@Override
    public void inicializar() {
        this.mapa = new Mapa(6);
        Jugador jugador1 = new Jugador();
        Jugador jugador2 = new Jugador();

        Cartera carteraJugador1=new Cartera();
        Cartera carteraJugador2=new Cartera();

        jugador1.conocerCartera(carteraJugador1);
        jugador2.conocerCartera(carteraJugador2);

        for(int i = 0; i < 6; i++) {
        	CreadorDeEspadachines creadorDeEspadachines = new CreadorDeEspadachines();
        	creadorDeEspadachines.asignarEquipo(jugador1.equipo);
            Espadachin espadachin = creadorDeEspadachines.crearUnidad();
            Posiciones posicionesEspadachin = new Posiciones();
            posicionesEspadachin.agregar(new Posicion(i, 1));
            espadachin.darPosiciones(posicionesEspadachin);
            mapa.colocar(espadachin, posicionesEspadachin);
        }
     
        for(int i = 0; i < 6; i++) {
        	CreadorDeArmaDeAsedio creadorDeArmaDeAsedio = new CreadorDeArmaDeAsedio();
            creadorDeArmaDeAsedio.asignarEquipo(jugador1.equipo);
            ArmaDeAsedio arma = creadorDeArmaDeAsedio.crearUnidad();
            Posiciones posicionesArma = new Posiciones();
            posicionesArma.agregar(new Posicion(i, 0));
            arma.darPosiciones(posicionesArma);
            mapa.colocar(arma, posicionesArma);
        }

        ConstructorDeCastillo constructorCastillo = new ConstructorDeCastillo();
        constructorCastillo.asignarEquipo(jugador2.equipo);
        Castillo castillo = constructorCastillo.construir(new Posicion(5, 5));
        castillo.asignarEquipo(jugador2.equipo);
        while(true){
            try{
                mapa.colocar(castillo, castillo.obtenerPosiciones());
                break;
            }catch (PosicionInvalidaException e ){
                try {
                	constructorCastillo.reasignarPosicion(castillo);
                } catch (NoHayPosicionesValidasException e1) {
                    throw new PosicionInvalidaException();
                }
            }
        }
        CreadorDeEspadachines creadorDeEspadachines = new CreadorDeEspadachines();
        creadorDeEspadachines.asignarEquipo(jugador2.equipo);
        Espadachin espadachin = creadorDeEspadachines.crearUnidad();
        Posiciones posicionesEspadachin = new Posiciones();
        posicionesEspadachin.agregar(new Posicion(1, 5));
        espadachin.darPosiciones(posicionesEspadachin);
        mapa.colocar(espadachin, posicionesEspadachin);

        this.jugadores = new ArrayList<Jugador>();

        jugadores.add(jugador1);
        jugadores.add(jugador2);
    }
}
