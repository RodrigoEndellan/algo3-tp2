package Juego.inicializadores;

import java.util.ArrayList;
import java.util.List;

import colocables.edificios.Castillo;
import colocables.edificios.PlazaCentral;
import colocables.estados.EstaOcupadoException;
import colocables.unidades.Cartera;
import constructores.ConstructorDeCastillo;
import constructores.ConstructorDePlazaCentral;
import constructores.NoHayPosicionesValidasException;
import creadores.CreadorDeAldeanos;
import geometria.Posicion;
import jugadores.Jugador;
import mapa.Mapa;
import mapa.PosicionInvalidaException;

public class InicializadorNormal extends Inicializador{

    List<Jugador> jugadores;
    Mapa mapa;

    public void inicializar() {


        this.mapa = new Mapa(16);
        Jugador jugador1=new Jugador();
        Jugador jugador2=new Jugador();

        Cartera carteraJugador1=new Cartera();
        Cartera carteraJugador2=new Cartera();

        jugador1.conocerCartera(carteraJugador1);
        jugador2.conocerCartera(carteraJugador2);

        ConstructorDeCastillo constructorDeCastilloJugador1 = new ConstructorDeCastillo();
        constructorDeCastilloJugador1.asignarEquipo(jugador1.equipo);

        Castillo castilloJugador1 = constructorDeCastilloJugador1.construir(new Posicion(0,0));


        while(true){
            try{
                mapa.colocar(castilloJugador1, castilloJugador1.obtenerPosiciones());
                break;
            }catch (mapa.PosicionInvalidaException e) {
                try{
                    constructorDeCastilloJugador1.reasignarPosicion(castilloJugador1);
                }catch(NoHayPosicionesValidasException f){
                    throw new PosicionInvalidaException();
                }
            }
        }

        ConstructorDePlazaCentral constructorDePlazaCentralJugador1 = new ConstructorDePlazaCentral();
        constructorDePlazaCentralJugador1.asignarEquipo(jugador1.equipo);

        PlazaCentral plazaCentralJugador1 = constructorDePlazaCentralJugador1.construir(new Posicion(0,5));
        plazaCentralJugador1.asignarEquipo(jugador1.equipo);


        while(true){
            try{
                mapa.colocar(plazaCentralJugador1, plazaCentralJugador1.obtenerPosiciones());
                break;
            }catch (mapa.PosicionInvalidaException e) {
                try{
                    constructorDePlazaCentralJugador1.reasignarPosicion(plazaCentralJugador1);
                }catch(NoHayPosicionesValidasException f){
                    throw new PosicionInvalidaException();
                }
            }
        }

        plazaCentralJugador1.construir();
        plazaCentralJugador1.construir();
        plazaCentralJugador1.construir();

        CreadorDeAldeanos creadorJugador1 = new CreadorDeAldeanos();
        creadorJugador1.conocerCartera(carteraJugador1);
        creadorJugador1.asignarEquipo(jugador1.equipo);

        try {
            plazaCentralJugador1.crearUnidadCon(creadorJugador1);
        } catch (EstaOcupadoException e) {
            e.printStackTrace();
        }
        plazaCentralJugador1.terminarTurno();

        try {
            plazaCentralJugador1.crearUnidadCon(creadorJugador1);
        } catch (EstaOcupadoException e) {
            e.printStackTrace();
        }

        plazaCentralJugador1.terminarTurno();

        try {
            plazaCentralJugador1.crearUnidadCon(creadorJugador1);
        } catch (EstaOcupadoException e) {
            e.printStackTrace();
        }

        plazaCentralJugador1.terminarTurno();

//--------------------------------------------------------------------------------

        ConstructorDeCastillo constructorDeCastilloJugador2 = new ConstructorDeCastillo();
        constructorDeCastilloJugador2.asignarEquipo(jugador2.equipo);

        Castillo castilloJugador2 = constructorDeCastilloJugador2.construir(new Posicion(15,15));

        while(true){
            try{
                mapa.colocar(castilloJugador2, castilloJugador2.obtenerPosiciones());
                break;
            }catch (mapa.PosicionInvalidaException e) {
                try{
                    constructorDeCastilloJugador2.reasignarPosicion(castilloJugador2);
                }catch(NoHayPosicionesValidasException f){
                    throw new PosicionInvalidaException();
                }
            }
        }

        ConstructorDePlazaCentral constructorDePlazaCentralJugador2 = new ConstructorDePlazaCentral();
        constructorDePlazaCentralJugador2.asignarEquipo(jugador2.equipo);


        PlazaCentral plazaCentralJugador2 = constructorDePlazaCentralJugador2.construir(new Posicion(13,10));
        plazaCentralJugador2.asignarEquipo(jugador2.equipo);


        while(true){
            try{
                mapa.colocar(plazaCentralJugador2, plazaCentralJugador2.obtenerPosiciones());
                break;
            }catch (mapa.PosicionInvalidaException e) {
                try{
                    constructorDePlazaCentralJugador2.reasignarPosicion(plazaCentralJugador2);
                }catch(NoHayPosicionesValidasException f){
                    throw new PosicionInvalidaException();
                }
            }
        }

        plazaCentralJugador2.construir();
        plazaCentralJugador2.construir();
        plazaCentralJugador2.construir();

        CreadorDeAldeanos creadorJugador2 = new CreadorDeAldeanos();
        creadorJugador2.conocerCartera(carteraJugador2);
        creadorJugador2.asignarEquipo(jugador2.equipo);

        try {
            plazaCentralJugador2.crearUnidadCon(creadorJugador2);
        } catch (EstaOcupadoException e) {
            e.printStackTrace();
        }
        plazaCentralJugador2.terminarTurno();

        try {
            plazaCentralJugador2.crearUnidadCon(creadorJugador2);
        } catch (EstaOcupadoException e) {
            e.printStackTrace();
        }

        plazaCentralJugador2.terminarTurno();

        try {
            plazaCentralJugador2.crearUnidadCon(creadorJugador2);
        } catch (EstaOcupadoException e) {
            e.printStackTrace();
        }

        plazaCentralJugador2.terminarTurno();
//------------------------------------------------------------------------------
        carteraJugador1.setOro(300);
        carteraJugador2.setOro(300);

        this.jugadores = new ArrayList<Jugador>();


        jugadores.add(jugador1);
        jugadores.add(jugador2);
    }

    public Mapa obtenerMapa() {
        return this.mapa;
    }

    public List<Jugador> obtenerJugadores() {
        return this.jugadores;
    }


}


