package Juego.inicializadores;

import java.util.List;

import jugadores.Jugador;
import mapa.Mapa;

public abstract class Inicializador {
	
	List<Jugador> jugadores;
	Mapa mapa;

	//TODO Revisar si tamanio minimo de mapa puede ser 20
	public Inicializador() {
		
	}
	
	public abstract void inicializar();

	public Mapa obtenerMapa() {
		return this.mapa;
	}

	public List<Jugador> obtenerJugadores() {
		return this.jugadores;
	}

}
