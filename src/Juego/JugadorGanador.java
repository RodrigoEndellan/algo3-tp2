package Juego;

import jugadores.Jugador;

public class JugadorGanador extends RuntimeException {
	Jugador ganador;
	public JugadorGanador(Jugador ganador) {
		super();
		
		this.ganador = ganador;
	}
	public Jugador obtenerGanador() {
		return this.ganador;
	}
}
