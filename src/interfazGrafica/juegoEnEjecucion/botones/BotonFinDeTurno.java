package interfazGrafica.juegoEnEjecucion.botones;

import Juego.Juego;
import interfazGrafica.juegoEnEjecucion.eventos.FinDeTurnoEventHandler;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeImagenes;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

public class BotonFinDeTurno extends Button {

	Juego juegoActual;
	
	public BotonFinDeTurno(Juego juegoActual) {
		super();
		
		this.juegoActual = juegoActual;
		
		this.setAlignment(Pos.CENTER);
		
		this.setMinWidth(170);
		this.setMaxWidth(170);
		this.setMinHeight(100);
		this.setMaxHeight(100);
		
		this.actualizar();

				
		this.setOnAction(new FinDeTurnoEventHandler(this, juegoActual));
	}

	public void actualizar() {
		Image escudoDeJugador = new Image("/interfazGrafica/recursos/texturas/BotonTerminarTurno.png");
		
		escudoDeJugador = GrillaDeImagenes.pintarImagen(escudoDeJugador, juegoActual.obtenerJugadorDeTurno());
		
		BackgroundImage backImg = new BackgroundImage(
				escudoDeJugador, 
				BackgroundRepeat.NO_REPEAT, 
				BackgroundRepeat.NO_REPEAT, 
				BackgroundPosition.CENTER,
		        BackgroundSize.DEFAULT
				);
		
		this.setBackground(new Background(backImg));
	}
}
