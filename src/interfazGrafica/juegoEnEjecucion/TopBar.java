package interfazGrafica.juegoEnEjecucion;

import java.util.Map;

import Juego.Juego;
import interfazGrafica.JugadorFX;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import jugadores.Jugador;

public class TopBar extends HBox {

	Map<Jugador, String> nombre;
	Juego juegoActual;
	Label jugadorDeTurno;
	
	public TopBar(Map<Jugador, String> nombre, Juego juegoActual) {
		super();
		
		this.nombre = nombre;
		this.juegoActual = juegoActual;
		
		this.setAlignment(Pos.CENTER);
		this.setMinHeight(30);
		this.setMaxHeight(30);
		
		/*
		 * TopBar Background
		 */
		
		BackgroundImage backImg = new BackgroundImage(
				new Image("/interfazGrafica/recursos/texturas/TopBar.png"),
				BackgroundRepeat.REPEAT, 
				BackgroundRepeat.REPEAT, 
				BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT
		        );
		
		this.setBackground(new Background(backImg));
		
		
		jugadorDeTurno = new Label();
		
		Jugador jugador = this.juegoActual.obtenerJugadorDeTurno();
		String nombreJugadorDeTurno = JugadorFX.obtenerNombre(jugador);
		
		this.jugadorDeTurno.setText("Juega " + nombreJugadorDeTurno);
		
		jugadorDeTurno.setTextFill(Paint.valueOf(Color.WHITE.toString()));
		
		this.getChildren().add(jugadorDeTurno);
		
	}
	
	public void actualizar() {
		
		Jugador jugador = this.juegoActual.obtenerJugadorDeTurno();
		String nombreJugadorDeTurno = JugadorFX.obtenerNombre(jugador);
		
		this.jugadorDeTurno.setFont(new Font(
				"interfazGrafica/recursos/fuentes/LACONICK.TTF",
				12));
		
		this.jugadorDeTurno.setText("Juega " + nombreJugadorDeTurno);
		
		this.jugadorDeTurno.setFont(new Font(
				"interfazGrafica/recursos/fuentes/LACONICK.TTF",
				12));
	}
	
}
