

package interfazGrafica.juegoEnEjecucion.eventos;

import java.net.URL;

import Juego.Juego;
import interfazGrafica.escenas.JuegoEnEjecucionPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.media.AudioClip;

public class FinDeTurnoEventHandler implements EventHandler<ActionEvent> {
	
	private Button boton;
	private Juego juegoActual;
	
	public FinDeTurnoEventHandler(Button boton, Juego juegoActual) {
		this.boton = boton;
		this.juegoActual = juegoActual;
	}
	
	@Override
	public void handle(ActionEvent actionEvent) {
		URL resource = getClass().getResource("/interfazGrafica/Sonidos/interface/interface1.wav");
	    AudioClip clip = new AudioClip(resource.toString());
		clip.play();
	    
        juegoActual.terminarTurno();
        ((JuegoEnEjecucionPane) this.boton.getScene().getRoot()).actualizar();
	}

}
