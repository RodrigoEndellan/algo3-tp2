package interfazGrafica.juegoEnEjecucion.grillaCentral;

import java.util.HashMap;
import java.util.Map;

import interfazGrafica.escenas.JuegoEnEjecucionPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import mapa.Mapa;

public abstract class Grilla extends GridPane {

    protected Mapa mapa;
    protected JuegoEnEjecucionPane juegoActual;
    protected Map<String, String> colocablesPosibles;
    protected Map<String, Integer> tamanioDeColocables;
    
    public Grilla(JuegoEnEjecucionPane juegoEnEjecucionPane, Mapa mapaASetear) {
        super();
        mapa = mapaASetear;
        juegoActual = juegoEnEjecucionPane;
        this.crearGrilla(mapa.obtenerTamanio());
        

        crearHashDeColocables();
        crearHashTamanioDeColocables();
    }

    /* Campo relacionado con GrillaImagenes
     * PRE: Ninguna.
     * POST: A cada clase, se le asigna un tamanio.
     */
    private void crearHashTamanioDeColocables() {
		this.tamanioDeColocables = new HashMap<String, Integer>();
		
		this.tamanioDeColocables.put("Aldeano", 1);
		this.tamanioDeColocables.put("Espadachin", 1);
		this.tamanioDeColocables.put("Arquero", 1);
		this.tamanioDeColocables.put("ArmaDeAsedio", 1);
		
		this.tamanioDeColocables.put("Castillo", 4);
		this.tamanioDeColocables.put("PlazaCentral", 2);
		this.tamanioDeColocables.put("Cuartel", 2);
		
	}

    /* Campo relacionado con GrillaDeJuego
     * PRE: Ninguna.
     * POST: Se le asigna a cada clase un boton menu para instanciar.
     */
	private void crearHashDeColocables(){
    	this.colocablesPosibles = new HashMap<String, String >();
    	
    	this.colocablesPosibles.put(
    			"Aldeano", 
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuAldeano" 
    			);
    	this.colocablesPosibles.put(
    			"Espadachin", 
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuEspadachin" 
    			);
    	this.colocablesPosibles.put(
    			"Arquero", 
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuArquero" 
    			);
    	this.colocablesPosibles.put(
    			"ArmaDeAsedio",
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuAsedio" 
    			);
    	
    	this.colocablesPosibles.put(
    			"Castillo", 
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuCastillo" 
    			);
    	this.colocablesPosibles.put(
    			"PlazaCentral", 
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuPlazaCentral" 
    			);
    	this.colocablesPosibles.put(
    			"Cuartel", 
    			"interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuCuartel" 
    			);
    	
    	
    }

	private void crearGrilla(int tamanio) {
        this.setHgap(0);
        this.setVgap(0);
        this.setGridLinesVisible(true);
        this.getStylesheets().add(getClass().getResource("/interfazGrafica/botonesTransparentes.css").toExternalForm());

        for (int i = 0; i < tamanio; i++) {
            ColumnConstraints columna = new ColumnConstraints(50);
            columna.setHgrow(Priority.ALWAYS);
            columna.setFillWidth(true);
            this.getColumnConstraints().add(columna);

            RowConstraints fila = new RowConstraints(50);
            fila.setVgrow(Priority.ALWAYS);
            fila.setFillHeight(true);
            this.getRowConstraints().add(fila);
        }
        
        
    }
}
