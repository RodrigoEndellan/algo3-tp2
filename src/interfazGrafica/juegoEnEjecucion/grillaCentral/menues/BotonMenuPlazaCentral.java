package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.edificios.PlazaCentral;
import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

public class BotonMenuPlazaCentral extends BotonMenu{

    public BotonMenuPlazaCentral(Colocable colocable, GrillaDeJuego juego){
        super(colocable, juego);
        this.inicializar();
    }

    @Override
    protected void inicializar(){
        MenuItem crearAldeano = new MenuItem("Crear Aldeano (25)");

        this.getItems().addAll(crearAldeano);

        crearAldeano.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                miJuego.crearAldeano((PlazaCentral)miColocable);
                miJuego.actualizar();
            }
        });


    }

}

