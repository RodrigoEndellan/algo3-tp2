package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.unidades.Colocable;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBoton;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import mapa.Mapa;

public abstract class BotonMenu extends MenuButton{

    protected Colocable miColocable;
    protected GrillaDeJuego miJuego;


    public BotonMenu(Colocable colocable, GrillaDeJuego juego){
        super();
        miColocable = colocable;
        miJuego = juego;
    }

    protected abstract void inicializar();


    protected void accionMoverConstruir(FactoryBoton factoryBoton) {

        Mapa mapa = miJuego.obtenerMapa();
        Node node = miJuego.getChildren().get(0);
        miJuego.getChildren().clear();
        miJuego.getChildren().add(0, node);

        miJuego.setGridLinesVisible(true);
        for (int i = 0; i < mapa.obtenerTamanio(); i++) {
            for (int j = 0; j < mapa.obtenerTamanio(); j++) {
                Posicion pos = new Posicion(i, j);
                Button boton = new Button("");
                boton.setOnAction(factoryBoton.crearBoton(pos, miJuego, miColocable));
                boton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                miJuego.add(boton, i, j);
            }
        }
    }

    protected void accionAtacarReparar(FactoryBoton factoryBoton) {
        Mapa mapa = miJuego.obtenerMapa();
        Node node = miJuego.getChildren().get(0);
        miJuego.getChildren().clear();
        miJuego.getChildren().add(0, node);

        miJuego.setGridLinesVisible(true);
        for (int i = 0; i < mapa.obtenerTamanio(); i++) {
            for (int j = 0; j < mapa.obtenerTamanio(); j++) {
                Posicion pos = new Posicion(i, j);
                if (!mapa.estaOcupado(pos)) {
                    continue;
                }
                Button boton = new Button("");
                boton.setOnAction(factoryBoton.crearBoton(pos, miJuego, miColocable));
                boton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                miJuego.add(boton, i, j);
            }
        }
    }


}
