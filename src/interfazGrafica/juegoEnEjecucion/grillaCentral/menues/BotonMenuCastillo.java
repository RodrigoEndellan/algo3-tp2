package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.edificios.Castillo;
import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

public class BotonMenuCastillo extends BotonMenu{

    public BotonMenuCastillo(Colocable colocable, GrillaDeJuego juego){
        super(colocable, juego);
        this.inicializar();
    }

    @Override
    protected void inicializar(){
        MenuItem crearAsedio = new MenuItem("Crear Asedio (200)");
        this.getItems().addAll(crearAsedio);

        crearAsedio.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
               miJuego.crearAsedio((Castillo)miColocable);
               miJuego.actualizar();
            }
        });
    }
}
