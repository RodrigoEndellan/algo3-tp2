package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import jugadores.UnidadNoPerteneceJugadorExcepcion;

public class BotonMenuAsedio extends BotonMenuUnidadAtaque {
    public BotonMenuAsedio(Colocable colocable, GrillaDeJuego juego) {

        super(colocable, juego);
        MenuItem cambiarMontura = new MenuItem("Cambiar montura");

        this.getItems().add(cambiarMontura);

        cambiarMontura.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    miJuego.cambiarMontura((ArmaDeAsedio) colocable);
                } catch (UnidadNoPerteneceJugadorExcepcion unidadNoPerteneceJugadorExcepcion) {
                    unidadNoPerteneceJugadorExcepcion.printStackTrace();
                }
            }
        });


    }
}
