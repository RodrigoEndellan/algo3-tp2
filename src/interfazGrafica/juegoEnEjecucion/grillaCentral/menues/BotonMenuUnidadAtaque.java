package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBotonAtacar;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBotonMover;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

public abstract class BotonMenuUnidadAtaque extends BotonMenu {

    public BotonMenuUnidadAtaque(Colocable colocable, GrillaDeJuego juegoFX) {
        super(colocable, juegoFX);
        this.inicializar();
    }

    @Override
    protected void inicializar() {
        MenuItem mover = new MenuItem("Mover");
        MenuItem atacar = new MenuItem("Atacar");

        this.getItems().addAll(mover, atacar);

        mover.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                accionMoverConstruir(new FactoryBotonMover());
            }
        });

        atacar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                accionAtacarReparar(new FactoryBotonAtacar());
            }
        });
    }
}
