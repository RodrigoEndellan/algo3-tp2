package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;


import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBotonConstruirCuartel;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBotonConstruirPlazaCentral;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBotonMover;
import interfazGrafica.juegoEnEjecucion.grillaCentral.botones.FactoryBotonReparar;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

public class BotonMenuAldeano extends BotonMenu {

    public BotonMenuAldeano(Colocable colocable, GrillaDeJuego juego) {
        super(colocable, juego);
        this.inicializar();
    }

    @Override
    protected void inicializar() {
        MenuItem mover = new MenuItem("Mover");
        MenuItem reparar = new MenuItem("Reparar");
        MenuItem construirCuartel = new MenuItem("Construir Cuartel (50)");
        MenuItem construirPlaza = new MenuItem("Construir PlazaCentral (100)");

        this.getItems().addAll(mover, construirCuartel, construirPlaza, reparar);

        mover.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                accionMoverConstruir(new FactoryBotonMover());
            }
        });

        reparar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                accionAtacarReparar(new FactoryBotonReparar());
            }
        });

        construirCuartel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                accionMoverConstruir(new FactoryBotonConstruirCuartel());
            }
        });

        construirPlaza.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                accionMoverConstruir(new FactoryBotonConstruirPlazaCentral());
            }
        });

    }

}
