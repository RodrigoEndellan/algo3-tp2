package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;

public class BotonMenuEspadachin extends BotonMenuUnidadAtaque {
    public BotonMenuEspadachin(Colocable colocable, GrillaDeJuego juego) {
        super(colocable, juego);
    }
}
