package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.edificios.Cuartel;
import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

public class BotonMenuCuartel extends BotonMenu{

    public BotonMenuCuartel(Colocable colocable, GrillaDeJuego juego){
        super(colocable, juego);
        this.inicializar();
    }

    @Override
    protected void inicializar(){
        MenuItem crearEspadachin = new MenuItem("Crear Espadachin (50)");
        MenuItem crearArquero = new MenuItem("Crear Arquero (75)");


        this.getItems().addAll(crearEspadachin, crearArquero);

        crearEspadachin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                miJuego.crearEspadachin((Cuartel)miColocable);
                miJuego.actualizar();
            }
        });

        crearArquero.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                miJuego.crearArquero((Cuartel)miColocable);
                miJuego.actualizar();
            }
        });

    }
}