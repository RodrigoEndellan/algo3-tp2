package interfazGrafica.juegoEnEjecucion.grillaCentral.menues;

import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;

public class BotonMenuArquero extends BotonMenuUnidadAtaque {
    public BotonMenuArquero(Colocable colocable, GrillaDeJuego juego) {
        super(colocable, juego);
    }
}
