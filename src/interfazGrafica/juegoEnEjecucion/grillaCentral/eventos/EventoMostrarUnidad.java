package interfazGrafica.juegoEnEjecucion.grillaCentral.eventos;

import colocables.unidades.Colocable;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.ConsoleBar;
import javafx.event.EventHandler;
import javafx.scene.input.ContextMenuEvent;
import mapa.Mapa;
import mapa.PosicionVaciaException;

public class EventoMostrarUnidad implements EventHandler<ContextMenuEvent> {

	ConsoleBar console;
	Mapa mapa;
	
	public EventoMostrarUnidad(ConsoleBar console, Mapa mapa) {
		super();
		this.console = console;
		this.mapa = mapa;
	}
	
	@Override
	public void handle(ContextMenuEvent event) {
    	
    	Colocable colocableAMostrar = null;
    	
    	try {
    		colocableAMostrar = this.obtenerColocable(event.getX() , event.getY());
    	} catch (PosicionVaciaException exception) {
    		//NADA
    	}
    	
    	if(colocableAMostrar != null) {
    		console.mostrarColocable(colocableAMostrar);
    	}
		
	}
	
	private Colocable obtenerColocable(double x, double y) {
		//Al castear por int, trunca, y me ayuda a conocer la posicion.
		//Numero magico, deberiamos poder setear en algun lugar el tamanio
		//De las celdas.
		int posX = (int) x / 50;
		int posY = (int) y / 50;
		
		Colocable colocableAMostrar = null;
		
		try {
			colocableAMostrar = this.mapa.obtener(new Posicion(posX, posY));
		} catch (PosicionVaciaException e) {
			throw new PosicionVaciaException();
		}
		
		return colocableAMostrar;
	}

}
