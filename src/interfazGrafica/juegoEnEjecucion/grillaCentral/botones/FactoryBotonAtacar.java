package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import colocables.unidades.Colocable;
import colocables.unidades.UnidadDeAtaque;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;

public class FactoryBotonAtacar extends FactoryBoton {
    @Override
    public BotonUnidad crearBoton(Posicion posicion, GrillaDeJuego juego, Colocable colocable) {
        return new BotonAtacar(posicion, juego, (UnidadDeAtaque)colocable);
    }
}
