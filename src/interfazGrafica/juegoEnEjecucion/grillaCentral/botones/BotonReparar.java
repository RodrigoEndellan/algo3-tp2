package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import java.net.URL;

import colocables.estados.estadosDeReparacion.EstaFueraDeRango;
import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.NoEsUnEdificioException;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import colocables.unidades.Aldeano;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.AudioClip;

public class BotonReparar extends BotonUnidad implements EventHandler<ActionEvent> {

    Aldeano miAldeano;
    public BotonReparar(Posicion posicion, GrillaDeJuego juego, Aldeano aldeano){
        super(posicion, juego);
        miAldeano = aldeano;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/wood-small.wav");
	    AudioClip clip = new AudioClip(resource.toString());
		clip.play();
    	
        miJuego.reparar(miAldeano, miJuego.obtenerMapa().obtener(miPosicion));
    }
}
