package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import java.net.URL;

import colocables.unidades.Aldeano;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.scene.media.AudioClip;

public class BotonConstruirCuartel extends BotonUnidad{

    Aldeano miAldeano;
    public BotonConstruirCuartel(Posicion posicion, GrillaDeJuego juego, Aldeano aldeano) {
        super(posicion, juego);
        miAldeano = aldeano;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/wood-small.wav");
	    AudioClip clip = new AudioClip(resource.toString());
    	
        miJuego.construirCuartel(miAldeano, miPosicion);
        clip.play();
    }
}
