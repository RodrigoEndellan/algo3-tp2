package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import colocables.unidades.Aldeano;
import colocables.unidades.Colocable;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;

public class FactoryBotonConstruirCuartel extends FactoryBoton{
    @Override
    public BotonUnidad crearBoton(Posicion posicion, GrillaDeJuego juego, Colocable colocable) {
        return new BotonConstruirCuartel(posicion, juego, (Aldeano)colocable);
    }
}
