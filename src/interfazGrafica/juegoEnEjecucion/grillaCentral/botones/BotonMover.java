package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import java.net.URL;

import colocables.unidades.Colocable;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.scene.media.AudioClip;

public class BotonMover extends BotonUnidad{

    Colocable miColocable;
    public BotonMover(Posicion posicion, GrillaDeJuego juego, Colocable colocable){
        super(posicion, juego);
        miColocable = colocable;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/armor-light.wav");
	    AudioClip clip = new AudioClip(resource.toString());
		clip.play();
    	
        miJuego.mover(miColocable, miPosicion);
    }
}




