package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public abstract class BotonUnidad implements EventHandler<ActionEvent> {

    Posicion miPosicion;
    GrillaDeJuego miJuego;

    public BotonUnidad(Posicion pos, GrillaDeJuego juego){
        miJuego = juego;
        miPosicion = pos;
    }

    @Override
    public abstract void handle(ActionEvent actionEvent);
}
