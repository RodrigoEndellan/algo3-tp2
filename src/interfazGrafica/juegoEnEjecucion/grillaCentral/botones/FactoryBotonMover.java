package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import colocables.unidades.Colocable;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;

public class FactoryBotonMover extends FactoryBoton{

    public FactoryBotonMover(){

    }

    @Override
    public BotonUnidad crearBoton(Posicion posicion, GrillaDeJuego juego, Colocable colocable){
        return new BotonMover(posicion, juego, colocable);
    }

}
