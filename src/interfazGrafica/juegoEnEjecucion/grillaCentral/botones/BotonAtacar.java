package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import java.net.URL;

import colocables.unidades.ColocableAtacable;
import colocables.unidades.UnidadDeAtaque;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.AudioClip;

public class BotonAtacar extends BotonUnidad implements EventHandler<ActionEvent> {

    UnidadDeAtaque miUnidad;
    public BotonAtacar(Posicion pos, GrillaDeJuego juego, UnidadDeAtaque colocable) {
        super(pos, juego);
        miUnidad = colocable;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/battle/swing2.wav");
	    AudioClip clip = new AudioClip(resource.toString());    	
        miJuego.atacar((UnidadDeAtaque)miUnidad, (ColocableAtacable) miJuego.obtenerMapa().obtener(miPosicion));
        clip.play();
    }
}
