package interfazGrafica.juegoEnEjecucion.grillaCentral.botones;

import colocables.unidades.Colocable;
import geometria.Posicion;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;

public abstract class FactoryBoton {

    public abstract BotonUnidad crearBoton(Posicion posicion, GrillaDeJuego juego, Colocable colocable);
}
