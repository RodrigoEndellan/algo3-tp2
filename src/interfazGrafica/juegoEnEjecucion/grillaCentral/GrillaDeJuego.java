package interfazGrafica.juegoEnEjecucion.grillaCentral;



import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

import colocables.edificios.Castillo;
import colocables.edificios.Cuartel;
import colocables.edificios.PlazaCentral;
import colocables.estados.estadosDeReparacion.EstaFueraDeRango;
import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.NoEsUnEdificioException;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import colocables.unidades.Aldeano;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Colocable;
import colocables.unidades.ColocableAtacable;
import colocables.unidades.UnidadDeAtaque;
import geometria.Posicion;
import interfazGrafica.escenas.JuegoEnEjecucionPane;
import interfazGrafica.juegoEnEjecucion.ConsoleBar;
import interfazGrafica.juegoEnEjecucion.grillaCentral.eventos.EventoMostrarUnidad;
import interfazGrafica.juegoEnEjecucion.grillaCentral.menues.BotonMenuAldeano;
import javafx.scene.control.MenuButton;
import javafx.scene.media.AudioClip;
import jugadores.UnidadNoPerteneceJugadorExcepcion;
import mapa.Mapa;


public class GrillaDeJuego extends Grilla {
	
	ConsoleBar consoleBar;
	
	//TODO Transportar logica de juego aca
	public GrillaDeJuego(JuegoEnEjecucionPane juegoEnEjecucionPane, Mapa mapaASetear, ConsoleBar consoleBar) {
        super(juegoEnEjecucionPane, mapaASetear);
        
        this.consoleBar = consoleBar;
        
        this.setOnContextMenuRequested(new EventoMostrarUnidad(this.consoleBar, this.mapa));
        actualizar();
	}
	
	

	/* TODO: Una clase estatica o algo que se encargue de todas esta
	 * acciones.
	 */
	public void reparar(Aldeano aldeano, Colocable obtener){
		juegoActual.reparar(aldeano, obtener);
    }

    public void atacar(UnidadDeAtaque unidadDeAtaque, ColocableAtacable colocableAtacable){
    	juegoActual.atacar(unidadDeAtaque,colocableAtacable);
    }

    public void construirPlazaCentral(Aldeano miAldeano, Posicion miPosicion) {
	    juegoActual.construirPlazaCentral(miAldeano, miPosicion);
    }

    public void construirCuartel(Aldeano miAldeano, Posicion posicion) {
	    juegoActual.construirCuartel(miAldeano, posicion);
    }

    public void crearAsedio(Castillo castillo) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/coin3.wav");
	    AudioClip clip = new AudioClip(resource.toString());
    	juegoActual.crearAsedio(castillo);
    	clip.play();
    }

    public void crearEspadachin(Cuartel cuartel) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/coin3.wav");
	    AudioClip clip = new AudioClip(resource.toString());
    	juegoActual.crearEspadachin(cuartel);
    	clip.play();
    }

    public void crearArquero(Cuartel cuartel) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/coin3.wav");
	    AudioClip clip = new AudioClip(resource.toString());
    	juegoActual.crearArquero(cuartel);
    	clip.play();
    }

    public void crearAldeano(PlazaCentral plazaCentral) {
    	
    	URL resource = getClass().getResource("/interfazGrafica/Sonidos/inventory/coin3.wav");
	    AudioClip clip = new AudioClip(resource.toString());
    	juegoActual.crearAldeano(plazaCentral);
    	clip.play();
    }

    public Mapa obtenerMapa() {
    	return this.mapa;
    }
    
    public void mover(Colocable colocable, Posicion pos){
    	juegoActual.mover(colocable, pos);
    }

    public void cambiarMontura(ArmaDeAsedio arma) throws UnidadNoPerteneceJugadorExcepcion { juegoActual.cambiarMontura(arma);}


	public void actualizar() {

        this.getChildren().clear();
		Set<Colocable> colocables = mapa.obtenerColocables();
		
		Iterator<Colocable> colocablesIter = colocables.iterator();
		
		while (colocablesIter.hasNext()) {

            Colocable colocable = colocablesIter.next();
            MenuButton menu = new MenuButton("");

            /*
             * inicia el curso de magia negra
             */
            Class<?> c = null;
            
            String nameColocable = colocable.getClass().getSimpleName();
            
            String buttonMenuName = this.colocablesPosibles.get(nameColocable);
            
            BotonMenuAldeano botonMenu = new BotonMenuAldeano(colocable, this);
            
            String nameBotonMenuAldeanoClass = botonMenu.getClass().getName();
            
            try {
				c = Class.forName(buttonMenuName) ;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            Constructor<?> cons = null;
            
            try {
				cons = c.getConstructor(Colocable.class, GrillaDeJuego.class);
			} catch (NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            try {
				menu = (MenuButton) cons.newInstance(colocable, this);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            /* Finaliza el curso de magia negra*/

            menu.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            Posicion posIzquierda = colocable.obtenerPosiciones().esquinaInferiorIzquierda();
            Posicion posDerecha = colocable.obtenerPosiciones().esquinaSuperiorDerecha();

            if (posDerecha.x() == posIzquierda.x() || posDerecha.y() == posIzquierda.y()) {
                this.add(menu, posDerecha.x(), posDerecha.y());
            } else {
                this.add(menu, posIzquierda.x() , posIzquierda.y(), (posDerecha.x() - posIzquierda.x() + 1), (posDerecha.y() - posIzquierda.y() +1));
            }
            menu.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        }
	}

	
    
}
