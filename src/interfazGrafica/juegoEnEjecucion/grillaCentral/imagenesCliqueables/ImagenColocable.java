package interfazGrafica.juegoEnEjecucion.grillaCentral.imagenesCliqueables;

import colocables.unidades.Colocable;
import geometria.Posicion;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImagenColocable extends ImageView {
	
	Colocable colocable;
	Posicion origen;
	
	public ImagenColocable(Colocable colocable, String ruta) {
		super(ruta);
		
		this.colocable = colocable;
		
		this.origen = colocable.obtenerPosiciones().esquinaInferiorIzquierda();
		
	}
	
	/*private Posicion obtenerPosicionOrigen(Posiciones posicionesDondeBuscar) {
		
		Iterador iter = posicionesDondeBuscar.getIterator();
		
		Posicion posicionAux = iter.obtenerActual();
		iter.next();
		
		while (iter.hasNext()) {
			Posicion posicion = iter.obtenerActual();
			iter.next();
			
			if(posicionAux.x() > posicion.x() ) {
				posicionAux.x(posicion.x());
			}
			
			if(posicionAux.y() > posicion.y() ) {
				posicionAux.y(posicion.y());
			}
		}
		return posicionAux;
	}*/
	
	public int origenX() {
		return origen.x();
	}
	
	public int origenY() {
		return origen.y();
	}

	public void reemplazar(Image reemplazo) {
		
		this.setImage(reemplazo);
	}
	
	public Colocable obtenerColocable() {
		return this.colocable;
	}
}
