package interfazGrafica.juegoEnEjecucion.grillaCentral;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import colocables.edificios.Edificio;
import colocables.unidades.Colocable;
import colocables.unidades.Unidad;
import interfazGrafica.JugadorFX;
import interfazGrafica.escenas.JuegoEnEjecucionPane;
import interfazGrafica.juegoEnEjecucion.grillaCentral.imagenesCliqueables.ImagenColocable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import jugadores.Jugador;
import mapa.Mapa;

public class GrillaDeImagenes extends Grilla {
    public GrillaDeImagenes(JuegoEnEjecucionPane juegoEnEjecucionPane, Mapa mapaASetear) {
        super(juegoEnEjecucionPane, mapaASetear);
        this.actualizar();
    }
    
    /*
     * PRE: Ninguna.
     * POST: Actualiza la grilla de imagenes para representar la situacion
     * del momento en el juego.
     */
    public void actualizar() {
        Node node = this.getChildren().get(0);
        
        this.getChildren().clear();
        this.getChildren().add(0, node);

        Set<Colocable> colocables = mapa.obtenerColocables();
        
        Iterator<Colocable> colocablesIter = colocables.iterator();

        while (colocablesIter.hasNext()) {

            Colocable colocable = colocablesIter.next();

            String ruta = "/interfazGrafica/recursos/" + colocable.getClass().getSimpleName() + ".png";
            String nombreDeColocable = colocable.getClass().getSimpleName();
            
            ImagenColocable imagen = new ImagenColocable(colocable, ruta);
            
            /*
             * LOGICA DE CAMBIO DE COLOR
             */
            cambiarColor(imagen); 
            
            
            /*
             * FIN LOGICA CAMBIO DE COLOR
             */
            
            this.add(
            		imagen, 
            		imagen.origenX(), 
            		imagen.origenY(), 
            		this.tamanioDeColocables.get(nombreDeColocable), 
            		this.tamanioDeColocables.get(nombreDeColocable)
            		);

        }
    }

    /*
     * PRE: Se le envia una imagen colocable que tenga dentro una imagen.
     * POST: Dependiendo del color de pixel y del jugador, este cambiara
     * a un determinado color.
     * 
     * (250,250,250) Cambiara a color base
     * (200,200,200) Cambiara a sombreado
     * (150,150,150) Cambiara a bordeado
     * 
     * Me gustaria dejarlo mejor, pero por ahora lo hardcodeo
     * 
     * ADEMAS, ESTA CADENA DE IFS ES HORRIBLE! COMO CAMBIO ESTO?!
     * 
     * ESTE ES UNO DE LOS PEORES CODIGOS QUE ESCRIBI EN MI VIDA!
     */
    
	private void cambiarColor(ImagenColocable imagenColocable) {
		
		List<Jugador> jugadores = this.juegoActual.obtenerJugadores();
		
		Image imagenIndividual = imagenColocable.getImage();
		Colocable colocableActual = imagenColocable.obtenerColocable();
		
		Image reemplazo = null;
		
		Jugador j1 = jugadores.get(0);
		Jugador j2 = jugadores.get(1);
		
		if(colocableActual instanceof Edificio) {
			Edificio edificioActual = (Edificio) colocableActual;
			
			if(j1.incluye(edificioActual)) {
				reemplazo = pintarImagen(imagenIndividual, j1);
			} else if (j2.incluye(edificioActual)) {
				reemplazo = pintarImagen(imagenIndividual, j2);
			}
			
		} else if (colocableActual instanceof Unidad) {
			Unidad unidadActual = (Unidad) colocableActual;
			
			if(j1.incluye(unidadActual)) {
				reemplazo = pintarImagen(imagenIndividual, j1);
			} else if (j2.incluye(unidadActual)) {
				reemplazo = pintarImagen(imagenIndividual, j2);
			}
			
		}
		
		
		/*for(int i = 0; i < imagenIndividual.getWidth(); i++) {
			for(int j = 0; j < imagenIndividual.getHeight(); j++) {
				
				Color color = lector.getColor(i, j);
				
				if(colocableActual instanceof Edificio) {
					
					if (j1.incluye((Edificio) colocableActual)) {
						
						if (color.equals(base)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j1).obtenerBase()
									);
							
						} else if (color.equals(sombreado)) {
							
							writer.setColor(i, j,
									JugadorFX.obtenerColores(j1).obtenerSombreado()
									);
							
						} else if (color.equals(bordeado)) {
							
							writer.setColor(i, j,
									JugadorFX.obtenerColores(j1).obtenerBordeado()
									);
							
						} else {
							writer.setColor(i, j, color);
						}
						
					} else {
						
						if (color.equals(base)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j2).obtenerBase()
									);
							
						} else if (color.equals(sombreado)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j2).obtenerSombreado()
									);
							
						} else if (color.equals(bordeado)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j2).obtenerBordeado()
									);
							
						} else {
							writer.setColor(i, j, color);
						}
						
					}
					
				} else if (colocableActual instanceof Unidad) {
					if (j1.incluye((Unidad) colocableActual)) {
						
						if (color.equals(base)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j1).obtenerBase()
									);
							
						} else if (color.equals(sombreado)) {
							
							writer.setColor(i, j,
									JugadorFX.obtenerColores(j1).obtenerSombreado()
									);
							
						} else if (color.equals(bordeado)) {
							
							writer.setColor(i, j,
									JugadorFX.obtenerColores(j1).obtenerBordeado()
									);
							
						} else {
							writer.setColor(i, j, color);
						}
						
					} else {
						
						if (color.equals(base)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j2).obtenerBase()
									);
							
						} else if (color.equals(sombreado)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j2).obtenerSombreado()
									);
							
						} else if (color.equals(bordeado)) {
							
							writer.setColor(i, j, 
									JugadorFX.obtenerColores(j2).obtenerBordeado()
									);
							
						} else {
							writer.setColor(i, j, color);
						}
						
					}
				}
				
				
				
			}
		}*/
		
		imagenColocable.reemplazar(reemplazo);
	}

	/*
	 * TODO: Separar esta logica, no se usa solo en grilla de imagenes. Se
	 * utiliza tambien para pintar el boton de fin de turno.
	 */
	
	public static Image pintarImagen(Image imagenIndividual, Jugador jugador) {
		
		Color base = Color.rgb(250,250,250);
		Color sombreado = Color.rgb(200,200,200);
		Color bordeado = Color.rgb(150,150,150);
		
		WritableImage output = new WritableImage(
				(int)imagenIndividual.getWidth(), 
				(int)imagenIndividual.getHeight()
				);
		
		PixelReader lector = imagenIndividual.getPixelReader();
		PixelWriter escritor = output.getPixelWriter();
		
		for(int i = 0; i < imagenIndividual.getWidth(); i++) {
			for(int j = 0 ; j < imagenIndividual.getHeight(); j++) {
				
				Color color = lector.getColor(i, j);
				
				if (color.equals(base)) {
					
					escritor.setColor(i, j, 
							JugadorFX.obtenerColores(jugador).obtenerBase()
							);
					
				} else if (color.equals(sombreado)) {
					
					escritor.setColor(i, j, 
							JugadorFX.obtenerColores(jugador).obtenerSombreado()
							);
					
				} else if (color.equals(bordeado)) {
					
					escritor.setColor(i, j, 
							JugadorFX.obtenerColores(jugador).obtenerBordeado()
							);
					
				} else {
					escritor.setColor(i, j, color);
				}
			}
		}
		
		return output;
	}
	
}