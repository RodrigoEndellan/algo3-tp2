package interfazGrafica.juegoEnEjecucion;


import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;

public class ConsolaDeJuego extends VBox {

	VBox content;
	public ConsolaDeJuego() {
		super();

		this.setMaxWidth(300);
		this.setMinWidth(170);
		this.setMaxHeight(Double.MAX_VALUE);

		BackgroundImage backImg = new BackgroundImage(
				new Image("/interfazGrafica/recursos/texturas/MapBackground.png"),
				BackgroundRepeat.REPEAT, 
				BackgroundRepeat.REPEAT, 
				BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT
		        );
		
		ScrollPane sp = new ScrollPane();
		
		
		this.content = new VBox();
		
		sp.setContent(content);
		
		this.content.setBackground(new Background(backImg));
		
		
		
		this.setBackground(new Background(backImg));
		sp.setBackground(new Background(backImg));
		sp.setStyle("-fx-background-color: transparent;"
				+ "-fx-background: transparent;");

		this.getChildren().add(sp);
		
	}

	public void agregarLabel(Label label){
		content.getChildren().add(label);
	}
}
