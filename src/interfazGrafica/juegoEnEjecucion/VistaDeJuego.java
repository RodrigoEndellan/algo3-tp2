package interfazGrafica.juegoEnEjecucion;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class VistaDeJuego extends VBox {

	ScrollPane content;
	
	public VistaDeJuego() {
		super();
		
		HBox hboxContainingVbox = new HBox();
		
		hboxContainingVbox.setAlignment(Pos.CENTER);
		
		this.setAlignment(Pos.CENTER);
		
		this.content = new ScrollPane();
		
		/*
		 * Map Background
		 */
		
		BackgroundImage backImg = new BackgroundImage(
				new Image("/interfazGrafica/recursos/texturas/MapBackground.png"),
				BackgroundRepeat.REPEAT, 
				BackgroundRepeat.REPEAT, 
				BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT
		        );
		
		this.setBackground(new Background(backImg));
		
		/*
		 * DRAG EVENTS
		 */
		this.content.setPannable(true);
		this.content.setHbarPolicy(ScrollBarPolicy.NEVER);
		this.content.setVbarPolicy(ScrollBarPolicy.NEVER);
		/*
		 * FIN DRAG EVENTS
		 */
		
		hboxContainingVbox.getChildren().add(this.content);
		
		this.getChildren().add(hboxContainingVbox);
	}
	
	public void setContent(Node node) {
		this.content.setContent(node);
	}
	
}
