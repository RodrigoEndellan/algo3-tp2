package interfazGrafica.juegoEnEjecucion;


import java.util.List;

import Juego.Juego;
import colocables.edificios.Edificio;
import colocables.unidades.Colocable;
import colocables.unidades.ColocableAtacable;
import colocables.unidades.Unidad;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeImagenes;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import jugadores.Jugador;

public class EstadoDeUnidad extends HBox {

	Colocable colocableActual = null;
	VBox descipcion;
	Juego juegoActual;
	
	public EstadoDeUnidad(Juego juegoActual) {
		super();
		
		this.juegoActual = juegoActual;
		this.descipcion = new VBox();
		
		this.setAlignment(Pos.TOP_LEFT);
		
		this.getChildren().add(this.descipcion);
	}

	private EstadoDeUnidad (Colocable colocableAMostrar) {
		super();
		this.colocableActual = colocableAMostrar;

		this.setAlignment(Pos.CENTER);
		
		Image imagen = new Image("interfazGrafica/recursos/" + colocableAMostrar.getClass().getSimpleName() + "50px.png");
		
		/*
		 * LOGICA DE CAMBIO DE COLOR
		 * TODO: Las unidades y los edificios deberian conocer a su jugador...
		 */
		List<Jugador> jugadores = this.juegoActual.obtenerJugadores();
		
		if(colocableAMostrar instanceof Edificio) {
			Edificio edificio = (Edificio) colocableAMostrar;
			if(jugadores.get(0).incluye(edificio)) {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(0));
			} else {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(1));
			}
		} else if (colocableAMostrar instanceof Unidad) {
			Unidad unidad = (Unidad) colocableAMostrar;
			if(jugadores.get(0).incluye(unidad)) {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(0));
			} else {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(1));
			}
		}
		
		ImageView ColocableThumbnail = new ImageView(imagen);
		
		Label unidad = new Label(colocableAMostrar.getClass().getSimpleName());
		
		Label vida = new Label( Integer.toString(( ((ColocableAtacable) colocableAMostrar).obtenerVidaActual()) ));
		
		vida.setTextFill(Paint.valueOf(Color.WHITE.toString()));
		unidad.setTextFill(Paint.valueOf(Color.WHITE.toString()));
		
		this.getChildren().addAll(ColocableThumbnail, unidad, vida);
	}
	
	public void mostrarColocable(Colocable colocableAMostrar) {
		this.getChildren().clear();
		this.descipcion.getChildren().clear();
		
		this.colocableActual = colocableAMostrar;

		Image imagen = new Image("interfazGrafica/recursos/" + colocableAMostrar.getClass().getSimpleName() + "50px.png");
		
		/*
		 * LOGICA DE CAMBIO DE COLOR
		 * TODO: Las unidades y los edificios deberian conocer a su jugador...
		 */
		List<Jugador> jugadores = this.juegoActual.obtenerJugadores();
		
		if(colocableAMostrar instanceof Edificio) {
			Edificio edificio = (Edificio) colocableAMostrar;
			if(jugadores.get(0).incluye(edificio)) {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(0));
			} else {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(1));
			}
		} else if (colocableAMostrar instanceof Unidad) {
			Unidad unidad = (Unidad) colocableAMostrar;
			if(jugadores.get(0).incluye(unidad)) {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(0));
			} else {
				imagen = GrillaDeImagenes.pintarImagen(imagen, jugadores.get(1));
			}
		}
		
		ImageView ColocableThumbnail = new ImageView(imagen);
	
		Label unidad = new Label(colocableAMostrar.getClass().getSimpleName());
		Label vida = new Label( 
				Integer.toString(( ((ColocableAtacable) colocableAMostrar).obtenerVidaActual()) )
				+ "/" +
				((ColocableAtacable) colocableAMostrar).obtenerVidaMaxima()
				);
		
		vida.setTextFill(Paint.valueOf(Color.WHITE.toString()));
		unidad.setTextFill(Paint.valueOf(Color.WHITE.toString()));
		
		this.descipcion.getChildren().addAll(unidad,vida);
		
		this.getChildren().addAll(ColocableThumbnail, this.descipcion);
	}

	public void actualizar() {
		if (this.colocableActual != null) {
			this.mostrarColocable(this.colocableActual);
		}
	}

}
