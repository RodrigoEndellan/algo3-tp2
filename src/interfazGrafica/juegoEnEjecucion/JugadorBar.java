package interfazGrafica.juegoEnEjecucion;


import interfazGrafica.JugadorFX;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import jugadores.Jugador;

public class JugadorBar extends VBox {

	Jugador jugador;
	Label nombre;
	Label oro;
	Label poblacion;
	
	public JugadorBar(Jugador jugador) {
		super();
		this.setMaxWidth(120);
		this.setMinWidth(120);
		/*
		 * ConsoleBar Background
		 */
		
		BackgroundImage backImg = new BackgroundImage(
				new Image("/interfazGrafica/recursos/texturas/JugadorBar.png"),
				BackgroundRepeat.REPEAT, 
				BackgroundRepeat.REPEAT, 
				BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT
		        );
		
		this.setBackground(new Background(backImg));
		
		this.jugador = jugador;
		
		
		this.nombre = new Label(JugadorFX.obtenerNombre(this.jugador));
		this.oro = new Label("Oro: " + jugador.cantOro() );
		this.poblacion = new Label("Poblacion " + jugador.cantPoblacion() + "/" + jugador.cantPoblacionMax());
		
		
		nombre.setTextFill(Paint.valueOf(
				
				JugadorFX.obtenerColores(this.jugador).obtenerBase().toString()
				
				));
		oro.setTextFill(Paint.valueOf(
				
				JugadorFX.obtenerColores(this.jugador).obtenerBase().toString()
				
				));
		poblacion.setTextFill(Paint.valueOf(
				
				JugadorFX.obtenerColores(this.jugador).obtenerBase().toString()
				
				));
		
		this.getChildren().add(this.nombre);
		this.getChildren().add(this.oro);
		this.getChildren().add(this.poblacion);
	}
	
	public void actualizar() {
		this.oro.setText("Oro: " + jugador.cantOro());
		this.poblacion.setText("Poblacion " + jugador.cantPoblacion() + "/" + jugador.cantPoblacionMax());
	}

}
