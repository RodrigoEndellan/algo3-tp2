package interfazGrafica.juegoEnEjecucion;


import Juego.Juego;
import colocables.unidades.Colocable;
import interfazGrafica.juegoEnEjecucion.botones.BotonFinDeTurno;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

public class ConsoleBar extends HBox {

	BotonFinDeTurno finDeTurno;
	ConsolaDeJuego consola;
	EstadoDeUnidad barraDeEstado;
	
	public ConsoleBar(Juego juegoActual) {
		super();
		this.setMaxHeight(100);
		this.setMinHeight(100);

		this.finDeTurno = new BotonFinDeTurno(juegoActual);
		this.barraDeEstado = new EstadoDeUnidad(juegoActual);
		this.consola = new ConsolaDeJuego();

		//consola.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		//barraDeEstado.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		//finDeTurno.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		/*
		 * ConsoleBar Background
		 */
		
		BackgroundImage backImg = new BackgroundImage(
				new Image("/interfazGrafica/recursos/texturas/ConsoleBar.png"),
				BackgroundRepeat.REPEAT, 
				BackgroundRepeat.REPEAT, 
				BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT
		        );
		
		this.setBackground(new Background(backImg));
		

		this.setHgrow(consola, Priority.ALWAYS);
		this.setHgrow(barraDeEstado, Priority.ALWAYS);
		this.setHgrow(finDeTurno, Priority.ALWAYS);

		this.getChildren().addAll(consola, barraDeEstado, finDeTurno);
	}

	public void agregarTexto(String string, String color){
		Label label = new Label(string);
		label.setTextFill(Paint.valueOf(color));
		consola.agregarLabel(label);
	}
	
	public EstadoDeUnidad obtenerBarraDeEstado() {
		return this.barraDeEstado;
	}

	public void mostrarColocable(Colocable colocableAMostrar) {
		this.barraDeEstado.mostrarColocable(colocableAMostrar);

	}

	public void actualizar() {
		this.barraDeEstado.actualizar();
		this.finDeTurno.actualizar();
	}

}
