package interfazGrafica.escenas;

import interfazGrafica.menuDeInicio.BotoneraMenuDeInicio;
import interfazGrafica.menuDeInicio.LogoGame;
import javafx.scene.layout.BorderPane;

public class InicioPane extends BorderPane {
	public InicioPane() {
		super();
		
		this.setMinWidth(640);
		this.setMinHeight(480);
		
		this.setTop(new LogoGame());
		this.setCenter(new BotoneraMenuDeInicio());
		
	}
}
