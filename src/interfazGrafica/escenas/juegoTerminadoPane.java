package interfazGrafica.escenas;



import interfazGrafica.JugadorFX;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import jugadores.Jugador;

public class juegoTerminadoPane extends StackPane {
	public juegoTerminadoPane(Jugador ganador) {
		Label nombreGanador = new Label("Gano " + JugadorFX.obtenerNombre(ganador) + "! Haz click con el mouse para continuar");
		
		this.getChildren().add(nombreGanador);
		
		this.setOnMouseClicked(e->{
			
			this.getScene().setRoot(new InicioPane());
		});
		
	}
}
