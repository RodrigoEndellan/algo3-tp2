package interfazGrafica.escenas;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Juego.Juego;
import colocables.edificios.Castillo;
import colocables.edificios.Cuartel;
import colocables.edificios.NoSeConstruyeEnMismaPosicionException;
import colocables.edificios.NoSeConstruyeEnPosicionAlejadaException;
import colocables.edificios.PlazaCentral;
import colocables.estados.EstaOcupadoException;
import colocables.estados.estadosDeReparacion.EstaFueraDeRango;
import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.NoEsUnEdificioException;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import colocables.unidades.Aldeano;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.AtaqueFueraDeRangoException;
import colocables.unidades.Colocable;
import colocables.unidades.ColocableAtacable;
import colocables.unidades.MovimientoInvalidoException;
import colocables.unidades.NoHaySuficienteOroException;
import colocables.unidades.PoblacionMaximaAlcanzadaException;
import colocables.unidades.UnidadDeAtaque;
import geometria.Posicion;
import interfazGrafica.JugadorFX;
import interfazGrafica.juegoEnEjecucion.ConsoleBar;
import interfazGrafica.juegoEnEjecucion.JugadorBar;
import interfazGrafica.juegoEnEjecucion.TopBar;
import interfazGrafica.juegoEnEjecucion.VistaDeJuego;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeImagenes;
import interfazGrafica.juegoEnEjecucion.grillaCentral.GrillaDeJuego;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.AudioClip;
import jugadores.EdificioNoPerteneceJugadorExcepcion;
import jugadores.Jugador;
import jugadores.UnidadNoPerteneceJugadorExcepcion;

public class JuegoEnEjecucionPane extends BorderPane {

	Juego juegoActual;
	List<Jugador> jugadores;
	Map<Jugador, String> nombre = new HashMap<>();
	
	GrillaDeJuego grillaDeJuego;
	GrillaDeImagenes grillaDeImagenes;
	TopBar topBar;
	ConsoleBar consoleBar;
	JugadorBar jugadorBarLeft;
	JugadorBar jugadorBarRight;

	public JuegoEnEjecucionPane(Juego juego) {

		juegoActual = juego;
		jugadores = juegoActual.obtenerJugadores();

		
		/* LA CONSOLA TIENE QUE ESTAR ACA ARRIBA!!!*/
		this.consoleBar = new ConsoleBar(juegoActual);
		this.setBottom(this.consoleBar);
		
		this.jugadorBarLeft = new JugadorBar(jugadores.get(0));
		this.setLeft(this.jugadorBarLeft);
		
		this.jugadorBarRight = new JugadorBar(jugadores.get(1));
		this.setRight( this.jugadorBarRight );
		/*Fin hardcodeado por ahora*/

		StackPane grillaJuegoSuperpuesta = new StackPane();
		
		this.grillaDeImagenes = new GrillaDeImagenes(this, juegoActual.obtenerMapa());
		this.grillaDeJuego = new GrillaDeJuego(this, juegoActual.obtenerMapa(), this.consoleBar);

		/*Fondo de grilla de imagenes*/
		Background backgroundImagenes = new Background(
				new BackgroundImage(
						new Image(
								"/interfazGrafica/recursos/suelo/sueloVerde.png"), 
						BackgroundRepeat.REPEAT, 
						BackgroundRepeat.REPEAT, 
						BackgroundPosition.CENTER, 
						BackgroundSize.DEFAULT)
				);
		
		
		grillaDeImagenes.setBackground(backgroundImagenes);

		grillaJuegoSuperpuesta.getChildren().add(this.grillaDeImagenes);
		grillaJuegoSuperpuesta.getChildren().add(this.grillaDeJuego);

		VistaDeJuego vista = new VistaDeJuego();
		vista.setContent(grillaJuegoSuperpuesta);
		
		this.setCenter(vista);
		
		
		this.topBar = new TopBar(nombre, juegoActual);
		this.setTop(this.topBar);

		this.grillaDeJuego.actualizar();
	}
	
	/*
	 * Llama a la escena a actualizar, cambia el estado de todos los paneles.
	 */
	public void actualizar() {
		this.grillaDeJuego.actualizar();
		this.grillaDeImagenes.actualizar();
		this.jugadorBarLeft.actualizar();
		this.jugadorBarRight.actualizar();
		this.topBar.actualizar();
		this.consoleBar.actualizar();
	}

	public List<Jugador> obtenerJugadores(){
		return this.juegoActual.obtenerJugadores();
	}

	/* INICIA INTERACCION ESCENA CON JUEGO */
	public void reparar(Aldeano aldeano, Colocable colocable) {
		
		
		
		try {
			juegoActual.reparar(aldeano, colocable);
		} catch (EstaFueraDeRango estaFueraDeRango) {
			consoleBar.agregarTexto("Lo mandado a reparar esta fuera de rango",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaSiendoReparadaPorUnAldeanoException e) {
			consoleBar.agregarTexto("Lo mandado a reparar esta siendo reaparado por otro aldeano",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (NoNecesitaReparacionExeption noNecesitaReparacionExeption) {
			consoleBar.agregarTexto("Lo mandado a reparar no necesita reparacion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally{
			this.actualizar();
		}
	}

	public void atacar(UnidadDeAtaque unidadDeAtaque, ColocableAtacable colocableAtacable) {
		try{
			juegoActual.atacar(unidadDeAtaque, colocableAtacable);
		}catch (AtaqueFueraDeRangoException e){
			consoleBar.agregarTexto("El ataque esta fuera de rango",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}catch (MovimientoInvalidoException e){
			consoleBar.agregarTexto("No se puede realizar el ataque, fijarse el estado de la unidad",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally{
			this.actualizar();
		}
	}

	public void construirPlazaCentral(Aldeano miAldeano, Posicion miPosicion) {
		try {
			juegoActual.construirPlazaCentral(miAldeano, miPosicion);
		} catch (NoHaySuficienteOroException e) {
			consoleBar.agregarTexto("No hay suficiente oro para crear la unidad",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaOcupadoException e) {
			consoleBar.agregarTexto("La unidad esta opcupada",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (NoSeConstruyeEnPosicionAlejadaException e) {
			consoleBar.agregarTexto("NoSeConstruyeEnPosicionAlejadaException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (NoSeConstruyeEnMismaPosicionException e) {
			consoleBar.agregarTexto("NoSeConstruyeEnMismaPosicionException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (UnidadNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("UnidadNoPerteneceJugadorExcepcion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}
		finally {
			this.actualizar();
		}
	}

	public void construirCuartel(Aldeano aldeano, Posicion posicion) {
		try {
			juegoActual.construirCuartel(aldeano, posicion);
		} catch (NoHaySuficienteOroException e) {
			consoleBar.agregarTexto("NoHaySuficienteOroException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaOcupadoException e) {
			consoleBar.agregarTexto("EstaOcupadoException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (NoSeConstruyeEnPosicionAlejadaException e) {
			consoleBar.agregarTexto("NoSeConstruyeEnPosicionAlejadaException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (NoSeConstruyeEnMismaPosicionException e) {
			consoleBar.agregarTexto("NoSeConstruyeEnMismaPosicionException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (UnidadNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("UnidadNoPerteneceJugadorExcepcion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} finally {
			this.actualizar();
		}
	}

	public void crearAsedio(Castillo castillo) {
		try {
			juegoActual.crearAsedio(castillo);
		} catch (NoHaySuficienteOroException e) {
			consoleBar.agregarTexto("NoHaySuficienteOroException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EdificioNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("Edificio no pertenece al jugador",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaOcupadoException e) {
			consoleBar.agregarTexto("EstaOcupadoException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}catch (PoblacionMaximaAlcanzadaException e){
			consoleBar.agregarTexto("Llegaste a poblacion maxima",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally {
			this.actualizar();
		}
	}

	public void crearEspadachin(Cuartel cuartel) {
		try {
			juegoActual.crearEspadachin(cuartel);
		} catch (NoHaySuficienteOroException e) {
			consoleBar.agregarTexto("NoHaySuficienteOroException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EdificioNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("EdificioNoPerteneceJugadorExcepcion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaOcupadoException e) {
			consoleBar.agregarTexto("EstaOcupadoException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}catch (PoblacionMaximaAlcanzadaException e){
			consoleBar.agregarTexto("Llegaste a poblacion maxima",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally {
			this.actualizar();
		}
	}

	public void crearArquero(Cuartel cuartel) {
		try {
			juegoActual.crearArquero(cuartel);
		} catch (NoHaySuficienteOroException e) {
			consoleBar.agregarTexto("NoHaySuficienteOroException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EdificioNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("EdificioNoPerteneceJugadorExcepcion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaOcupadoException e) {
			consoleBar.agregarTexto("EstaOcupadoException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}catch (PoblacionMaximaAlcanzadaException e){
			consoleBar.agregarTexto("Llegaste a poblacion maxima",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally {
			this.actualizar();
		}
	}

	public void crearAldeano(PlazaCentral plazaCentral) {
		try {
			juegoActual.crearAldeano(plazaCentral);
		} catch (NoHaySuficienteOroException e) {
			consoleBar.agregarTexto("NoHaySuficienteOroException",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EdificioNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("El Edificio no pertenece alJugador de turno",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (EstaOcupadoException e) {
			consoleBar.agregarTexto("El edificio esta ocupado",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}catch (PoblacionMaximaAlcanzadaException e){
			consoleBar.agregarTexto("Llegaste a poblacion maxima",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally {
			this.actualizar();
		}
	}

	public void mover(Colocable colocable, Posicion pos) {
		try{
			juegoActual.mover(colocable, pos);
		} catch (mapa.PosicionInvalidaException | MovimientoInvalidoException e){
			consoleBar.agregarTexto("Movimiento invalido",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		} catch (UnidadNoPerteneceJugadorExcepcion e) {
			consoleBar.agregarTexto("UnidadNoPerteneceJugadorExcepcion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}finally {
			this.actualizar();
		}
	}

	public void cambiarMontura(ArmaDeAsedio arma) {
		try {
			juegoActual.cambiarMontura(arma);
		} catch (UnidadNoPerteneceJugadorExcepcion unidadNoPerteneceJugadorExcepcion) {
			consoleBar.agregarTexto("UnidadNoPerteneceJugadorExcepcion",
					JugadorFX.obtenerColores(juegoActual.obtenerJugadorDeTurno()).obtenerBase().toString());
		}
	}

	/* FINALIZA INTERACCION ESCENA CON JUEGO */
}
