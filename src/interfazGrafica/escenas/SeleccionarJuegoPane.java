package interfazGrafica.escenas;

import interfazGrafica.SeleccionDeJuego.BotoneraAtras;
import interfazGrafica.SeleccionDeJuego.BotoneraSeleccionDeJuego;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class SeleccionarJuegoPane extends StackPane {
	public SeleccionarJuegoPane() {
		super();
		
		
		
		this.setAlignment(Pos.CENTER);
		
		BorderPane seleccionDeJuego = new BorderPane();
		
		seleccionDeJuego.setCenter(new BotoneraSeleccionDeJuego());
		
		seleccionDeJuego.setBottom(new BotoneraAtras());
		
		/*
		 * INICIO ALINEACIO CENTRO
		 */
		
		VBox alineacionVertical = new VBox();
		HBox alineacionHorizontal = new HBox();
		
		alineacionVertical.setAlignment(Pos.CENTER);
		alineacionHorizontal.setAlignment(Pos.CENTER);
		
		alineacionHorizontal.getChildren().add(seleccionDeJuego);
		alineacionVertical.getChildren().add(alineacionHorizontal);
		
		/*
		 * FIN ALINEACION CENTRO
		 */
		
		this.getChildren().add(alineacionVertical);
		
	}
}
