package interfazGrafica.SeleccionDeJuego.colores;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ColorDeEquipo extends Rectangle {
	Color base;
	Color sombreado;
	Color bordeado;
	
	public ColorDeEquipo(Color base, Color sombreado, Color bordeado) {
		super(16,16,base);
		
		this.base = base;
		this.sombreado = sombreado;
		this.bordeado = bordeado;
	}
	
	public static final ColorDeEquipo AZUL = new ColorDeEquipo(
			Color.rgb(0,0,255), 
			Color.rgb(0,0,200), 
			Color.rgb(0,0,140)
			);
	
	public static final ColorDeEquipo ROJO = new ColorDeEquipo(
			Color.rgb(255,0,0), 
			Color.rgb(200,0,0), 
			Color.rgb(140,0,0)
			);
	
	public static final ColorDeEquipo VERDE = new ColorDeEquipo(
			Color.rgb(0,240,0), 
			Color.rgb(0,120,0), 
			Color.rgb(0,90,0)
			);
	
	public static final ColorDeEquipo ROSA = new ColorDeEquipo(
			Color.rgb(255,105,180), 
			Color.rgb(255,20,147), 
			Color.rgb(199,21,133)
			);
	
	public static final ColorDeEquipo NARANJA = new ColorDeEquipo(
			Color.rgb(255,195,96), 
			Color.rgb(255,244,163), 
			Color.rgb(228,169,93)
			);
	
	public static final ColorDeEquipo CYAN = new ColorDeEquipo(
			Color.rgb(155,219,245), 
			Color.rgb(107,171,218), 
			Color.rgb(92,150,201)
			);
	
	public static final ColorDeEquipo PURURA = new ColorDeEquipo(
			Color.rgb(204,156,223), 
			Color.rgb(164,107,189), 
			Color.rgb(145,86,169)
			);
	
	public static final ColorDeEquipo BLANCO = new ColorDeEquipo(
			Color.rgb(234,238,240), 
			Color.rgb(223,228,227), 
			Color.rgb(189,193,194)
			);
	
	public static final ColorDeEquipo AMARILLO = new ColorDeEquipo(
			Color.rgb(250,245,171), 
			Color.rgb(243,228,136), 
			Color.rgb(233,212,97)
			);
	
	public Color obtenerBase() {
		return this.base;
	}
	
	public Color obtenerSombreado() {
		return this.sombreado;
	}
	
	public Color obtenerBordeado() {
		return this.bordeado;
	}

}
