package interfazGrafica.SeleccionDeJuego.botones;

import Juego.inicializadores.InicializadorDemostracionCastillo;
import javafx.scene.control.Button;

public class BotonIniciarDemostracionCastillo extends Button{   
	 public BotonIniciarDemostracionCastillo() {
		 super();
		 this.setText("Demostracion De Castillo");
		 this.setOnAction(new SeleccionDeJugadorEventHandler(this, new InicializadorDemostracionCastillo()));
	 }
}
