package interfazGrafica.SeleccionDeJuego.botones;

import Juego.inicializadores.InicializadorLimiteDePoblacion;
import javafx.scene.control.Button;

public class BotonIniciarCantMaxPoblacion  extends Button {

    public BotonIniciarCantMaxPoblacion() {
        super();

        this.setText("Demostracion De Cantidad Maxima De Poblacion");

        //BotonCantMaxPoblacion iniciarJuego = new BotonCantMaxPoblacion(this);
        this.setOnAction(new SeleccionDeJugadorEventHandler(this, new InicializadorLimiteDePoblacion()));

    }
}
