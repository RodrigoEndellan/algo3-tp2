package interfazGrafica.SeleccionDeJuego.botones;

import Juego.inicializadores.Inicializador;
import interfazGrafica.SeleccionDeJuego.SplashSeleccionJugador;
import interfazGrafica.escenas.SeleccionarJuegoPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class SeleccionDeJugadorEventHandler	implements EventHandler<ActionEvent> {

	Button boton;
	Inicializador init;
	
	public SeleccionDeJugadorEventHandler(Button boton, Inicializador init) {
		super();
		
		this.boton = boton;
		this.init = init;
	}
	
	@Override
	public void handle(ActionEvent event) {
		
		((SeleccionarJuegoPane) boton.getScene().getRoot()).getChildren().add(
				
				new SplashSeleccionJugador(this.init)
				
				);
		
	}

}
