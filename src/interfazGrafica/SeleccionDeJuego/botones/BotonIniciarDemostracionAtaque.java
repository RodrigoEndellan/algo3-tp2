package interfazGrafica.SeleccionDeJuego.botones;

import Juego.inicializadores.InicializadorDemostracionAtaque;
import javafx.scene.control.Button;

public class BotonIniciarDemostracionAtaque extends Button {

    public BotonIniciarDemostracionAtaque() {
        super();

        this.setText("Demostracion De Ataque");

        //BotonDemostracionDeAtaque iniciarJuego = new BotonDemostracionDeAtaque(this);
        this.setOnAction(new SeleccionDeJugadorEventHandler(this, new InicializadorDemostracionAtaque()));

    }
}
