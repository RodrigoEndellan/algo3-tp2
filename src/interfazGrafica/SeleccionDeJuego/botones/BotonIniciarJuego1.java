package interfazGrafica.SeleccionDeJuego.botones;

import Juego.inicializadores.InicializadorNormal;
import javafx.scene.control.Button;

public class BotonIniciarJuego1 extends Button {
	
	public BotonIniciarJuego1() {
		super();
		
		this.setText("Modo de juego 1");
		
		//IniciarJuego1EventHandler IniciarJuego = new IniciarJuego1EventHandler(this);
		
		this.setOnAction(new SeleccionDeJugadorEventHandler(this, new InicializadorNormal()));

	}
	
}
