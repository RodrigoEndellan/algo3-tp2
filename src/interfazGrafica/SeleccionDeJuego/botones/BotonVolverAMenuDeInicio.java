package interfazGrafica.SeleccionDeJuego.botones;

import interfazGrafica.SeleccionDeJuego.eventos.BotonVolverAMenuDeInicioEventHandler;
import javafx.scene.control.Button;

public class BotonVolverAMenuDeInicio {
	
	public static Button aniadirBoton() {
		Button boton = new Button();
		
		boton.setText("Atras");
		
		BotonVolverAMenuDeInicioEventHandler eventHandler = new BotonVolverAMenuDeInicioEventHandler(boton);
		
		boton.setOnAction(eventHandler);
		
		return boton;
	}
	
}
