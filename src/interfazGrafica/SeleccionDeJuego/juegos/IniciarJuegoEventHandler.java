package interfazGrafica.SeleccionDeJuego.juegos;

import java.util.List;

import Juego.Juego;
import Juego.inicializadores.Inicializador;
import interfazGrafica.JugadorFX;
import interfazGrafica.SeleccionDeJuego.InputJugadorFX;
import interfazGrafica.escenas.JuegoEnEjecucionPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import jugadores.Jugador;

public class IniciarJuegoEventHandler implements EventHandler<ActionEvent> {

		Button boton;
		Inicializador init;
		InputJugadorFX j1;
		InputJugadorFX j2;
	
	public IniciarJuegoEventHandler(Button boton, 
			Inicializador init, 
			InputJugadorFX j1, 
			InputJugadorFX j2) {
		
		this.boton = boton;
		this.init = init;
		this.j1 = j1;
		this.j2 = j2;
		
	}
	
	@Override
	public void handle(ActionEvent event) {
		//Inicio el juego
			Juego nuevoJuego = new Juego(init);
			
			//Seteo jugadores
			List<Jugador> jugadores = nuevoJuego.obtenerJugadores();
			
			JugadorFX.agregar(jugadores.get(0), j1.obtenerCustom());
			JugadorFX.agregar(jugadores.get(1), j2.obtenerCustom());
			
			//Creo el nuevo juego
			boton.getScene().setRoot(new JuegoEnEjecucionPane(nuevoJuego));
	}
	
}
