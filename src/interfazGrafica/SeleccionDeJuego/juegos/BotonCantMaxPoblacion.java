package interfazGrafica.SeleccionDeJuego.juegos;

import java.util.List;

import Juego.Juego;
import Juego.inicializadores.InicializadorLimiteDePoblacion;
import interfazGrafica.JugadorFX;
import interfazGrafica.SeleccionDeJuego.InputJugadorFX;
import interfazGrafica.escenas.JuegoEnEjecucionPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import jugadores.Jugador;

public class BotonCantMaxPoblacion implements EventHandler<ActionEvent> {

    Button miBoton;
    InputJugadorFX j1;
    InputJugadorFX j2;

    public BotonCantMaxPoblacion(Button boton, InputJugadorFX j1, InputJugadorFX j2) {
        miBoton = boton;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
    	
    	Juego nuevoJuego = new Juego(new InicializadorLimiteDePoblacion());
    	
    	//Seteo jugadores
		List<Jugador> jugadores = nuevoJuego.obtenerJugadores();
		
		JugadorFX.agregar(jugadores.get(0), j1.obtenerCustom());
		JugadorFX.agregar(jugadores.get(1), j2.obtenerCustom());
    	
        miBoton.getScene().setRoot(new JuegoEnEjecucionPane(nuevoJuego));
    }
}
