package interfazGrafica.SeleccionDeJuego.juegos;

import Juego.Juego;
import Juego.inicializadores.InicializadorDemostracionAtaque;
import interfazGrafica.SeleccionDeJuego.InputJugadorFX;
import interfazGrafica.escenas.JuegoEnEjecucionPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class BotonDemostracionDeAtaque implements EventHandler<ActionEvent> {

    Button miBoton;
    InputJugadorFX j1;
    InputJugadorFX j2;

    public BotonDemostracionDeAtaque(Button boton, InputJugadorFX j1, InputJugadorFX j2) {
        miBoton = boton;
        this.j1 = j1;
        this.j2 = j2;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
    	
    	Juego nuevoJuego = new Juego(new InicializadorDemostracionAtaque());
    	
        miBoton.getScene().setRoot(new JuegoEnEjecucionPane(nuevoJuego));
    }
}
