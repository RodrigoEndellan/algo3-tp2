package interfazGrafica.SeleccionDeJuego;

import interfazGrafica.SeleccionDeJuego.botones.BotonVolverAMenuDeInicio;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

public class BotoneraAtras extends VBox {

	public BotoneraAtras() {
		super();
		
		this.setAlignment(Pos.BOTTOM_LEFT);
		
		this.getChildren().add(BotonVolverAMenuDeInicio.aniadirBoton());
		
	}

}
