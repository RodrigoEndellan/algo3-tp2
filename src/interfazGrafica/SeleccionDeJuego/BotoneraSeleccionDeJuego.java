package interfazGrafica.SeleccionDeJuego;

import interfazGrafica.SeleccionDeJuego.botones.BotonIniciarCantMaxPoblacion;
import interfazGrafica.SeleccionDeJuego.botones.BotonIniciarDemostracionAtaque;
import interfazGrafica.SeleccionDeJuego.botones.BotonIniciarDemostracionCastillo;
import interfazGrafica.SeleccionDeJuego.botones.BotonIniciarJuego1;
import javafx.scene.layout.VBox;

public class BotoneraSeleccionDeJuego extends VBox {

	public BotoneraSeleccionDeJuego() {
		super();
		
		this.getChildren().add(new BotonIniciarJuego1());
		this.getChildren().add(new BotonIniciarDemostracionAtaque());
		this.getChildren().add(new BotonIniciarCantMaxPoblacion());
		this.getChildren().add(new BotonIniciarDemostracionCastillo());
		
		
	}

}
