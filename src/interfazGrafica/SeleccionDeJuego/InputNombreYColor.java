package interfazGrafica.SeleccionDeJuego;

import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class InputNombreYColor extends HBox {
	
	TextField nombre;
	SeleccionDeColor colores;
	
	public InputNombreYColor() {
		this.nombre = new TextField();
		this.colores = new SeleccionDeColor();
		
		this.getChildren().addAll(nombre, colores);
	}

	public CustomJugador obtenerCustom() {
		
		return new CustomJugador(
				this.nombre.getText(), 
				this.colores.getSelectionModel().getSelectedItem()
				);
	}
}
