package interfazGrafica.SeleccionDeJuego.eventos;

import interfazGrafica.escenas.InicioPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class BotonVolverAMenuDeInicioEventHandler implements EventHandler<ActionEvent> {

private Button boton;
	
	public BotonVolverAMenuDeInicioEventHandler(Button boton) {
		this.boton = boton;
	}
	
	@Override
	public void handle(ActionEvent actionEvent) {
        boton.getScene().setRoot(new InicioPane());
	}
}
