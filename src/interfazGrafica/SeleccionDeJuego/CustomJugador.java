package interfazGrafica.SeleccionDeJuego;

import interfazGrafica.SeleccionDeJuego.colores.ColorDeEquipo;

public class CustomJugador {
	String nombre;
	ColorDeEquipo colores;
	
	public CustomJugador(String nombre, ColorDeEquipo colores) {
		this.nombre = nombre;
		this.colores = colores;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setColor(ColorDeEquipo colores) {
		this.colores = colores;
	}

	public String obtenerNombre() {
		return nombre;
	}

	public ColorDeEquipo obtenerColores() {
		return colores;
	}
}
