package interfazGrafica.SeleccionDeJuego;

import Juego.inicializadores.Inicializador;
import interfazGrafica.SeleccionDeJuego.juegos.IniciarJuegoEventHandler;
import interfazGrafica.escenas.SeleccionarJuegoPane;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class SplashSeleccionJugador extends HBox {
	
	public SplashSeleccionJugador(Inicializador inicializador) {
		super();
		
		this.setAlignment(Pos.CENTER);
		
		InputJugadorFX inputJugador1 = new InputJugadorFX();
		InputJugadorFX inputJugador2 = new InputJugadorFX();
		
		Button iniciar = new Button("Iniciar");
		
		VBox vBoxContenedor = new VBox();
		vBoxContenedor.setAlignment(Pos.CENTER);
		vBoxContenedor.setBackground(new Background(new BackgroundFill(Color.BROWN, null, null)));
		vBoxContenedor.getChildren().addAll(inputJugador1, inputJugador2, iniciar);
		
		this.getChildren().addAll(vBoxContenedor);
		
		this.requestFocus();
		
		IniciarJuegoEventHandler iniciarJuego = new IniciarJuegoEventHandler(iniciar, inicializador, inputJugador1, inputJugador2);
		
		iniciar.setOnAction(iniciarJuego);
		
		this.setOnKeyPressed( ke ->{
			KeyCode code = ke.getCode();
			if(code.equals(KeyCode.ESCAPE)) {
				InputJugadorFX.reset();
				((SeleccionarJuegoPane) this.getScene().getRoot()).getChildren().remove(this);
			}
			
		});
		
	}
}
