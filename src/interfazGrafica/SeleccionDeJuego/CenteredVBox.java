package interfazGrafica.SeleccionDeJuego;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CenteredVBox extends HBox {
	public CenteredVBox() {
		super();
		
		this.setAlignment(Pos.CENTER);
		
		VBox centered = new VBox();
		centered.setAlignment(Pos.CENTER);
		
		this.getChildren().add(centered);
	}
}
