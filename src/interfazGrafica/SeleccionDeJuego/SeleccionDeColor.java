package interfazGrafica.SeleccionDeJuego;


import interfazGrafica.SeleccionDeJuego.colores.ColorDeEquipo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

public class SeleccionDeColor extends ComboBox<ColorDeEquipo> {
	

	
	public SeleccionDeColor() {
		super();
		
		ObservableList<ColorDeEquipo> options = FXCollections.observableArrayList();
		
		options.addAll(
				ColorDeEquipo.ROJO,
				ColorDeEquipo.AZUL,
				ColorDeEquipo.VERDE,
				ColorDeEquipo.ROSA,
				ColorDeEquipo.AMARILLO,
				ColorDeEquipo.BLANCO,
				ColorDeEquipo.CYAN,
				ColorDeEquipo.NARANJA,
				ColorDeEquipo.PURURA
				
				);
		
		this.setItems(options);
		
	}


}
