package interfazGrafica.SeleccionDeJuego;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class InputJugadorFX extends VBox {
	
	static private int cantidadDeJugadores = 0;
	
	InputNombreYColor input;
	
	public InputJugadorFX() {
		super();
		
		InputJugadorFX.cantidadDeJugadores++;
		
		Label jugador = new Label("Jugador" + InputJugadorFX.cantidadDeJugadores);
		this.input = new InputNombreYColor();
		
		this.getChildren().add(jugador);
		this.getChildren().add(input);
		
	}
	
	public static void reset() {
		InputJugadorFX.cantidadDeJugadores = 0;
	}

	public CustomJugador obtenerCustom() {
		return input.obtenerCustom();
	}
}
