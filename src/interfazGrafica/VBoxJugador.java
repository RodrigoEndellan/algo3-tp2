package interfazGrafica;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import jugadores.Jugador;


public class VBoxJugador extends VBox{

    Jugador jugador;

    Label nombreJugador;
    Label oro;
    Label poblacion;


    public VBoxJugador (Jugador jugadorSetear, int numJugador){
        super();

        jugador = jugadorSetear;

        nombreJugador = new Label("Jugador: " + Integer.toString(numJugador));
        oro = new Label("Oro: " + Integer.toString(jugador.cantOro()));
        poblacion = new Label("Poblacion " + Integer.toString(jugador.cantPoblacion()) + "/" + Integer.toString(jugador.cantPoblacionMax()));

        this.getChildren().addAll(nombreJugador, oro, poblacion);
    }

    public void actualizar(){

        oro.setText("Oro: " + Integer.toString(jugador.cantOro()));
        poblacion.setText("Poblacion " + Integer.toString(jugador.cantPoblacion()) + "/" + Integer.toString(jugador.cantPoblacionMax()));
    }
}
