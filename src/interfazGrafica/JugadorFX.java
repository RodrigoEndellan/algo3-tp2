package interfazGrafica;

import java.util.HashMap;

import interfazGrafica.SeleccionDeJuego.CustomJugador;
import interfazGrafica.SeleccionDeJuego.colores.ColorDeEquipo;
import jugadores.Jugador;

public class JugadorFX {
	
	static HashMap<Jugador, CustomJugador> jugadores = new HashMap<>();

	public static void agregar(Jugador jugador, CustomJugador customizacion) {
		
		JugadorFX.jugadores.put(jugador, customizacion);
		
	}

	public static String obtenerNombre(Jugador jugador) {
		return jugadores.get(jugador).obtenerNombre();
	}

	public static ColorDeEquipo obtenerColores(Jugador jugador) {
		return jugadores.get(jugador).obtenerColores();
	}
	
}
