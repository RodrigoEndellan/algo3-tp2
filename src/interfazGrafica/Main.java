package interfazGrafica;

import interfazGrafica.escenas.InicioPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
 * BIG TODO: HACER QUE TODOS LOS TAMANIOS SEAN PROPORCIONALES
 * Y NO FIJOS
 */

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
    	//Primero seteo el escenario
        stage.setTitle("Age of Empires(papu)");
        
        stage.setMinWidth(640);
        stage.setMinHeight(480);
        
        //Luego creo una escena
        Scene scene = new Scene(new InicioPane());
        
        //Mando la escena al escenario
        stage.setScene(scene);
        
        //Muestro. A partir de ahora, se intercambian las escenas, y el Stage quda para siempre.
        stage.show();

    }

}
