package interfazGrafica.menuDeInicio;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class LogoGame extends VBox {
	public LogoGame() {
		this.setAlignment(Pos.TOP_CENTER);
		ImageView logo = new ImageView(new Image("interfazGrafica/recursos/menuDeInicio/logo.png"));
		this.getChildren().add(logo);
	}
}
