package interfazGrafica.menuDeInicio;

import interfazGrafica.menuDeInicio.botones.BotonCreditos;
import interfazGrafica.menuDeInicio.botones.BotonIniciar;
import interfazGrafica.menuDeInicio.botones.BotonSalir;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;


public class BotoneraMenuDeInicio extends VBox {
	
	public BotoneraMenuDeInicio() {
		
		super();
		
		this.setAlignment(Pos.BOTTOM_CENTER);
		
		this.getChildren().add(new BotonIniciar());
		this.getChildren().add(new BotonCreditos());
		this.getChildren().add(new BotonSalir());
		
	}
	
}
