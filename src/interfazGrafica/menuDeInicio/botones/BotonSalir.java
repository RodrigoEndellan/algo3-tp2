package interfazGrafica.menuDeInicio.botones;

import javafx.application.Platform;
import javafx.scene.control.Button;

public class BotonSalir extends Button {

	public BotonSalir() {
		super();
		
		this.setText("Salir");
		
		this.setOnAction( e-> Platform.exit());

	}
}
