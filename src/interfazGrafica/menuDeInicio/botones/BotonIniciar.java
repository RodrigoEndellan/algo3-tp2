package interfazGrafica.menuDeInicio.botones;

import interfazGrafica.menuDeInicio.eventos.NuevaPartidaEventHandler;
import javafx.scene.control.Button;

public class BotonIniciar extends Button {
	
	public BotonIniciar() {
		super();
		
		this.setText("Nueva Partida");
		
		NuevaPartidaEventHandler nuevaPartida = new NuevaPartidaEventHandler(this);
		
		this.setOnAction(nuevaPartida);
	}
	
}
