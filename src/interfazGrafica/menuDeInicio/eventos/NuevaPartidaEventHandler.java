package interfazGrafica.menuDeInicio.eventos;

import interfazGrafica.escenas.SeleccionarJuegoPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class NuevaPartidaEventHandler implements EventHandler<ActionEvent> {
	
	private Button boton;
	
	public NuevaPartidaEventHandler(Button boton) {
		this.boton = boton;
	}
	
	@Override
	public void handle(ActionEvent actionEvent) {
        boton.getScene().setRoot(new SeleccionarJuegoPane());
	}

}
