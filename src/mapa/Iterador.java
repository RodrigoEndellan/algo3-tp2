package mapa;

import geometria.Posicion;

public interface Iterador {

    public boolean hasNext();
    public int next();
    public Posicion obtenerActual();
}
