package mapa;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import colocables.unidades.Colocable;
import geometria.Posicion;
import geometria.Posiciones;

public class Mapa {
	
	Map<Colocable, Posiciones > posicionesOcupadas;
	int tamanio;

	/*
	 * PRE: Tamanio es mayor a cero
	 * POST: Crea un mapa cuadrado de dimensiones tamanio*tamanio.
	 * Si el tamanio es menor a cero, se toma el modulo.
	 * 
	 * TODO: Establecer un tamanio minimo de mapa. Si se intenta crear algo por debajo
	 * del tamanio minimo, deberia usar el tamanio minimo.
	 */
	public Mapa(int tamanio) {
		posicionesOcupadas = new HashMap<>();
		this.tamanio = tamanio;
	}

	/*
	 * PRE: Se pasa por parametro un colocable y una posicion mayor a cero
	 * y menor a tamanio - 1 en todos los ejes.
	 * POST: Si la posicion no esta ocupada, coloca el objeto. Caso contrario,
	 * levanta una excepcion.
	 * 
	 * Excepciones:
	 * 	* PosicionInvalidaException
	 */
	public void colocar(Colocable colocable, Posiciones posiciones) {

		//Primero verifico que las posiciones sean correctas
		
		try {
			Iterador iterPosiciones = posiciones.getIterator();

			while(iterPosiciones.hasNext()) {
				Posicion posicionActual = iterPosiciones.obtenerActual();
				if ((!posicionEsValida(posicionActual)) || (estaOcupado(posicionActual))) {
					throw new PosicionInvalidaException();
				}
				iterPosiciones.next();
			}

		} catch (PosicionInvalidaException e) {
			throw new PosicionInvalidaException();
		}
		
		posicionesOcupadas.put(colocable, posiciones);
		colocable.darPosiciones(posiciones);
		colocable.conocerMapa(this);
		
		
		/*int tamanioDelColocable = colocable.obtenerTamanio();

		if ( hayEspacioSuficiente(tamanioDelColocable, posicion) ) {

			int posicionInicialX = posicion.x();
			int posicionInicialY = posicion.y();
			Posiciones posicionesQueOcupaElColocable = new Posiciones();

			for( int i = posicionInicialX; i < posicionInicialX + tamanioDelColocable; i++){
				for( int j = posicionInicialY; j < posicionInicialY + tamanioDelColocable; j++){
					posicionesQueOcupaElColocable.agregar(new Posicion(i, j));
				}
			}

			posicionesOcupadas.put(colocable, posicionesQueOcupaElColocable);

		} else {
			throw new PosicionInvalidaException();
		}*/

	}

	private boolean enRango(int coordenada) {
		return (coordenada >= 0) && (coordenada < this.tamanio);
	}

	private boolean posicionEsValida(Posicion posicion){
		return enRango(posicion.x()) && enRango(posicion.y());
	}



	/*
	 * PRE: Se le pasa una posicion correcta. Puedo preguntar por los
	 * bordes del mapa, pero siempre van a estar desocupados.
	 * POST: Devuelve si la celda esta ocupada o no.
	 * 
	 * Excepciones:
	 * 	* FueraDeRangoException
	 */
	public boolean estaOcupado(Posicion posicion) {

		if ( posicionEsValida(posicion) ) {

			for (Map.Entry<Colocable, Posiciones > e : posicionesOcupadas.entrySet()) {
				if (e.getValue().contiene(posicion)) {
					return true;
				}
			}

			return false;
		} else {
			throw new PosicionInvalidaException();
		}
	}

	/*
	 * PRE: Se le pasa una posicion correcta.
	 * POST: Libera la celda en esa posicion. Si la celda esta
	 * vacia, no hace nada.
	 * 
	 * Excepciones:
	 * 	* FueraDeRangoException
	 */
	public void remover(Colocable colocable) {
		posicionesOcupadas.remove(colocable);
	}

	/*
	 * PRE: Se le pasa una posicion correcta. Puede preguntar por el
	 * borde del mapa.
	 * POST: Devuelve lo que haya en esa posicion. Si no hay nada,
	 * devuelve NULL.
	 */
	public Colocable obtener(Posicion posicion) {

		for( Map.Entry<Colocable, Posiciones > e : posicionesOcupadas.entrySet() ){
			if(e.getValue().contiene(posicion)){
				return e.getKey();
			}
		}

		throw new PosicionVaciaException();

	}

	public int obtenerTamanio() {
		return this.tamanio;
	}

	public Set<Colocable> obtenerColocables() {
		return this.posicionesOcupadas.keySet();
	}

    public void cambiarPosicion(Colocable colocable, Posiciones posicionesNuevas) {
    	Iterador iter = posicionesNuevas.getIterator();
    	
    	while (iter.hasNext()) {
    		Posicion posicion = iter.obtenerActual();
    		
    		if(!(this.posicionEsValida(posicion)) || (this.estaOcupado(posicion))) {
    			throw new PosicionInvalidaException();
    		}
    		
    		iter.next();
    	}
		this.posicionesOcupadas.put(colocable, posicionesNuevas);
    }
}