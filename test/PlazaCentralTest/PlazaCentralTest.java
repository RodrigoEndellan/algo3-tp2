package PlazaCentralTest;

import creadores.CreadorDeAldeanos;
import creadores.CreadorDeUnidades;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.*;
import org.junit.Test;

import colocables.edificios.*;
import colocables.estados.EstaOcupadoException;
import colocables.unidades.Aldeano;
import colocables.unidades.Poblacion;

public class PlazaCentralTest {

        @Test
        public void Test01CrearAldeanoEnCeldaMasCercanaDePlazaCentral() {

                Mapa mapa = new Mapa(64);

                Equipo equipo = new Equipo();
                equipo.agregarPoblacion(new Poblacion(50));

                Posiciones _posicionPlazaCentral = new Posiciones();

                _posicionPlazaCentral.agregar(new Posicion(10, 10));
                _posicionPlazaCentral.agregar(new Posicion(11, 10));
                _posicionPlazaCentral.agregar(new Posicion(10, 11));
                _posicionPlazaCentral.agregar(new Posicion(11, 11));

                Posicion posicionAldeno = new Posicion(9, 9);

                PlazaCentral _PlazaCentral = new PlazaCentral();

                mapa.colocar(_PlazaCentral, _posicionPlazaCentral);

                CreadorDeUnidades _creadorDeAldeanos = new CreadorDeAldeanos();

                _PlazaCentral.construir();
                _PlazaCentral.construir();
                _PlazaCentral.construir();

                try {
                        _creadorDeAldeanos.asignarEquipo(equipo);
                        _PlazaCentral.crearUnidadCon(_creadorDeAldeanos);
                }
                catch (EstaOcupadoException e) {
                        assert (false);
                }
                assert (mapa.obtener(posicionAldeno) instanceof Aldeano);
        }

        @Test
        public void Test02CrearDosAldeanoEnLasDosPosicionesMasCercanaDePlazaCentral() {

                Mapa mapa = new Mapa(64);
                Equipo equipo = new Equipo();
                equipo.agregarPoblacion(new Poblacion(50));
                Posiciones _posicionPlazaCentral = new Posiciones();

                _posicionPlazaCentral.agregar(new Posicion(10, 10));
                _posicionPlazaCentral.agregar(new Posicion(11, 10));
                _posicionPlazaCentral.agregar(new Posicion(10, 11));
                _posicionPlazaCentral.agregar(new Posicion(11, 11));

                Posicion posicionAldeno = new Posicion(9, 9);

                PlazaCentral _PlazaCentral = new PlazaCentral();

                mapa.colocar(_PlazaCentral, _posicionPlazaCentral);

                CreadorDeUnidades _creadorDeAldeanos = new CreadorDeAldeanos();

                _PlazaCentral.construir();
                _PlazaCentral.construir();
                _PlazaCentral.construir();
                _creadorDeAldeanos.asignarEquipo(equipo);
                try {
                        _PlazaCentral.crearUnidadCon(_creadorDeAldeanos);
                        _PlazaCentral.terminarTurno();
                        _PlazaCentral.crearUnidadCon(_creadorDeAldeanos);
                        _PlazaCentral.terminarTurno();

                } catch (EstaOcupadoException e) {
                        assert (false);
                }

        }
}