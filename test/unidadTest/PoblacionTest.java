package unidadTest;

import mapa.Mapa;

import org.junit.Test;

import colocables.unidades.*;
import geometria.Posicion;
import geometria.Posiciones;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.mockito.Mockito.*;

public class PoblacionTest {

    @Test
    public void test01AgregarUnaUnidaAPoblacionVerificarQuEsaUnidadSeHayaAgregado(){
        Poblacion poblacion = new Poblacion(50);
        Aldeano unaAldeano = new Aldeano();
        poblacion.agregar(unaAldeano);
        assert(poblacion.incluye(unaAldeano));
    }

    @Test
    public void test02AgregarUnaUnidaAPoblacionVerificarQueLaPoblacionSeaDeCantidad1(){
        Poblacion poblacion = new Poblacion(50);
        Aldeano unaAldeano = new Aldeano();
        poblacion.agregar(unaAldeano);
        assertEquals(1,poblacion.obtenerCantidad());
    }

    @Test
    public void test03AgregarDosUnidadesAPoblacionVerificarQuEsasUnidadesSeHayaAgregado(){
        Poblacion poblacion = new Poblacion(50);
        Aldeano unaAldeano = new Aldeano();
        Aldeano otroAldeano = new Aldeano();
        poblacion.agregar(unaAldeano);
        poblacion.agregar(otroAldeano);
        assert(poblacion.incluye(unaAldeano));
        assert(poblacion.incluye(otroAldeano));
    }

    @Test
    public void test04AgregarDosUnidadesAPoblacionVerificarQueLaPoblacionSeaDeCantidad2(){
        Poblacion poblacion = new Poblacion(50);
        Aldeano unaAldeano = new Aldeano();
        Aldeano otroAldeano = new Aldeano();
        poblacion.agregar(unaAldeano);
        poblacion.agregar(otroAldeano);
        assertEquals(2,poblacion.obtenerCantidad());

    }

    @Test
    public void test05AgregarUnaUnidaAPoblacionEliminarEsMismaUnidaDePoblacionVerificarQueSeHayaEliminado(){
        Poblacion poblacion = new Poblacion(50);

        Mapa mapa = new Mapa(64);
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));

        Aldeano unAldeano = new Aldeano();
        mapa.colocar(unAldeano,posiciones);

        poblacion.agregar(unAldeano);
        assert(poblacion.incluye(unAldeano));
        poblacion.remover(unAldeano);
        assertFalse(poblacion.incluye(unAldeano));
    }

    @Test
    public void test06AgregarUnaUnidaAPoblacionEliminarEsMismaUnidaDePoblacionVerificarQueLaCantidaDePoblacionSeaDe0(){
        Mapa mapa = new Mapa(64);

        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));

        Poblacion poblacion = new Poblacion(50);

        Aldeano unAldeano = new Aldeano();

        mapa.colocar(unAldeano,posiciones);

        poblacion.agregar(unAldeano);
        assertEquals(1,poblacion.obtenerCantidad());
        poblacion.remover(unAldeano);
        assertEquals(0,poblacion.obtenerCantidad());

    }

    @Test
    public void test07Agregar50UnidadesAPoblacion(){
        Poblacion poblacion = new Poblacion(50);
        int i = 0;
        while(i<50){
            Aldeano unAldeano = new Aldeano();
            poblacion.agregar(unAldeano);
            i++;
        }
        assertEquals(50,poblacion.obtenerCantidad());

    }

    @Test (expected = PoblacionMaximaAlcanzadaException.class)
    public void test08Agregar51UnidadesAPoblacionYQueLevanteUnErrorDeMaximaCantidadDePoblacion(){
        Poblacion poblacion = new Poblacion(50);
        int i = 0;
        while(i<50){
            Aldeano unAldeano = new Aldeano();
            poblacion.agregar(unAldeano);
            i++;
        }
        assertEquals(50,poblacion.obtenerCantidad());
        Aldeano unAldeano = new Aldeano();
        poblacion.agregar(unAldeano);

    }

    @Test
    public void test09TerminarTurnoDeLasUnidadesDePoblacionSeLLamoUnaVezParaAldeano(){
        Poblacion poblacion = new Poblacion(50);
        Aldeano aldeanoMock = mock(Aldeano.class);
        poblacion.agregar(aldeanoMock);
        doNothing().when(aldeanoMock).terminarTurno();
        poblacion.terminarTurno();
        verify(aldeanoMock, times(1)).terminarTurno();
    }


}
