package unidadTest;

import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Colocable;
import colocables.unidades.MovimientoInvalidoException;
import geometria.Posicion;
import geometria.Posiciones;

public class MovimientoArmaDeAsedioTest {

    @Test(expected = MovimientoInvalidoException.class)
    public void test01ArmaDeAsedioSeDesmontaNoAvanzaUnTurnoYTrataDeMoverseLevantaMovimientoInvalidoException(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();

        Posiciones posicionesArmaAsedio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.mover(new Posicion(10,11));

    }

    @Test
    public void test02ArmaDeAsedioSeCreaYSeMueve(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();

        Posiciones posicionesArmaAsedio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.mover(new Posicion(10,11));
        assert(mapa.obtener(new Posicion(10,11))instanceof ArmaDeAsedio);

    }

    @Test
    public void test03ArmaDeAsedioSeMontaSeDesmontaAvanzaUnTurnoYSeMueve(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();

        Posiciones posicionesArmaAsedio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.terminarTurno();
        unArmaAsedio.mover(new Posicion(10,11));
        assert(mapa.obtener(new Posicion(10,11))instanceof ArmaDeAsedio);

    }

    @Test(expected = MovimientoInvalidoException.class)
    public void test04ArmaDeAsedioSeMontaTrataDeMoverseLevantaMovimientoInvalidoException(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();

        Posiciones posicionesArmaAsedio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.mover(new Posicion(10,11));

    }
}
