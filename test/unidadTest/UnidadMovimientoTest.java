package unidadTest;

/*
 * Esta prueba tiene como finalidad probar el movimiento de las unidades en el mapa.
 * TODO Prueba de acoplamiento entre mapa y unidad? Esto esta bien?
 */
import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.*;

import colocables.unidades.*;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.*;

public class UnidadMovimientoTest {
	
	@Test
	public void test01UnidadColcoadaEnPosicionX5Y5DeberiaMoverArribaYTerminarEnPosicionX5Y6() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(5,6));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(5,6)));
		
	}
	
	@Test
	public void test02UnidadColcoadaEnPosicionX5Y5DeberiaMoverAbajoYTerminarEnPosicionX5Y4() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(5,4));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(5,4)));
		
	}
	
	@Test
	public void test03UnidadColcoadaEnPosicionX5Y5DeberiaMoverDerechaYTerminarEnPosicionX6Y5() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(6,5));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(6,5)));
		
	}
	
	@Test
	public void test04UnidadColcoadaEnPosicionX5Y5DeberiaMoverIzquierdaYTerminarEnPosicionX4Y5() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(4,5));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(4,5)));
		
	}
	
	@Test
	public void test05UnidadColcoadaEnPosicionX5Y5DeberiaMoverArribaIzquierdaYTerminarEnPosicionX4Y6() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(4,6));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(4,6)));
		
	}
	
	@Test
	public void test06UnidadColcoadaEnPosicionX5Y5DeberiaMoverArribaDerechaYTerminarEnPosicionX6Y6() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(6,6));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(6,6)));
		
	}
	
	@Test
	public void test07UnidadColcoadaEnPosicionX5Y5DeberiaMoverAbajoDerechaYTerminarEnPosicionX6Y4() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(6,4));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(6,4)));
		
	}
	
	@Test
	public void test08UnidadColocadaEnPosicionX5Y5DeberiaMoverAbajoIzquierdaYTerminarEnPosicionX4Y4() {
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		try {
			unidad.mover(new Posicion(4,4));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}
		
		assertFalse(mapaATestear.estaOcupado(new Posicion(5,5)));
		assert(mapaATestear.estaOcupado(new Posicion(4,4)));
		
	}

	@Test
	public void test09UnidadSoloSePuedeMoverUnaVezPorTurnoLanzaErrorMovimientoInvalidoException(){
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad, posicionesQueOcupa);

		boolean ocurrioErrorMovimientoInvalidoException = false;

		try {
			unidad.mover(new Posicion(4,4));
		} catch (MovimientoInvalidoException e) {
			ocurrioErrorMovimientoInvalidoException = true;
		}

		assert(!ocurrioErrorMovimientoInvalidoException);

		try {
			unidad.mover(new Posicion(4,4));
		} catch (MovimientoInvalidoException e) {
			ocurrioErrorMovimientoInvalidoException = true;
		}

		assert(ocurrioErrorMovimientoInvalidoException);

	}
	
	@Test
	public void test10AlMoverLaPosicionDeLaUnidadEnElMapaYEnLaUnidadDeberiaSerLaMisma(){
		Mapa mapaATestear = new Mapa(64);
		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(1,1));
		mapaATestear.colocar(unidad, posicionesQueOcupa);

		try {
			unidad.mover(new Posicion(2,1));
		} catch (MovimientoInvalidoException e) {
			e.printStackTrace();
		}

		Posiciones verificacionDePosiciones = unidad.obtenerPosiciones();
		
		assert(verificacionDePosiciones.contiene(new Posicion(2,1)));
		assert(mapaATestear.estaOcupado(new Posicion(2,1)));
		assertFalse(mapaATestear.estaOcupado(new Posicion(1,1)));
		assertFalse(verificacionDePosiciones.contiene(new Posicion(1,1)));

	}
}
