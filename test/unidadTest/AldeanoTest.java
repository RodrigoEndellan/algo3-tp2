package unidadTest;

import constructores.ConstructorDeCuartel;
import constructores.ConstructorDePlazaCentral;
import constructores.NoHayPosicionesValidasException;
import geometria.Posicion;
import geometria.Posiciones;

import  org.junit.Test;

import colocables.edificios.*;
import colocables.estados.Construyendo;
import colocables.estados.EstaOcupadoException;
import colocables.estados.Libre;
import colocables.unidades.*;
import mapa.*;

import static org.mockito.Mockito.*;

public class AldeanoTest {

    @Test
    public void test01AldeanoNoPuedeHacerNadaMientrasConstruye() {

        Aldeano aldeano = new Aldeano();
        Posiciones posAldeano = new Posiciones();
        posAldeano.agregar(new Posicion(0,1));
        aldeano.darPosiciones(posAldeano);
        Posicion posicionPlazaRef = new Posicion(1, 1);
        Posiciones posicionesPlaza = new Posiciones();
        posicionesPlaza.agregar(posicionPlazaRef);
        posicionesPlaza.agregar(new Posicion(1, 2));
        posicionesPlaza.agregar(new Posicion(2, 2));
        posicionesPlaza.agregar(new Posicion(2, 1));

        //clases mockeada
        Mapa mapaMocked = mock(Mapa.class);
        ConstructorDePlazaCentral constructorMocked = mock(ConstructorDePlazaCentral.class);
        PlazaCentral plazaMocked = mock(PlazaCentral.class);



        aldeano.conocerMapa(mapaMocked);

        //comportamiento mockeado
        when(constructorMocked.construir(posicionPlazaRef)).thenReturn(plazaMocked);
        when(plazaMocked.obtenerPosiciones()).thenReturn(posicionesPlaza);
        doNothing().when(mapaMocked).colocar(plazaMocked, posicionesPlaza);
        when(plazaMocked.construir()).thenReturn(new Construyendo(plazaMocked));

        try {
            aldeano.construir(constructorMocked, posicionPlazaRef);
        } catch (EstaOcupadoException | NoSeConstruyeEnPosicionAlejadaException
                | NoSeConstruyeEnMismaPosicionException e) {
            assert (false);
        }

        aldeano.terminarTurno();

        try {
            aldeano.mover(new Posicion(0,2));
        } catch (MovimientoInvalidoException e) {
            assert (true);
        }

    }

    @Test
    public void test02AldeanoNoPuedeConstruirEnSuMismaPosicion(){

        Aldeano aldeano = new Aldeano();
        Posiciones posicionesAldeano = new Posiciones();
        Posicion posAldeano = new Posicion(0,1);
        posicionesAldeano.agregar(new Posicion(0,1));
        aldeano.darPosiciones(posicionesAldeano);

        //clases mockeada
        ConstructorDePlazaCentral constructorMocked = mock(ConstructorDePlazaCentral.class);


        boolean huboExcepcion = false;

        try {aldeano.construir(constructorMocked, posAldeano);
        } catch (NoSeConstruyeEnMismaPosicionException e ){
            huboExcepcion = true;
        } catch (PosicionInvalidaException | EstaOcupadoException |
                 NoSeConstruyeEnPosicionAlejadaException e) {
            assert (false);
        }
        assert (huboExcepcion);

    }

    @Test
    public void test03AldeanoNoPuedeConstruirEnUnaPosicionAlejada(){
        Aldeano aldeano = new Aldeano();
        Posiciones posicionesAldeano = new Posiciones();
        posicionesAldeano.agregar(new Posicion(0,1));
        aldeano.darPosiciones(posicionesAldeano);
        Posicion posicionAlejada = new Posicion(1, 5);

        //clases mockeada
        ConstructorDePlazaCentral constructorMocked = mock(ConstructorDePlazaCentral.class);

        boolean huboExcepcion = false;

        try {aldeano.construir(constructorMocked, posicionAlejada);
        } catch (NoSeConstruyeEnPosicionAlejadaException e ){
            huboExcepcion = true;
        } catch (PosicionInvalidaException | EstaOcupadoException |
                 NoSeConstruyeEnMismaPosicionException e) {
            assert (false);
        }
        assert (huboExcepcion);
    }

    @Test
    public void test04AldeanoConstruyeYDejaDePorducirOro() {


        Aldeano aldeano = new Aldeano();
        Posiciones posicionesAldeano = new Posiciones();
        posicionesAldeano.agregar(new Posicion(0,1));
        aldeano.darPosiciones(posicionesAldeano);

        Posicion posicionNueva = new Posicion(1, 1);
        int oroCartera = 0;

        //clases mockeadas
        Cartera carteraMocked = mock(Cartera.class);
        Mapa mapaMocked = mock(Mapa.class);

        aldeano.conocerCartera(carteraMocked);
        aldeano.conocerMapa(mapaMocked);

        //comportamiento mockeado
        when(carteraMocked.obtenerOro()).thenReturn(oroCartera);
        doNothing().when(carteraMocked).sumarOro(20);
        when(mapaMocked.estaOcupado(posicionNueva)).thenReturn(false);

        aldeano.terminarTurno();

        try {
            aldeano.mover(posicionNueva);
        } catch (MovimientoInvalidoException e) {
            assert(false);
        }

        aldeano.terminarTurno();

        aldeano.terminarTurno();

        verify(carteraMocked, times(3)).sumarOro(20);

    }

    @Test
    public void test05AldeanoConstruyeCuartelEn3Turnos() {

        Aldeano aldeano = new Aldeano();
        Posiciones posicionesAldeano = new Posiciones();
        posicionesAldeano.agregar(new Posicion(0,1));
        aldeano.darPosiciones(posicionesAldeano);

        Posicion posicionCuartelRef = new Posicion(1, 1);
        Posiciones posiciones1Cuartel = new Posiciones();
        posiciones1Cuartel.agregar(posicionCuartelRef);
        posiciones1Cuartel.agregar(new Posicion(1, 2));
        posiciones1Cuartel.agregar(new Posicion(2, 2));
        posiciones1Cuartel.agregar(new Posicion(2, 1));

        //clases mockeadas
        ConstructorDeCuartel constructorMocked = mock(ConstructorDeCuartel.class);
        Cuartel cuartelMocked = mock(Cuartel.class);
        Mapa mapaMocked = mock(Mapa.class);
        Cartera carteraMocked = mock(Cartera.class);

        //comportamiento mockeado
        when(constructorMocked.construir(posicionCuartelRef)).thenReturn(cuartelMocked);
        when(cuartelMocked.obtenerPosiciones()).thenReturn(posiciones1Cuartel);
        when(cuartelMocked.construir()).thenReturn(new Construyendo(cuartelMocked));
        doNothing().when(carteraMocked).sumarOro(20);

        aldeano.conocerCartera(carteraMocked);
        aldeano.conocerMapa(mapaMocked);

        try {
            aldeano.construir(constructorMocked, posicionCuartelRef);
        } catch (PosicionInvalidaException | 
                EstaOcupadoException | NoSeConstruyeEnPosicionAlejadaException |
                NoSeConstruyeEnMismaPosicionException e){
            assert (false);
        }

        aldeano.terminarTurno();
        aldeano.terminarTurno();

        verify(cuartelMocked, times(2)).construir();
        when(cuartelMocked.construir()).thenReturn(new Libre());

        aldeano.terminarTurno();

        verify(cuartelMocked, times(3)).construir();

        aldeano.terminarTurno();

        verify(carteraMocked, times(1)).sumarOro(20);
    }

    @Test
    public void test06AldeanoConstruyePlazaCentralEn3Turnos() {

        Aldeano aldeano = new Aldeano();
        Posiciones posAldeano = new Posiciones();
        posAldeano.agregar(new Posicion(0,1));
        aldeano.darPosiciones(posAldeano);
        Posicion posicionPlazaRef = new Posicion(1, 1);
        Posiciones posicionesPlaza = new Posiciones();
        posicionesPlaza.agregar(posicionPlazaRef);
        posicionesPlaza.agregar(new Posicion(1, 2));
        posicionesPlaza.agregar(new Posicion(2, 2));
        posicionesPlaza.agregar(new Posicion(2, 1));

        //clases mockeadas
        ConstructorDePlazaCentral constructorMocked = mock(ConstructorDePlazaCentral.class);
        PlazaCentral plazaMocked = mock(PlazaCentral.class);
        Mapa mapaMocked = mock(Mapa.class);
        Cartera carteraMocked = mock(Cartera.class);

        //comportamiento mockeado
        when(constructorMocked.construir(posicionPlazaRef)).thenReturn(plazaMocked);
        when(plazaMocked.obtenerPosiciones()).thenReturn(posicionesPlaza);
        when(plazaMocked.construir()).thenReturn(new Construyendo(plazaMocked));
        doNothing().when(carteraMocked).sumarOro(20);

        aldeano.conocerCartera(carteraMocked);
        aldeano.conocerMapa(mapaMocked);

        try {
            aldeano.construir(constructorMocked, posicionPlazaRef);
        } catch (PosicionInvalidaException |
                EstaOcupadoException | NoSeConstruyeEnPosicionAlejadaException |
                NoSeConstruyeEnMismaPosicionException e){
            assert (false);
        }

        aldeano.terminarTurno();
        aldeano.terminarTurno();

        when(plazaMocked.construir()).thenReturn(new Libre());

        aldeano.terminarTurno();

        verify(plazaMocked, times(3)).construir();

        aldeano.terminarTurno();

        verify(carteraMocked, times(1)).sumarOro(20);
    }
}

