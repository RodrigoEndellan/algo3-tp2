package unidadTest;

import colocables.estados.estadosDeReparacion.EstaSiendoReparadaPorUnAldeanoException;
import colocables.estados.estadosDeReparacion.EstaFueraDeRango;
import colocables.estados.estadosDeReparacion.NoNecesitaReparacionExeption;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.*;
import colocables.unidades.*;

import static org.junit.Assert.*;

public class ReparacionTest {


    @Test
    public void test01AldeanoIntentaRepararCastilloRecienCreadoDeberiaAlTerminarTurnoAumentarOro() throws EstaFueraDeRango, EstaSiendoReparadaPorUnAldeanoException, NoNecesitaReparacionExeption {

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        Mapa mapa = new Mapa(64);

        Posiciones posicionesCastillo = new Posiciones();
        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesCastillo.agregar(new Posicion(11,10));
        posicionesCastillo.agregar(new Posicion(10,11));
        posicionesCastillo.agregar(new Posicion(11,11));

        Castillo castillo = new Castillo();
        castillo.conocerMapa(mapa);
        mapa.colocar(castillo, posicionesCastillo);

        Aldeano aldeano = new Aldeano();

        castillo.asignarEquipo(equipo);
        aldeano.asignarEquipo(equipo);

        Posiciones posicionesAldeano = new Posiciones();
        posicionesAldeano.agregar(new Posicion(12,11));
        mapa.colocar(aldeano, posicionesAldeano);

        Cartera cartera = new Cartera();

        aldeano.conocerCartera(cartera);

        try{
            aldeano.reparar(castillo);
        }
        catch (NoNecesitaReparacionExeption e){
            assert(true);
        }

    }

    @Test
    public void Test02AldeanoTrataDeRepararUnEdificioQueNoTieneAlLadoDevuelveError() throws EstaSiendoReparadaPorUnAldeanoException, NoNecesitaReparacionExeption, EstaSiendoReparadaPorUnAldeanoException, NoNecesitaReparacionExeption {
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unEspdachin.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unEspdachin, posicionesEspadachin);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unEspdachin.atacar(unColocable);
        assertEquals(235,unEdificio.obtenerVidaActual());

        Aldeano aldeano = new Aldeano();

        aldeano.asignarEquipo(equipo);
        Posiciones posicionesAldeano = new Posiciones();

        Cartera cartera = new Cartera();


        posicionesAldeano.agregar(new Posicion(14,11));

        aldeano.conocerCartera(cartera);
        mapa.colocar(aldeano, posicionesAldeano);

        try {
            aldeano.reparar(unEdificio);
        }
        catch (EstaFueraDeRango a ){
            assert(true);
        }
    }

    @Test
    public void Test03AldeanoTrataDeRepararUnCuartelDaniadoPorUnEspadachin() {
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unEspdachin.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unEspdachin, posicionesEspadachin);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unEspdachin.atacar(unColocable);

        assertEquals(235,unEdificio.obtenerVidaActual());

        Aldeano aldeano = new Aldeano();

        aldeano.asignarEquipo(equipo);
        Posiciones posicionesAldeano = new Posiciones();

        Cartera cartera = new Cartera();

        posicionesAldeano.agregar(new Posicion(13,11));

        aldeano.conocerCartera(cartera);
        mapa.colocar(aldeano, posicionesAldeano);

        int vida = unEdificio.obtenerVidaActual();

        try {
            aldeano.reparar(unEdificio);
        } catch (EstaFueraDeRango | EstaSiendoReparadaPorUnAldeanoException | NoNecesitaReparacionExeption estaFueraDeRango) {
            fail();
        }

        aldeano.terminarTurno();

        assertNotEquals(vida, unEdificio.obtenerVidaActual());

    }
}
