package unidadTest;

import static org.junit.Assert.*;

import org.junit.Test;

import colocables.unidades.*;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.*;

public class UnidadMovimientoCasosBordeTest {


	@Test(expected = MovimientoInvalidoException.class)
	public void test01UnidadEnX5Y5NoDeberiaPoderMoverseHaciaX5Y6SiEstaOcupadoDeberiaLevantarExcepcionMovimientoInvalidoException() {
		Mapa mapaATestear = new Mapa(64);
		//Init unidad1
		Aldeano unidad1 = new Aldeano();
		Posiciones posicionesQueOcupa1 = new Posiciones();
		posicionesQueOcupa1.agregar(new Posicion(5,5));
		mapaATestear.colocar(unidad1, posicionesQueOcupa1);
		//Init unidad2
		Aldeano unidad2 = new Aldeano();
		Posiciones posicionesQueOcupa2 = new Posiciones();
		posicionesQueOcupa2.agregar(new Posicion(5,6));
		mapaATestear.colocar(unidad2, posicionesQueOcupa2);
		
		unidad1.mover(new Posicion(5,6));
		
	}
	

	@Test(expected = MovimientoInvalidoException.class)
	public void test02UnidadEnX63Y63NoDeberiaPoderMoverseHaciaX64Y63() {
		Mapa mapaATestear = new Mapa(64);

		Aldeano unidad = new Aldeano();
		Posiciones posicionesQueOcupa = new Posiciones();
		posicionesQueOcupa.agregar(new Posicion(63,63));
		mapaATestear.colocar(unidad, posicionesQueOcupa);
		
		unidad.mover(new Posicion(64,64));
	}
	
	/*	
	@Test
	public void test03UnidadEnX1Y1NoDeberiaPoderMoverseHaciaAbajoDerecha() {
		Mapa mapaATestear = new Mapa(64);
		Unidad aldeano = new Aldeano();
		try {
			mapaATestear.colocar(aldeano, new Posicion(1,1));
		} catch (PosicionInvalidaException | FueraDeRangoException e) {
			e.printStackTrace();
		}
		boolean ocurrioUnaExcepcion = false;
		
		try {
			aldeano.moverAbajoDerecha();
		} catch (MovimientoInvalidoException e) {
			ocurrioUnaExcepcion = true;
		}
		
		assertEquals(aldeano.obtenerPosicion() , new Posicion(1,1));

		assertFalse(mapaATestear.estaOcupado(new Posicion(2,0)));

		assert(ocurrioUnaExcepcion);
		
	}

	
	@Test
	public void test040UnidadEnX1Y1NoDeberiaPoderMoverseHaciaIzquierda() {
		Mapa mapaATestear = new Mapa(64);
		Unidad aldeano = new Aldeano();
		try {
			mapaATestear.colocar(aldeano, new Posicion(1,1));
		} catch (PosicionInvalidaException | FueraDeRangoException e) {
			e.printStackTrace();
		}
		boolean ocurrioUnaExcepcion = false;
		
		try {
			aldeano.moverIzquierda();
		} catch (MovimientoInvalidoException e) {
			ocurrioUnaExcepcion = true;
		}
		
		assertEquals(aldeano.obtenerPosicion() , new Posicion(1,1));

		assertFalse(mapaATestear.estaOcupado(new Posicion(0,1)));

		assert(ocurrioUnaExcepcion);
		
	}
	
	@Test
	public void test050UnidadEnX1Y1NoDeberiaPoderMoverseHaciaIzquierdaArriba() {
		Mapa mapaATestear = new Mapa(64);
		Unidad aldeano = new Aldeano();
		try {
			mapaATestear.colocar(aldeano, new Posicion(1,1));
		} catch (PosicionInvalidaException | FueraDeRangoException e) {
			e.printStackTrace();
		}
		boolean ocurrioUnaExcepcion = false;
		
		try {
			aldeano.moverArribaIzquierda();
		} catch (MovimientoInvalidoException e) {
			ocurrioUnaExcepcion = true;
		}
		
		assertEquals(aldeano.obtenerPosicion() , new Posicion(1,1));
		assertFalse(mapaATestear.estaOcupado(new Posicion(0,2)));
		assert(ocurrioUnaExcepcion);
		
	}
	
	@Test
	public void test06UnidadEnX62Y62NoDeberiaPoderMoverseHaciaArriba() {
		Mapa mapaATestear = new Mapa(64);
		Unidad aldeano = new Aldeano();
		try {
			mapaATestear.colocar(aldeano, new Posicion(62,62));
		} catch (PosicionInvalidaException | FueraDeRangoException e) {
			e.printStackTrace();
		}
		boolean ocurrioUnaExcepcion = false;
		
		try {
			aldeano.moverArriba();
		} catch (MovimientoInvalidoException e) {
			ocurrioUnaExcepcion = true;
		}
		
		assertEquals(aldeano.obtenerPosicion() , new Posicion(62,62));

		assertFalse(mapaATestear.estaOcupado(new Posicion(62,63)));

		assert(ocurrioUnaExcepcion);
		
	}
	
	@Test
	public void test070UnidadEnX62Y62NoDeberiaPoderMoverseHaciaArribaDerecha() {
		Mapa mapaATestear = new Mapa(64);
		Unidad aldeano = new Aldeano();
		try {
			mapaATestear.colocar(aldeano, new Posicion(62,62));
		} catch (PosicionInvalidaException | FueraDeRangoException e) {
			e.printStackTrace();
		}
		boolean ocurrioUnaExcepcion = false;
		
		try {
			aldeano.moverArribaDerecha();
		} catch (MovimientoInvalidoException e) {
			ocurrioUnaExcepcion = true;
		}
		
		assertEquals(aldeano.obtenerPosicion() , new Posicion(62,62));

		assertFalse(mapaATestear.estaOcupado(new Posicion(63,63)));

		assert(ocurrioUnaExcepcion);
		
	}
	
	@Test
	public void test070UnidadEnX62Y62NoDeberiaPoderMoverseHaciaDerecha() {
		Mapa mapaATestear = new Mapa(64);
		Unidad aldeano = new Aldeano();
		try {
			mapaATestear.colocar(aldeano, new Posicion(62,62));
		} catch (PosicionInvalidaException | FueraDeRangoException e) {
			e.printStackTrace();
		}
		boolean ocurrioUnaExcepcion = false;
		
		try {
			aldeano.moverDerecha();
		} catch (MovimientoInvalidoException e) {
			ocurrioUnaExcepcion = true;
		}
		
		
		assertEquals(aldeano.obtenerPosicion() , new Posicion(62,62));

		assertFalse(mapaATestear.estaOcupado(new Posicion(63,62)));

		assert(ocurrioUnaExcepcion);
		
	}
*/
}
