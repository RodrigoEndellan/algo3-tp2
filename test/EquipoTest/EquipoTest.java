package EquipoTest;

import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Castillo;
import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.estados.Estado;
import colocables.estados.Muerto;
import colocables.unidades.Aldeano;
import colocables.unidades.Espadachin;
import colocables.unidades.Poblacion;

import static junit.framework.TestCase.assertFalse;
import static org.mockito.Mockito.*;

public class EquipoTest {

    @Test
    public void test01AgregarUnaUnidadAlEquipo(){
        Poblacion poblacion = new Poblacion(50);
        Equipo unEquipo = new Equipo();
        unEquipo.agregarPoblacion(poblacion);
        Aldeano unaUnidad = new Aldeano();
        assertFalse (unEquipo.incluye(unaUnidad));
        unEquipo.agregar(unaUnidad);
        assert (unEquipo.incluye(unaUnidad));
    }

    @Test
    public void test02AgregarUnEdificodAlEquipoVerificarQueSeHayaAgregado(){

        Equipo unEquipo = new Equipo();
        Ciudad ciudad = new Ciudad();
        unEquipo.agregarCiudad(ciudad);
        Cuartel unEdificio = new Cuartel();
        assertFalse (unEquipo.incluye(unEdificio));
        unEquipo.agregar(unEdificio);
        assert (unEquipo.incluye(unEdificio));
    }

    @Test
    public void test03AgregarUnEdificodAlEquipoRemoverloVerificarQueSeHayaRemovido(){

        Equipo unEquipo = new Equipo();
        Cuartel unEdificio = new Cuartel();
        Ciudad ciudad = new Ciudad();

        Mapa mapa = new Mapa(64);
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));
        mapa.colocar(unEdificio,posiciones);

        unEquipo.agregarCiudad(ciudad);
        unEquipo.agregar(unEdificio);
        assert (unEquipo.incluye(unEdificio));
        unEquipo.remover(unEdificio);
        assertFalse(unEquipo.incluye(unEdificio));
    }

    @Test
    public void test04AgregarUnaUnidadAlEquipoRemoverlaVerificarQueSeHayaRemovido(){
        Mapa mapa = new Mapa(64);

        Poblacion poblacion = new Poblacion(50);
        Equipo unEquipo = new Equipo();
        unEquipo.agregarPoblacion(poblacion);

        Aldeano unaUnidad = new Aldeano();
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));
        unEquipo.agregar(unaUnidad);

        mapa.colocar(unaUnidad, posiciones);

        assert (unEquipo.incluye(unaUnidad));
        unEquipo.remover(unaUnidad);
        assertFalse(unEquipo.incluye(unaUnidad));
    }

    @Test
    public void test05TerminarTurnoDeLasUnidadesDePoblacionSeLLamoUnaVezParaAldeano(){
        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        Ciudad ciudad = new Ciudad();
        equipo.agregarCiudad(ciudad);
        Castillo castilloMock = mock(Castillo.class);
        equipo.agregar(castilloMock);
        doNothing().when(castilloMock).terminarTurno();
        equipo.terminarTurno();
        verify(castilloMock, times(1)).terminarTurno();
    }

    @Test
    public void test06TerminarTurnoDeLasUnidadesDePoblacionSeLLamoUnaVezParaAldeano(){
        Equipo equipo = new Equipo();
        Poblacion poblacion = new Poblacion(50);
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(poblacion);
        Aldeano aldeanoMock = mock(Aldeano.class);
        equipo.agregar(aldeanoMock);
        doNothing().when(aldeanoMock).terminarTurno();
        equipo.terminarTurno();
        verify(aldeanoMock, times(1)).terminarTurno();
    }

    @Test
    public void test07SeAgregaUnAldeanoAlEquipoSeReduceSuVidaA0YSeLoRemueveDelMapaYEquipo(){
        Mapa mapa = new Mapa(64);

        Equipo equipo = new Equipo();

        Poblacion poblacion = new Poblacion(50);

        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(poblacion);

        Aldeano aldeanoMock = mock(Aldeano.class);
        Estado muerto = new Muerto();
        doReturn(muerto).when(aldeanoMock).obtenerEstado();
        aldeanoMock.conocerMapa(mapa);

        Posiciones posicionesAldeano = new Posiciones();
        posicionesAldeano.agregar(new Posicion(10,10));

        mapa.colocar(aldeanoMock,posicionesAldeano);

        equipo.agregar(aldeanoMock);
        aldeanoMock.asignarEquipo(equipo);

        equipo.remover(aldeanoMock);
        assertFalse(equipo.incluye(aldeanoMock));
        //assert (equipo.incluye(aldeanoMock));
        //equipo.actualizarUnidadesActivas();
        //assertFalse(equipo.incluye(aldeanoMock));
    }


}
