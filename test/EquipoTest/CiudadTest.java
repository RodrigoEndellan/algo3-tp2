package EquipoTest;

import mapa.Mapa;

import org.junit.Test;
import org.mockito.Mockito;

import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import geometria.Posicion;
import geometria.Posiciones;

import static junit.framework.TestCase.assertFalse;
import static org.mockito.Mockito.*;

public class CiudadTest {

    @Test
    public void test01AgregarUnEdificioALaCiudad(){
        Ciudad ciudad = new Ciudad();
        Edificio cuartel = new Cuartel();
        ciudad.agregar(cuartel);
        assert (ciudad.incluye(cuartel));
    }

    @Test
    public void test02AgregarEdificioALaCiudadYEliminarlo(){
        Ciudad ciudad = new Ciudad();
        Edificio cuartel = new Cuartel();
        ciudad.agregar(cuartel);

        Mapa mapa = new Mapa(64);
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));
        mapa.colocar(cuartel,posiciones);

        assert (ciudad.incluye(cuartel));
        ciudad.remover(cuartel);
        assertFalse (ciudad.incluye(cuartel));
    }

    @Test
    public void test03TerminarTurnoDeUnEdificioEnLaCiudad(){
        Ciudad ciudad = new Ciudad();
        Edificio cuartelMock = mock(Cuartel.class);
        doNothing().when(cuartelMock).terminarTurno();
        ciudad.agregar(cuartelMock);
        ciudad.terminarTurno();
        verify(cuartelMock, times(1)).terminarTurno();

    }
}
