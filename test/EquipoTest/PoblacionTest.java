package EquipoTest;

import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Ciudad;
import colocables.unidades.Aldeano;
import colocables.unidades.Espadachin;
import colocables.unidades.Poblacion;
import geometria.Posicion;
import geometria.Posiciones;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class PoblacionTest {

    @Test
    public void test01AgregarUnaUnidadAPoblacionVerificarQueLaCantidadSeaUno(){

        Poblacion poblacion = new Poblacion(50);
        poblacion.agregar(new Aldeano());
        assertEquals(1,poblacion.obtenerCantidad());
    }

    @Test
    public void test02PoblacionEmpiezaConCantidadDePoblacion0(){

        Poblacion poblacion = new Poblacion(50);
        assertEquals(0,poblacion.obtenerCantidad());
    }

    @Test
    public void test03PoblacionAgregoUnaUnidadYLaQuitoLaPoblacionVuelveASer0(){

        Mapa mapa = new Mapa(64);
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));

        Poblacion poblacion = new Poblacion(50);
        Aldeano aldeano = new Aldeano();

        mapa.colocar(aldeano,posiciones);

        poblacion.agregar(aldeano);
        poblacion.remover(aldeano);
        assertEquals(0,poblacion.obtenerCantidad());
    }

    @Test//(expected = MovimientoInvalidoException.class)
    public void test04PoblacionAgregoUnaUnidadYLaQuitoLaPoblacionVuelveASer0(){

        Mapa mapa = new Mapa(64);
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));

        Poblacion poblacion = new Poblacion(50);
        Aldeano aldeano = new Aldeano();
        mapa.colocar(aldeano,posiciones);
        poblacion.remover(aldeano);
        assertEquals(0, poblacion.obtenerCantidad());

    }

    @Test
    public void test05PoblacionContieneUnAldeanoSuVidaBajaA0YEsQuitado(){
        Mapa mapa = new Mapa(64);
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));

        Poblacion poblacion = new Poblacion(50);
        Aldeano aldeano = new Aldeano();
        Espadachin espadachin = new Espadachin();
        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        equipo.agregarCiudad(new Ciudad());

        mapa.colocar(aldeano,posiciones);

        aldeano.asignarEquipo(equipo);
        aldeano.asignarEquipo(equipo);
        while (aldeano.obtenerVidaActual()>0) {
            aldeano.recibirDanioDeUn(espadachin);
        }
        poblacion.remover(aldeano);
        assertFalse(poblacion.incluye(aldeano));

    }


}
