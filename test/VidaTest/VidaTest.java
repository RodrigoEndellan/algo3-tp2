package VidaTest;

import org.junit.Test;

import colocables.estados.Estado;
import colocables.estados.Libre;
import colocables.estados.Muerto;
import colocables.unidades.Aldeano;
import colocables.unidades.Espadachin;
import colocables.unidades.Vida;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class VidaTest {

    @Test
    public void test01VidaEs50YSeResta15QuedaEn35(){
        Vida vida = new Vida(50);
        Estado estado = new Libre();
        vida.disminuirEnPuntos(15);
        assertEquals(vida.obtenerVidaActual(),35);
        assertNotEquals(vida.obtenerVidaActual(),50);
    }

    @Test
    public void test02VidaEs35YSeAumenta15QuedaEn50(){
        Vida vida = new Vida(35);
        assertEquals(vida.obtenerVidaActual(),35);
        vida.aumentarPuntosDeVida(15);
        assertEquals(vida.obtenerVidaActual(),50);
    }

    @Test
    public void test03VidaEs35YSeAumenta15QuedaEn50(){
        Vida vida = new Vida(35);
        assertEquals(vida.obtenerVidaActual(),35);
        vida.aumentarPuntosDeVida(15);
        assertEquals(vida.obtenerVidaActual(),50);
    }

    @Test
    public void test04VidaSeIniciaUnaVidaEstaEnEstadoLibre(){
        Vida vida = new Vida(35);
        Estado estado = new Libre();
        assert (vida.actualizarEstado(estado) instanceof Libre);


    }

    @Test
    public void test05VidaSeReduceA0oMenosCambiaEstadoAMuerto(){
        Vida vida = new Vida(100);
        vida.disminuirEnPuntos(100);
        Estado estado = new Libre();
        assertEquals(0,vida.obtenerVidaActual());
        assert (vida.actualizarEstado(estado) instanceof Muerto);

    }

}
