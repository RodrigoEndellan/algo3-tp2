package mapaTest;


import org.junit.Test;

import colocables.unidades.Colocable;
import geometria.Posicion;
import geometria.Posiciones;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;
import mapa.*;

/*
 * Estas pruebas tienen como finalidad probar poner y sacar unidades en el mapa.
 */

// TODO: Renombrar a MapaColocacionBasicaTest
public class MapaColocacionTest {

	@Test
	public void test01DeberiaPoderColocarUnColocableEnLaPosicionX0Y0RecienCreadoElMapa() {
		Mapa mapaATestear = new Mapa(64);
		Posiciones posicionesAOcupadar = new Posiciones();
		posicionesAOcupadar.agregar(new Posicion(0,0));
	    Colocable mockedColocable1 = mock(Colocable.class);

		mapaATestear.colocar(mockedColocable1, posicionesAOcupadar);

		assert(mapaATestear.estaOcupado(new Posicion(0,0)));
	}

	@Test(expected = PosicionInvalidaException.class)
	public void test02NoDeberiaPoderSuperponerColocablesEnPosicionX0Y0() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Colocable mockedColocable2 = mock(Colocable.class);
	    Posiciones posicionesAOcupar1 = new Posiciones();
	    Posiciones posicionesAOcupar2 = new Posiciones();
	    posicionesAOcupar1.agregar(new Posicion(0,0));
	    posicionesAOcupar2.agregar(new Posicion(0,0));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar1);
		mapaATestear.colocar(mockedColocable2, posicionesAOcupar2);

	}
	

	// COMIENZAN PRUEBAS DE CASOS BORDE EN EL MAPA
	
	@Test(expected = PosicionInvalidaException.class)
	public void test03NoDeberiaPoderColocarColocableEnPosicionXneg1Yneg1() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(-1,-1));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}

	
	@Test(expected = PosicionInvalidaException.class)
	public void test04NoDeberiaPoderColocarColocableEnPosicionX64Y64EnUnMapaDeTamanio64() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(64,64));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);
	
	}

	
	@Test(expected = PosicionInvalidaException.class)
	public void test05NoDeberiaPoderColocarColocableEnPosicionXneg1Y64() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(1,64));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}

	
	@Test(expected = PosicionInvalidaException.class)
	public void test06NoDeberiaPoderColocarColocableEnPosicionX64Yneg1() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(64,1));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}

	
	@Test(expected = PosicionInvalidaException.class)
	public void test07NoDeberiaPoderColocarColocableEnPosicionXneg1Y30() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(-1,30));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}


	@Test(expected = PosicionInvalidaException.class)
	public void test08NoDeberiaPoderColocarColocableEnPosicionX30Yneg1() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(30,-1));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}

	@Test(expected = PosicionInvalidaException.class)
	public void test09NoDeberiaPoderColocarColocableEnPosicionX64Y30() {
		Mapa mapaATestear = new Mapa(64);
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(64,30));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}

	@Test(expected = PosicionInvalidaException.class)
	public void test10NoDeberiaPoderColocarColocableEnPosicionX30Y64() {
		Mapa mapaATestear = new Mapa(64);
		Posicion posicionInicial = new Posicion(30,64);
		boolean ocurrioUnaExcepcion = false;
	    Colocable mockedColocable1 = mock(Colocable.class);
	    Posiciones posicionesAOcupar = new Posiciones();
	    posicionesAOcupar.agregar(new Posicion(30,64));
		
		mapaATestear.colocar(mockedColocable1, posicionesAOcupar);

	}
	
	// TERMINAN PRUEBAS DE CASOS BORDE EN MAPA
	
	@Test
	public void test11AlColocarUnColocableDeTamanio4ConOrigenEnX1Y1DeberiaOcuparCeldasHastaX4Y4() {
		Mapa mapaATestear = new Mapa(64);
		Posiciones posicionesAOcupar = new Posiciones();
		for (int i = 1; i < 5; i++) {
			for (int j = 1; j < 5; j++) {
				posicionesAOcupar.agregar(new Posicion(i,j));
			}
		}
	    Colocable mockedColocableDeTamanio4 = mock(Colocable.class);
		
		mapaATestear.colocar(mockedColocableDeTamanio4, posicionesAOcupar);
		
		for(int i = 1; i < 5; i++)
			for(int j = 1; j < 5; j++)
				assert( mapaATestear.estaOcupado( new Posicion(i,j) ) );
		
		
		assertFalse( mapaATestear.estaOcupado( new Posicion(0,0) ) );
		assertFalse( mapaATestear.estaOcupado( new Posicion(5,5) ) );
		assertFalse( mapaATestear.estaOcupado( new Posicion(0,5) ) );
		assertFalse( mapaATestear.estaOcupado( new Posicion(5,0) ) );
	}

	@Test(expected = PosicionInvalidaException.class)
	public void test12AlColocarUnColocableDeTamanio4ConOrigenEnX1Y1NoDeberiaHaberSuperposicion() {
		Mapa mapaATestear = new Mapa(64);
		Posiciones posicionAOcuparTamanio4 = new Posiciones();
		for(int i = 1; i < 5; i++)
			for(int j = 1; j < 5; j++)
				posicionAOcuparTamanio4.agregar(new Posicion(i,j));
		Posiciones posicionAOcuparTamanio1 = new Posiciones();
		posicionAOcuparTamanio1.agregar(new Posicion(1,1));
		Colocable mockedColocableDeTamanio4 = mock(Colocable.class);
		Colocable mockedColocableTamanio1 = mock(Colocable.class);

		
		mapaATestear.colocar(mockedColocableDeTamanio4, posicionAOcuparTamanio4 );
		
		mapaATestear.colocar(mockedColocableTamanio1, posicionAOcuparTamanio1);

	}
	
}