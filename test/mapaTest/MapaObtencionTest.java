package mapaTest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Test;

import colocables.unidades.Colocable;
import geometria.Posicion;
import geometria.Posiciones;
import mapa.*;

public class MapaObtencionTest {

	@Test(expected = PosicionVaciaException.class)
	public void test01AlObtenerUnColocableNoColocadoDeberiaDarError() {
		Mapa mapaATestear = new Mapa(64);
		
		mapaATestear.obtener(new Posicion(0,0));
	}

	@Test
	public void test02AlObtenerUnColocablePreviamenteColocadoEnLaPosicionX0Y0DeberiaDevolverElMismo() {
		Mapa mapaATestear = new Mapa(64);
		Colocable mockedColocable = mock(Colocable.class);
		Posiciones posicionesAOcupar = new Posiciones();
		posicionesAOcupar.agregar(new Posicion(0,0));
		
		mapaATestear.colocar(mockedColocable, posicionesAOcupar);
		
		assert(mockedColocable == mapaATestear.obtener(new Posicion(0,0)));
	}
	
	@Test(expected = PosicionVaciaException.class)
	public void test03NoDeberiaPoderObtenerNadaDeUnaPosicionLuegoDeSuRemocion() {
		Mapa mapaATestear = new Mapa(64);
		Colocable mockedColocable = mock(Colocable.class);
		Posiciones posicionesAOcupar = new Posiciones();
		posicionesAOcupar.agregar(new Posicion(0,0));

		mapaATestear.colocar(mockedColocable, posicionesAOcupar);
		mapaATestear.remover(mockedColocable);
		
		mapaATestear.obtener(new Posicion(0,0));
	}

	@Test
	public void test04NoDeberiaPoderObtenerNadaDeUnaPosicionLuegoDeSuRemocion() {
		Mapa mapaATestear = new Mapa(64);
		Colocable mockedColocable = mock(Colocable.class);
		Posiciones posicionesAOcupar = new Posiciones();
		posicionesAOcupar.agregar(new Posicion(0,0));
		posicionesAOcupar.agregar(new Posicion(1,0));
		posicionesAOcupar.agregar(new Posicion(0,1));
		posicionesAOcupar.agregar(new Posicion(1,1));

		mapaATestear.colocar(mockedColocable, posicionesAOcupar);

		assert(mockedColocable == mapaATestear.obtener(new Posicion(0,0)));
	}
}
