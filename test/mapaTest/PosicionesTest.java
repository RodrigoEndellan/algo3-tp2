package mapaTest;

import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Cuartel;
import colocables.unidades.Aldeano;
import colocables.unidades.Colocable;
import geometria.Posicion;
import geometria.Posiciones;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class PosicionesTest {

    @Test
    public void tes01ConseguirPosicionesDeUnColocableConUnaPosicion(){

        Mapa mapa = new Mapa(64);

        Aldeano unAldeano = new Aldeano();

        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(10,11));

        mapa.colocar(unAldeano, posicionesAldeano);

        Posiciones pAledeano = unAldeano.obtenerPosiciones();

        Colocable colocableAldeano = mapa.obtener(new Posicion(10,11));

        Posiciones posiciones = colocableAldeano.obtenerPosiciones();

        Posicion unaPosicion = posiciones.obtener(0);

        assertEquals(unaPosicion, new Posicion(10,11) );
    }

    @Test
    public void tes02ConseguirPosicionesDeUnColocableConDosPosiciones(){

        Mapa mapa = new Mapa(64);

        Cuartel unCuartel = new Cuartel();

        Posiciones posicionesCuartel = new Posiciones();

        posicionesCuartel.agregar(new Posicion(10,11));
        posicionesCuartel.agregar(new Posicion(10,10));

        mapa.colocar(unCuartel, posicionesCuartel);
        Posiciones pCuartel = unCuartel.obtenerPosiciones();
        Colocable colocableCuartel = mapa.obtener(new Posicion(10,11));

        Posiciones posiciones = colocableCuartel.obtenerPosiciones();

        Posicion unaPosicion = posiciones.obtener(0);

        assertEquals(new Posicion(10,11), unaPosicion);
    }

    @Test
    public void test03ObtenerAreaDe3DeUnaPosicion1010InsertadaEnPosiciones(){
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));
        Posiciones posicionesArea = posiciones.obtenerPosicionesDeArea(2);
        assert (posicionesArea.contiene(new Posicion(11,10)));
        assert (posicionesArea.contiene(new Posicion(12,10)));
        assert (posicionesArea.contiene(new Posicion(10,11)));
        assert (posicionesArea.contiene(new Posicion(10,12)));
        assert (posicionesArea.contiene(new Posicion(11,11)));
        assert (posicionesArea.contiene(new Posicion(12,12)));
        assert (posicionesArea.contiene(new Posicion(9,9)));
        assert (posicionesArea.contiene(new Posicion(9,8)));
        assert (posicionesArea.contiene(new Posicion(8,8)));
        assert (posicionesArea.contiene(new Posicion(8,9)));
        assertFalse (posicionesArea.contiene(new Posicion(12,13)));
        assertFalse (posicionesArea.contiene(new Posicion(8,13)));
    }

    @Test
    public void test04ObtenerAreaDe3Posiciones1010Y1011InsertadaEnPosiciones(){
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(10,10));
        posiciones.agregar(new Posicion(10,11));
        Posiciones posicionesArea = posiciones.obtenerPosicionesDeArea(2);
        assert (posicionesArea.contiene(new Posicion(11,10)));
        assert (posicionesArea.contiene(new Posicion(12,10)));
        assert (posicionesArea.contiene(new Posicion(10,11)));
        assert (posicionesArea.contiene(new Posicion(10,12)));
        assert (posicionesArea.contiene(new Posicion(11,11)));
        assert (posicionesArea.contiene(new Posicion(12,12)));
        assert (posicionesArea.contiene(new Posicion(9,9)));
        assert (posicionesArea.contiene(new Posicion(9,8)));
        assert (posicionesArea.contiene(new Posicion(8,8)));
        assert (posicionesArea.contiene(new Posicion(8,9)));
        assert (posicionesArea.contiene(new Posicion(10,13)));
        assert (posicionesArea.contiene(new Posicion(12,13)));
        assert (posicionesArea.contiene(new Posicion(8,13)));
        assertFalse(posicionesArea.contiene(new Posicion(7,10)));
    }

    @Test
    public void test05ObtenerAreaDe3Posiciones1010Y1011InsertadaEnPosiciones(){
        Posiciones posiciones = new Posiciones();
        posiciones.agregar(new Posicion(0,0));
        Posiciones posicionesArea = posiciones.obtenerPosicionesDeArea(2);
        assert (posicionesArea.contiene(new Posicion(0,1)));
        assert (posicionesArea.contiene(new Posicion(1,0)));
        assert (posicionesArea.contiene(new Posicion(1,1)));
        assert (posicionesArea.contiene(new Posicion(2,1)));
        assert (posicionesArea.contiene(new Posicion(1,2)));
        assert (posicionesArea.contiene(new Posicion(2,2)));
        assert (posicionesArea.contiene(new Posicion(-1,-1)));
        assert (posicionesArea.contiene(new Posicion(-1,0)));
    }

    @Test
    public void test06LaPosicionInferiorIzquierdaEs00(){
        Posiciones posiciones = new Posiciones();

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                posiciones.agregar(new Posicion(i, j));
            }
        }
        assertEquals(new Posicion(0, 0), posiciones.esquinaInferiorIzquierda());
    }

    @Test
    public void test06LaPosicionInferiorIzquierdaEs11(){
        Posiciones posiciones = new Posiciones();

        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                posiciones.agregar(new Posicion(i, j));
            }
        }
        assertEquals(new Posicion(1, 1), posiciones.esquinaSuperiorDerecha());
    }
}
