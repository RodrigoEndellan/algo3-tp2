package EstadosTest;

import org.junit.Test;

import colocables.estados.Estado;
import colocables.estados.Muerto;

import static junit.framework.TestCase.assertFalse;

public class EstadoMuertoTest {

    @Test
    public void test01ElEstadoMuertoDevuelveEstadoInactivo(){
        Estado muerto = new Muerto();
        assertFalse (muerto.estoyActivo());
    }
}
