package CarteraTest;

import creadores.CreadorDeAldeanos;
import org.junit.Test;

import colocables.unidades.Cartera;
import colocables.unidades.NoHaySuficienteOroException;

import static org.junit.Assert.assertEquals;

public class CarteraTest {

    @Test
    public void test01SeCobraUnAldeanoYSeDescuentaElOro(){

        Cartera cartera = new Cartera();
        CreadorDeAldeanos creador = new CreadorDeAldeanos();

        cartera.setOro(150);

        assertEquals(150, cartera.obtenerOro());

        try {
            creador.cobrar(cartera);
        } catch (NoHaySuficienteOroException e) {
            assert(false);
        }

        assertEquals(125, cartera.obtenerOro());
    }

    @Test
    public void test02SeCobraUnAldeanoYSeLanzaLaExcepcionNoHaySuficienteOroException(){

        Cartera cartera = new Cartera();
        CreadorDeAldeanos creador = new CreadorDeAldeanos();

        cartera.setOro(0);

        assertEquals(0, cartera.obtenerOro());

        boolean ocurrioExcepcionNoHaySuficienteOroException = false;

        try {
            creador.cobrar(cartera);
        } catch (NoHaySuficienteOroException e) {
            ocurrioExcepcionNoHaySuficienteOroException = true;
        }
        assert(ocurrioExcepcionNoHaySuficienteOroException);
    }
}
