package Castillo;

import creadores.CreadorDeArmaDeAsedio;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Castillo;
import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.estados.EstaOcupadoException;
import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Colocable;
import colocables.unidades.Poblacion;
import colocables.edificios.JuegoTerminadoException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class CastilloTest {

    @Test
    public void crearArmaDeAsedio(){

        Mapa mapa = new Mapa(64);

        Posiciones posicionesCastillo = new Posiciones();

        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesCastillo.agregar(new Posicion(11,10));
        posicionesCastillo.agregar(new Posicion(10,11));
        posicionesCastillo.agregar(new Posicion(11,11));

        Castillo castillo = new Castillo();

        castillo.conocerMapa(mapa);

        mapa.colocar(castillo, posicionesCastillo);

        CreadorDeArmaDeAsedio creadorDeArmaDeAsedio = new CreadorDeArmaDeAsedio();

        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        creadorDeArmaDeAsedio.asignarEquipo(equipo);

        try {
            castillo.crearUnidadCon(creadorDeArmaDeAsedio);
        } catch (EstaOcupadoException e) {
            assert(false);
        }

        assert (mapa.obtener(new Posicion(9,9)) instanceof ArmaDeAsedio);

    }

    @Test(expected = JuegoTerminadoException.class)
    public void test02AtacarCastilloHastaQueSeaQuitadoDelMapa() {

        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        Edificio castillo = new Castillo();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        Equipo otroEquipo = new Equipo();
        otroEquipo.agregarCiudad(new Ciudad());
        otroEquipo.agregarPoblacion(new Poblacion(50));

        unArmaAsedio.asignarEquipo(equipo);
        castillo.asignarEquipo(otroEquipo);

        equipo.agregar(unArmaAsedio);
        otroEquipo.agregar(castillo);

        Posiciones posicionesArmaAsedio = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10, 10));

        posicionesEdificio.agregar(new Posicion(11, 11));
        posicionesEdificio.agregar(new Posicion(12, 11));
        posicionesEdificio.agregar(new Posicion(11, 12));
        posicionesEdificio.agregar(new Posicion(12, 12));


        mapa.colocar(castillo, posicionesEdificio);
        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.terminarTurno();

        assert(mapa.estaOcupado(new Posicion(11, 11)));
        assert(mapa.estaOcupado(new Posicion(12, 11)));
        assert(mapa.estaOcupado(new Posicion(11, 12)));
        assert(mapa.estaOcupado(new Posicion(12, 12)));

        Colocable unColocable = mapa.obtener(new Posicion(11, 11));
        assertEquals(1000, castillo.obtenerVidaActual());
        while(castillo.obtenerVidaActual()>0){
            unArmaAsedio.atacar(unColocable);
            unArmaAsedio.terminarTurno();

        }

        assertFalse(mapa.estaOcupado(new Posicion(11, 11)));
        assertFalse(mapa.estaOcupado(new Posicion(12, 11)));
        assertFalse(mapa.estaOcupado(new Posicion(11, 12)));
        assertFalse(mapa.estaOcupado(new Posicion(12, 12)));

        //assertEquals(925, castillo.obtenerVidaActual());

    }

}
