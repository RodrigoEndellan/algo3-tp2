package CuartelTest;

import creadores.CreadorDeArqueros;
import creadores.CreadorDeEspadachines;
import creadores.CreadorDeUnidadesDeAtaque;
import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Cuartel;
import colocables.estados.EstaOcupadoException;
import colocables.unidades.Arquero;
import colocables.unidades.Espadachin;
import colocables.unidades.Poblacion;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class CuartelBasicoTest {

    @Test
    public void test01CuartelEnPosicion1010CreaUnEspadachinEnCasi0909eroMasCercano1110() {

        //Mapa mockMapa = mock(Mapa.class);
        //Posicion mockPosicionCuartel = mock(Posicion.class);
        //Posicion mockPosicionEspadachin
       // when(mockPosicionCuartel.x()).thenReturn(1);
       // when(mockPosicionCuartel.y()).thenReturn(1);
        //when(mockMapa.colocar(_cuartel,mockPosicionCuartel));
        //mockMapa.colocar(_cuartel, mockPosicionCuartel);
        //_cuartel.conocerMapa(mockMapa);
        //when(mockMapa.obtener(mockPosicionEspadachin)).thenCallRealMethod();

        Mapa mapa = new Mapa(64);

        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));

        Posicion posicionCuartel1 = new Posicion(10,10);
        Posicion posicionCuartel2 = new Posicion(11,10);
        Posicion posicionCuartel3 = new Posicion(10,11);
        Posicion posicionCuartel4 = new Posicion(11,11);

        Posiciones _posicionCuartel = new Posiciones();

        _posicionCuartel.agregar(posicionCuartel1);
        _posicionCuartel.agregar(posicionCuartel2);
        _posicionCuartel.agregar(posicionCuartel3);
        _posicionCuartel.agregar(posicionCuartel4);

        Posicion posicionEspadachin = new Posicion(9,9);
        Cuartel _cuartel = new Cuartel();
        _cuartel.conocerMapa(mapa);

        _cuartel.construir();
        _cuartel.construir();
        _cuartel.construir();

        mapa.colocar(_cuartel,_posicionCuartel);
        CreadorDeUnidadesDeAtaque _creadorDeEspadachines = new CreadorDeEspadachines();
        _creadorDeEspadachines.asignarEquipo(equipo);
        try {
            _cuartel.crearUnidadCon(_creadorDeEspadachines);
        } catch (EstaOcupadoException e) {
            assert(false);
        }
        assert(mapa.obtener(posicionEspadachin)instanceof Espadachin);

    }
    /*
    @Test
    public void test02VerificarQueSiHayUnEspacioOcupadoPorUnAqrqueEnLaPosicion0909SeGenereElEspadachinEnPosicion0110(){
        /*Mapa mapa = new Mapa(64);
        Posicion posicionCuartel = new Posicion(10,10);
        Posicion posicionEspadachin = new Posicion(9,9);
        Posicion posicionArquero = new Posicion(9,10);
        Arquero unArquero = new Arquero();
        mapa.colocar(unArquero,posicionArquero);
        Cuartel _cuartel = new Cuartel();
        _cuartel.conocerMapa(mapa);
        mapa.colocar(_cuartel,posicionCuartel);
        CreadorDeUnidadesDeAtaque _creadorDeEspadachines = new CreadorDeEspadachines();
        _cuartel.crearUnidadDeAtaqueCon(_creadorDeEspadachines);
        _cuartel.darPosicion(posicionCuartel);
        assertFalse(mapa.obtener(posicionEspadachin)instanceof Espadachin);
    */
    @Test
    public void test02CuartelEnPosicion1010CreaUnArqueroEnCasi0909eroMasCercano1110(){
        Mapa mapa = new Mapa(64);

        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));

        Posiciones _posicionCuartel = new Posiciones();

        _posicionCuartel.agregar(new Posicion(10,10));
        _posicionCuartel.agregar(new Posicion(11,10));
        _posicionCuartel.agregar(new Posicion(10,11));
        _posicionCuartel.agregar(new Posicion(11,11));

        Cuartel _cuartel = new Cuartel();

        _cuartel.conocerMapa(mapa);

        mapa.colocar(_cuartel,_posicionCuartel);

        CreadorDeUnidadesDeAtaque _creadorDeArquero = new CreadorDeArqueros();

        _creadorDeArquero.asignarEquipo(equipo);
        _cuartel.construir();
        _cuartel.construir();
        _cuartel.construir();
        try {
            _cuartel.crearUnidadCon(_creadorDeArquero);
        } catch (EstaOcupadoException e) {
            assert(false);
        }

        Posicion posicionArquero = new Posicion(9,9);

        assert(mapa.obtener(posicionArquero)instanceof Arquero);
    }
}
