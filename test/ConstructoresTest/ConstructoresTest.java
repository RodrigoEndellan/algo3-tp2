package ConstructoresTest;

import colocables.edificios.*;
import constructores.ConstructorDeCastillo;
import constructores.ConstructorDeCuartel;
import constructores.ConstructorDePlazaCentral;
import constructores.NoHayPosicionesValidasException;
import geometria.Posicion;
import jugadores.Equipo;
import mapa.Mapa;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConstructoresTest {

    @Test
    public void test01SeCreaPlazaCentral(){

        Mapa mapa = mock(Mapa.class);
        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        ConstructorDePlazaCentral constructorDePlazaCentral = new ConstructorDePlazaCentral();
        constructorDePlazaCentral.asignarEquipo(equipo);
        Edificio edificio = constructorDePlazaCentral.construir(new Posicion(0,0));
        assert (edificio instanceof PlazaCentral);
    }

    @Test
    public void test02SeReasignanPosicionesDeUnEdificioYCuamdoNoHayMasLevantaErrorNoHayMasPosicionesValidasException(){
        Mapa mapa = mock(Mapa.class);
        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        ConstructorDePlazaCentral constructorDePlazaCentral = new ConstructorDePlazaCentral();
        constructorDePlazaCentral.asignarEquipo(equipo);
        Edificio edificio = constructorDePlazaCentral.construir(new Posicion(0,0));

        for(int i = 0; i < 3; i++){
            try {
                constructorDePlazaCentral.reasignarPosicion(edificio);
            } catch (NoHayPosicionesValidasException e) {
                assert(false);
            }
        }

        boolean noHayPosicionesValidasSucedio = false;
        try {
            constructorDePlazaCentral.reasignarPosicion(edificio);
        } catch (NoHayPosicionesValidasException e) {
            noHayPosicionesValidasSucedio = true;
        }
        assert(noHayPosicionesValidasSucedio);
    }

    @Test
    public void test03SeCreaCuartel(){

        Mapa mapa = mock(Mapa.class);
        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        ConstructorDeCuartel constructorDeCuartel = new ConstructorDeCuartel();
        constructorDeCuartel.asignarEquipo(equipo);
        Edificio edificio = constructorDeCuartel.construir(new Posicion(0,0));
        assert (edificio instanceof Cuartel);
    }

    @Test
    public void test04SeCreaCastillo(){
        Mapa mapa = mock(Mapa.class);
        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        ConstructorDeCastillo constructorDeCastillo = new ConstructorDeCastillo();
        constructorDeCastillo.asignarEquipo(equipo);
        Edificio edificio = constructorDeCastillo.construir(new Posicion(0,0));
        assert (edificio instanceof Castillo);
    }

}
