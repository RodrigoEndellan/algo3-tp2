package CreadorDeUnidadesTest;

import creadores.CreadorDeAldeanos;
import geometria.Posicion;
import jugadores.Equipo;

import org.junit.Test;

import colocables.unidades.Aldeano;
import colocables.unidades.Poblacion;
import colocables.unidades.Unidad;

public class CreadorDeAldeanosTest {

    @Test
    public void test01CrearUnAldeano(){
        CreadorDeAldeanos _creadorDeAldeanos = new CreadorDeAldeanos();
        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        _creadorDeAldeanos.asignarEquipo(equipo);
        Unidad unEspadachin = _creadorDeAldeanos.crearUnidad();
        assert (unEspadachin instanceof Aldeano);
    }
}
