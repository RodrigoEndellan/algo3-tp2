package PosicionadorTest;

import mapa.Mapa;

import org.junit.Test;

import colocables.unidades.Colocable;
import colocables.unidades.Espadachin;
import geometria.Posicion;
import geometria.Posicionador;
import geometria.Posiciones;

public class PosicionadorTest {

    @Test
    public void test01IntentarPosicionarUnColocableEnLaPosicionVaciaAbajoALaIzquierdaConExito(){
        Mapa mapa = new Mapa(64);

        Posicion posicionDeOrigen = new Posicion(10,10);
        Posicion posicionNuevaOcupada = new Posicion(9,9);
        Colocable unaUnidad = new Espadachin();
        Posicionador.posicionar(mapa,posicionDeOrigen,unaUnidad);
        assert(mapa.estaOcupado(posicionNuevaOcupada));
    }

    @Test
    public void test02IntentarPosicionarUnColocableEnLaPosicionAbajoOcupadaALaIzquierdaPeroLaPosicionaAbajoEnCentro(){
        Mapa mapa = new Mapa(64);
        Posicion posicionDeOrigen = new Posicion(10,10);
        Posicion posicionOcupada = new Posicion(9,9);
        Posicion posicionNuevaOcupada = new Posicion(9,10);
        Colocable unaUnidad = new Espadachin();
        Colocable unaAnteriormenteUbicada = new Espadachin();
        Posiciones _posicionesOcupada = new Posiciones();
        _posicionesOcupada.agregar(posicionOcupada);
        mapa.colocar(unaAnteriormenteUbicada, _posicionesOcupada);
        Posicionador.posicionar(mapa,posicionDeOrigen,unaUnidad);
        assert(mapa.estaOcupado(posicionNuevaOcupada));
    }

    /*@Test
    public void test03IntentarPosicionarUnColocableEnLaPosicionAbajoOcupadaALaIzquierdaPeroTodoEstaOcupadoMenosSuultimaPosicionArribaALaDerecha(){
        Mapa mapaMockeado = mock(Mapa.class);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(10, 10))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(9, 9))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(10, 9))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(11, 9))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(9, 10))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(10, 11))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(9, 11))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(10, 11))).thenReturn(true);
        Mockito.when(mapaMockeado.estaOcupado(new Posicion(11, 10))).thenReturn(true);
        Posicion posicionDeOrigen = new Posicion(10,10);
        Posicion posicionNuevaOcupada = new Posicion(11,11);
        Posiciones _posicionNuevaOcupada = new Posiciones();
        _posicionNuevaOcupada.agregar(posicionNuevaOcupada);
        //Posicion posicionMock = mock(Posicion.class);
        Colocable unaUnidad = new Espadachin();
        Posicionador.posicionar(mapaMockeado,posicionDeOrigen,unaUnidad);
        doNothing().when(mapaMockeado).colocar(unaUnidad,_posicionNuevaOcupada);
        verify(mapaMockeado, times(1)).colocar(unaUnidad,_posicionNuevaOcupada);

    }*/


    @Test
    public void test04IntentarPosicionarUnColocableConPosicionesEnLaPosicionVaciaAbajoALaIzquierdaConExito(){
        Mapa mapa = new Mapa(64);

        Posicion posicionDeOrigen = new Posicion(10,10);
        Posicion posicionNuevaOcupada = new Posicion(9,9);
        Posiciones _posicionesDeOrigen = new Posiciones();
        _posicionesDeOrigen.agregar(posicionDeOrigen);
        Colocable unaUnidad = new Espadachin();
        Posicionador.posicionar(mapa,_posicionesDeOrigen,unaUnidad);
        assert(mapa.estaOcupado(posicionNuevaOcupada));
    }

    @Test
    public void test05IntentarPosicionarUnColocableConListaDePosicionesConUnaPosicionEnLaPosicionAbajoOcupadaALaIzquierdaPeroLaPosicionaAbajoEnCentro(){
        Mapa mapa = new Mapa(64);
        Posicion posicionDeOrigen = new Posicion(10,10);
        Posicion posicionOcupada = new Posicion(9,9);
        Posicion posicionNuevaOcupada = new Posicion(9,10);
        Colocable unaUnidad = new Espadachin();
        Colocable unaAnteriormenteUbicada = new Espadachin();
        Posiciones _posicionesOcupada = new Posiciones();
        _posicionesOcupada.agregar(posicionOcupada);
        mapa.colocar(unaAnteriormenteUbicada, _posicionesOcupada);
        Posiciones _posicionesOrigen = new Posiciones();
        _posicionesOrigen.agregar(posicionDeOrigen);
        Posicionador.posicionar(mapa,_posicionesOrigen,unaUnidad);
        assert(mapa.estaOcupado(posicionNuevaOcupada));
    }

    @Test
    public void test06IntentarPosicionarUnColocableEnUnaPosicionInvalidaLoPosicionaEnLaProooximaPosicionValida(){
        Mapa mapa = new Mapa(64);

        Posicion posicionDeOrigen = new Posicion(10,10);
        Posicion posicionNuevaOcupada = new Posicion(9,9);
        Colocable unaUnidad = new Espadachin();
        Posicionador.posicionar(mapa,posicionDeOrigen,unaUnidad);
        assert(mapa.estaOcupado(posicionNuevaOcupada));
    }
}
