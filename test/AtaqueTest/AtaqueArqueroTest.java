package AtaqueTest;

import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.estados.Estado;
import colocables.estados.Jugado;
import colocables.unidades.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;

public class AtaqueArqueroTest {

    @Test
    public void test01ArqueroEnPosicion1010AtacaColocableAldeanoEnPosicion1111(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unAldeano.asignarEquipo(equipo);
        unAquero.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(11,11));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);


        assertEquals(50,unAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unAquero.atacar(unColocable);
        assertEquals(35,unAldeano.obtenerVidaActual() );


    }


    @Test
    public void test02ArqueroEnPosicion1010AtacaColocableAldeanoEnPosicion1212(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unAldeano.asignarEquipo(equipo);
        unAquero.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(12,12));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);


        assertEquals(50,unAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(12,12));
        unAquero.atacar(unColocable);
        assertEquals(35,unAldeano.obtenerVidaActual() );


    }

    @Test
    public void test03ArqueroEnPosicion1010AtacaColocableAldeanoEnPosicion1313(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unAldeano.asignarEquipo(equipo);
        unAquero.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(13,13));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);


        assertEquals(50,unAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(13,13));
        unAquero.atacar(unColocable);
        assertEquals(35,unAldeano.obtenerVidaActual() );


    }

    @Test(expected = AtaqueFueraDeRangoException.class)
    public void test04ArqueroEnPosicion1010AtacaColocableAldeanoEnPosicion1414(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(14,14));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);


        assertEquals(50,unAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(14,14));
        unAquero.atacar(unColocable);

    }

    @Test
    public void Test04EspadachinenPosicion1010AtacaUnColocableCuartelEnPosicionAdyacenteQueEsUnEdificio(){
        Mapa mapa = new Mapa(64);

        Arquero unArquero = new Arquero();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unArquero.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArquero.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArquero, posicionesArquero);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArquero.atacar(unColocable);
        assertEquals(240,unEdificio.obtenerVidaActual());
    }

        @Test
        public void Test05EspadachinenPosicion1010AtacaUnColocableCuartelEnPosicionAdyacente1212QueEsUnEdificio(){
            Mapa mapa = new Mapa(64);

            Arquero unArquero = new Arquero();
            Edificio unEdificio = new Cuartel();

            Equipo equipo = new Equipo();
            equipo.agregarCiudad(new Ciudad());
            equipo.agregarPoblacion(new Poblacion(50));

            unArquero.asignarEquipo(equipo);
            unEdificio.asignarEquipo(equipo);


            Posiciones posicionesArquero = new Posiciones();
            Posiciones posicionesEdificio = new Posiciones();

            posicionesArquero.agregar(new Posicion(10,10));

            posicionesEdificio.agregar(new Posicion(11,11));
            posicionesEdificio.agregar(new Posicion(12,11));
            posicionesEdificio.agregar(new Posicion(11,12));
            posicionesEdificio.agregar(new Posicion(12,12));


            mapa.colocar(unEdificio, posicionesEdificio);
            mapa.colocar(unArquero, posicionesArquero);

            Colocable unColocable = mapa.obtener(new Posicion(12,12));
            assertEquals(250,unEdificio.obtenerVidaActual());
            unArquero.atacar(unColocable);
            assertEquals(240,unEdificio.obtenerVidaActual());
        }

    @Test
    public void Test06EspadachinenPosicion1010AtacaUnColocableCuartelEnPosicion1313QueEsUnEdificio(){
        Mapa mapa = new Mapa(64);

        Arquero unArquero = new Arquero();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unArquero.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArquero.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(13,13));
        posicionesEdificio.agregar(new Posicion(14,13));
        posicionesEdificio.agregar(new Posicion(13,14));
        posicionesEdificio.agregar(new Posicion(14,14));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArquero, posicionesArquero);

        Colocable unColocable = mapa.obtener(new Posicion(13,13));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArquero.atacar(unColocable);
        assertEquals(240,unEdificio.obtenerVidaActual());
    }

    @Test
    public void Test07EspadachinenPosicion1010AtacaUnColocableCuartelEnPosicion1313ASuPosicion1414QueEsUnEdificio(){
        Mapa mapa = new Mapa(64);

        Arquero unArquero = new Arquero();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unArquero.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArquero.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(13,13));
        posicionesEdificio.agregar(new Posicion(14,13));
        posicionesEdificio.agregar(new Posicion(13,14));
        posicionesEdificio.agregar(new Posicion(14,14));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArquero, posicionesArquero);

        Colocable unColocable = mapa.obtener(new Posicion(14,14));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArquero.atacar(unColocable);
        assertEquals(240,unEdificio.obtenerVidaActual());
    }

    @Test(expected = AtaqueFueraDeRangoException.class)
    public void Test08EspadachinenPosicion1010AtacaUnColocableCuartelEnPosicion1515QueEsUnEdificioLevantaFueraDeRangoException(){
        Mapa mapa = new Mapa(64);

        Arquero unArquero = new Arquero();
        Edificio unEdificio = new Cuartel();

        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArquero.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(14,14));
        posicionesEdificio.agregar(new Posicion(14,15));
        posicionesEdificio.agregar(new Posicion(15,14));
        posicionesEdificio.agregar(new Posicion(15,15));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArquero, posicionesArquero);

        Colocable unColocable = mapa.obtener(new Posicion(14,14));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArquero.atacar(unColocable);
    }

    @Test
    public void test09ArqueroEnPosicion1010AtacaColocableAldeanoEnPosicion1111DeOtroEquipo(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        equipo.agregarCiudad(new Ciudad());

        Equipo otroEquipo = new Equipo();
        otroEquipo.agregarPoblacion(new Poblacion(50));
        otroEquipo.agregarCiudad(new Ciudad());


        unAquero.asignarEquipo(equipo);
        unAldeano.asignarEquipo(otroEquipo);

        equipo.agregar(unAquero);
        otroEquipo.agregar(unAldeano);

        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(11,11));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);


        assertEquals(50,unAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unAquero.atacar(unColocable);
        assertEquals(35,unAldeano.obtenerVidaActual() );


    }

    @Test(expected = MovimientoInvalidoException.class)
    public void test10ArqueroAtacaAUnAldeanoDelMismoEquipoLevantaMovimientoInvalidoException(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));

        unAquero.asignarEquipo(equipo);
        unAldeano.asignarEquipo(equipo);

        equipo.agregar(unAquero);
        equipo.agregar(unAldeano);

        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(11,11));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unAquero.atacar(unColocable);

    }

    @Test
    public void test11Arquero0AtacaColocableAldeanoAqueroPasaNoEstarLibre(){

        Mapa mapa = new Mapa(64);

        Arquero unAquero = new Arquero();
        Aldeano unAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unAldeano.asignarEquipo(equipo);
        unAquero.asignarEquipo(equipo);


        Posiciones posicionesArquero = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesAldeano.agregar(new Posicion(11,11));
        posicionesArquero.agregar(new Posicion(10,10));

        mapa.colocar(unAquero, posicionesArquero);
        mapa.colocar(unAldeano,posicionesAldeano);


        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unAquero.atacar(unColocable);
        Estado estado = unAquero.obtenerEstado();
        assertFalse(estado.validarQueSeaLibre());

    }
}
