package AtaqueTest;

import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.estados.Muerto;
import colocables.unidades.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;

public class AtaquesEspadachinTest {

    @Test (expected = AtaqueFueraDeRangoException.class)
    public void Test01EspadachinenPosicion1010AtacaUnAldeanoUnColocableEnPosicion1212LevantaFueraDeRangoException(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Aldeano unaAldeano = new Aldeano();

        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(12,12));

        unEspdachin.darPosiciones(posicionesEspadachin);
        unaAldeano.darPosiciones(posicionesAldeano);

        mapa.colocar(unaAldeano, posicionesAldeano);
        mapa.colocar(unEspdachin, posicionesEspadachin);

        Colocable unColocable = mapa.obtener(new Posicion(12,12));
        unEspdachin.atacar(unColocable);

    }

    @Test
    public void Test02EspadachinenPosicion1010AtacaUnAldeanoUnColocableEnPosicion1111(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Aldeano unaAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unaAldeano.asignarEquipo(equipo);
        unEspdachin.asignarEquipo(equipo);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));

        unEspdachin.darPosiciones(posicionesEspadachin);
        unaAldeano.darPosiciones(posicionesAldeano);

        mapa.colocar(unaAldeano, posicionesAldeano);
        mapa.colocar(unEspdachin, posicionesEspadachin);


        assertEquals(50,unaAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unEspdachin.atacar(unColocable);
        assertEquals(25,unaAldeano.obtenerVidaActual() );
    }

    @Test(expected = AtaqueFueraDeRangoException.class)
    public void Test03EspadachinenPosicion1010AtacaUnCuartelEnPosicion1212(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Edificio unEdificio = new Cuartel();

        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(12,12));
        posicionesEdificio.agregar(new Posicion(12,13));
        posicionesEdificio.agregar(new Posicion(13,12));
        posicionesEdificio.agregar(new Posicion(13,13));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unEspdachin, posicionesEspadachin);

        Colocable unColocable = mapa.obtener(new Posicion(12,12));
        unEspdachin.atacar(unColocable);
    }

    @Test
    public void Test04EspadachinenPosicion1010AtacaUnColocableEnPosicionAdyacenteQueEsUnEdificio(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unEspdachin.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unEspdachin, posicionesEspadachin);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unEspdachin.atacar(unColocable);
        assertEquals(235,unEdificio.obtenerVidaActual());
    }

    @Test
    public void Test05EspadachinenPosicion1010AtacaUnColocableArqueroEnPosicion1111(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Arquero unaArquero = new Arquero();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unaArquero.asignarEquipo(equipo);
        unEspdachin.asignarEquipo(equipo);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesArquero = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));
        posicionesArquero.agregar(new Posicion(11,11));

        unEspdachin.darPosiciones(posicionesEspadachin);
        unaArquero.darPosiciones(posicionesArquero);

        mapa.colocar(unaArquero, posicionesArquero);
        mapa.colocar(unEspdachin, posicionesEspadachin);


        assertEquals(75,unaArquero.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unEspdachin.atacar(unColocable);
        assertEquals(50,unaArquero.obtenerVidaActual() );
    }

    @Test (expected = MovimientoInvalidoException.class)
    public void Test06Espadachin0AtacaUnAldeanoDelMismoEquipoLevantaMovimientoInvalidoException(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Aldeano unaAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));

        equipo.agregar(unEspdachin);
        equipo.agregar(unaAldeano);
        unaAldeano.asignarEquipo(equipo);
        unEspdachin.asignarEquipo(equipo);

        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));

        unEspdachin.darPosiciones(posicionesEspadachin);
        unaAldeano.darPosiciones(posicionesAldeano);

        mapa.colocar(unaAldeano, posicionesAldeano);
        mapa.colocar(unEspdachin, posicionesEspadachin);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unEspdachin.atacar(unColocable);

    }

    @Test
    public void Test07EspadachinenPosicion1010AtacaUnAldeanoUnColocableEnPosicion1111TerminaTurnoElAdenoSigueMuerto(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Aldeano unaAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        Equipo otroEquipo = new Equipo();
        otroEquipo.agregarPoblacion(new Poblacion(50));
        otroEquipo.agregarCiudad(new Ciudad());

        unaAldeano.asignarEquipo(otroEquipo);
        unEspdachin.asignarEquipo(equipo);

        equipo.agregar(unEspdachin);
        otroEquipo.agregar(unaAldeano);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));

        unEspdachin.darPosiciones(posicionesEspadachin);
        unaAldeano.darPosiciones(posicionesAldeano);

        mapa.colocar(unaAldeano, posicionesAldeano);
        mapa.colocar(unEspdachin, posicionesEspadachin);


        assertEquals(50,unaAldeano.obtenerVidaActual());
        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unEspdachin.atacar(unColocable);
        assertEquals(25,unaAldeano.obtenerVidaActual() );
        assert (otroEquipo.incluye(unaAldeano));
        assert (mapa.estaOcupado(new Posicion(11,11)));
        unEspdachin.terminarTurno();
        unEspdachin.atacar(unColocable);
        assertEquals(0,unaAldeano.obtenerVidaActual() );
        assert (unaAldeano.obtenerEstado() instanceof Muerto);
        unaAldeano.terminarTurno();
        assert (unaAldeano.obtenerEstado() instanceof Muerto);
        otroEquipo.remover(unaAldeano);
        assertFalse (otroEquipo.incluye(unaAldeano));
        assertFalse (mapa.estaOcupado(new Posicion(11,11)));

    }

    @Test
    public void Test08EspadachinenPosicion1010AtacaUnAldeanoUnColocableEnPosicion1111(){
        Mapa mapa = new Mapa(64);

        Espadachin unEspdachin = new Espadachin();
        Aldeano unaAldeano = new Aldeano();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unaAldeano.asignarEquipo(equipo);
        unEspdachin.asignarEquipo(equipo);


        Posiciones posicionesEspadachin = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesEspadachin.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));

        unEspdachin.darPosiciones(posicionesEspadachin);
        unaAldeano.darPosiciones(posicionesAldeano);

        mapa.colocar(unaAldeano, posicionesAldeano);
        mapa.colocar(unEspdachin, posicionesEspadachin);


        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unEspdachin.atacar(unColocable);
        assertFalse(unEspdachin.obtenerEstado().validarQueSeaLibre());
    }
}
