package AtaqueTest;

import colocables.unidades.MovimientoInvalidoException;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Castillo;
import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.unidades.Aldeano;
import colocables.unidades.Poblacion;
import geometria.Posicion;
import geometria.Posiciones;

import static junit.framework.TestCase.assertEquals;

public class AtaqueCastilloTest {

    @Test
    public void test01CastilloEnPoscicion1010AtacaUnAldeanoEnPosicion1111(){

        Mapa mapa = new Mapa(64);

        Castillo castillo = new Castillo();
        Aldeano aldeano = new Aldeano();

        Posiciones posicionesCastillo = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));

        mapa.colocar(castillo,posicionesCastillo);
        mapa.colocar(aldeano,posicionesAldeano);

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        castillo.asignarEquipo(equipo);
        aldeano.asignarEquipo(equipo);
        Aldeano aldeanoColocado = (Aldeano) mapa.obtener(new Posicion(11,11));
        assertEquals (50,aldeanoColocado.obtenerVidaActual());
        castillo.atacar();
        assertEquals (30,aldeanoColocado.obtenerVidaActual());



    }

    @Test
    public void test02CastilloEnPoscicion1010AtacaADosAldeanos(){

        Mapa mapa = new Mapa(64);

        Castillo castillo = new Castillo();
        Aldeano aldeano = new Aldeano();
        Aldeano aldeano2 = new Aldeano();

        Posiciones posicionesCastillo = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();
        Posiciones posicionesAldeano2 = new Posiciones();

        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));
        posicionesAldeano2.agregar(new Posicion(12,12));

        mapa.colocar(castillo,posicionesCastillo);
        mapa.colocar(aldeano,posicionesAldeano);
        mapa.colocar(aldeano2,posicionesAldeano2);

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        castillo.asignarEquipo(equipo);
        aldeano.asignarEquipo(equipo);
        aldeano2.asignarEquipo(equipo);

        Aldeano aldeanoColocado = (Aldeano) mapa.obtener(new Posicion(11,11));
        Aldeano aldeanoColocado2 = (Aldeano) mapa.obtener(new Posicion(12,12));

        assertEquals (50,aldeanoColocado.obtenerVidaActual());
        assertEquals (50,aldeanoColocado2.obtenerVidaActual());
        castillo.atacar();
        assertEquals (30,aldeanoColocado.obtenerVidaActual());
        assertEquals (30,aldeanoColocado2.obtenerVidaActual());

    }

    @Test
    public void test03CastilloEnPoscicion1010AtacaADosAldeanosYUnCuartel(){

        Mapa mapa = new Mapa(64);

        Castillo castillo = new Castillo();
        Aldeano aldeano = new Aldeano();
        Aldeano aldeano2 = new Aldeano();
        Cuartel cuartel = new Cuartel();

        Posiciones posicionesCastillo = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();
        Posiciones posicionesAldeano2 = new Posiciones();
        Posiciones posicionesCuartel = new Posiciones();

        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesAldeano.agregar(new Posicion(11,11));
        posicionesAldeano2.agregar(new Posicion(12,12));
        posicionesCuartel.agregar(new Posicion(9,9));
        posicionesCuartel.agregar(new Posicion(9,8));
        posicionesCuartel.agregar(new Posicion(8,9));
        posicionesCuartel.agregar(new Posicion(8,8));


        mapa.colocar(castillo,posicionesCastillo);
        mapa.colocar(aldeano,posicionesAldeano);
        mapa.colocar(aldeano2,posicionesAldeano2);
        mapa.colocar(cuartel, posicionesCuartel);

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        castillo.asignarEquipo(equipo);
        aldeano.asignarEquipo(equipo);
        aldeano2.asignarEquipo(equipo);
        cuartel.asignarEquipo(equipo);

        Aldeano aldeanoColocado = (Aldeano) mapa.obtener(new Posicion(11,11));
        Aldeano aldeanoColocado2 = (Aldeano) mapa.obtener(new Posicion(12,12));
        Cuartel cuartelColodo = (Cuartel) mapa.obtener(new Posicion(9,9));

        assertEquals (50,aldeanoColocado.obtenerVidaActual());
        assertEquals (50,aldeanoColocado2.obtenerVidaActual());
        assertEquals(250,cuartelColodo.obtenerVidaActual());
        castillo.atacar();
        assertEquals (30,aldeanoColocado.obtenerVidaActual());
        assertEquals (30,aldeanoColocado2.obtenerVidaActual());
        assertEquals(230,cuartelColodo.obtenerVidaActual());

    }

    @Test
    public void test04CastilloEnPoscicion0101AtacaADosAldeanosYUnCuartel(){

        Mapa mapa = new Mapa(64);

        Castillo castillo = new Castillo();
        Aldeano aldeano = new Aldeano();
        Aldeano aldeano2 = new Aldeano();
        Cuartel cuartel = new Cuartel();

        Posiciones posicionesCastillo = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();
        Posiciones posicionesAldeano2 = new Posiciones();
        Posiciones posicionesCuartel = new Posiciones();

        posicionesCastillo.agregar(new Posicion(0,0));
        posicionesAldeano.agregar(new Posicion(1,0));
        posicionesAldeano2.agregar(new Posicion(0,1));
        posicionesCuartel.agregar(new Posicion(2,2));
        posicionesCuartel.agregar(new Posicion(2,3));
        posicionesCuartel.agregar(new Posicion(3,2));
        posicionesCuartel.agregar(new Posicion(3,3));


        mapa.colocar(castillo,posicionesCastillo);
        mapa.colocar(aldeano,posicionesAldeano);
        mapa.colocar(aldeano2,posicionesAldeano2);
        mapa.colocar(cuartel, posicionesCuartel);

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        castillo.asignarEquipo(equipo);
        aldeano.asignarEquipo(equipo);
        aldeano2.asignarEquipo(equipo);
        cuartel.asignarEquipo(equipo);

        Aldeano aldeanoColocado = (Aldeano) mapa.obtener(new Posicion(1,0));
        Aldeano aldeanoColocado2 = (Aldeano) mapa.obtener(new Posicion(0,1));
        Cuartel cuartelColodo = (Cuartel) mapa.obtener(new Posicion(2,2));

        assertEquals (50,aldeanoColocado.obtenerVidaActual());
        assertEquals (50,aldeanoColocado2.obtenerVidaActual());
        assertEquals(250,cuartelColodo.obtenerVidaActual());
        castillo.atacar();
        assertEquals (30,aldeanoColocado.obtenerVidaActual());
        assertEquals (30,aldeanoColocado2.obtenerVidaActual());
        assertEquals(230,cuartelColodo.obtenerVidaActual());

    }

    @Test
    public void test05CastilloCreadoConVariasPosicionesAtacaUnAldeano(){

        Mapa mapa = new Mapa(64);

        Castillo castillo = new Castillo();
        Aldeano aldeano = new Aldeano();

        Posiciones posicionesCastillo = new Posiciones();
        Posiciones posicionesAldeano = new Posiciones();

        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesCastillo.agregar(new Posicion(9,9));
        posicionesCastillo.agregar(new Posicion(10,9));
        posicionesCastillo.agregar(new Posicion(9,10));
        posicionesAldeano.agregar(new Posicion(11,11));

        mapa.colocar(castillo,posicionesCastillo);
        mapa.colocar(aldeano,posicionesAldeano);

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        Equipo otroEquipo = new Equipo();
        otroEquipo.agregarCiudad(new Ciudad());
        otroEquipo.agregarPoblacion(new Poblacion(50));


        castillo.asignarEquipo(equipo);
        aldeano.asignarEquipo(otroEquipo);

        equipo.agregar(castillo);
        otroEquipo.agregar(aldeano);

        assertEquals (1000,castillo.obtenerVidaActual());
        Aldeano aldeanoColocado = (Aldeano) mapa.obtener(new Posicion(11,11));
        assertEquals (50,aldeanoColocado.obtenerVidaActual());
        castillo.atacar();
        assertEquals (1000,castillo.obtenerVidaActual());
        assertEquals (30,aldeanoColocado.obtenerVidaActual());


    }

    @Test
    public void test06CastilloEnPoscicionAtacaAUnCuartelDelMismoEquipo(){

        Mapa mapa = new Mapa(64);

        Castillo castillo = new Castillo();
        Cuartel cuartel = new Cuartel();

        Posiciones posicionesCastillo = new Posiciones();
        Posiciones posicionesCuartel = new Posiciones();

        posicionesCastillo.agregar(new Posicion(10,10));
        posicionesCuartel.agregar(new Posicion(9,9));
        posicionesCuartel.agregar(new Posicion(9,8));
        posicionesCuartel.agregar(new Posicion(8,9));
        posicionesCuartel.agregar(new Posicion(8,8));


        mapa.colocar(castillo,posicionesCastillo);
        mapa.colocar(cuartel, posicionesCuartel);

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        castillo.asignarEquipo(equipo);
        cuartel.asignarEquipo(equipo);

        equipo.agregar(castillo);
        equipo.agregar(cuartel);

        Cuartel cuartelColodo = (Cuartel) mapa.obtener(new Posicion(9,9));

        assertEquals(250,cuartelColodo.obtenerVidaActual());
        castillo.atacar();
        assertEquals(250,cuartelColodo.obtenerVidaActual());

    }

}
