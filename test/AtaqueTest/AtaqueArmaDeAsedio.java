package AtaqueTest;

import geometria.Posicion;
import geometria.Posiciones;
import jugadores.Equipo;
import mapa.Mapa;

import org.junit.Test;

import colocables.edificios.Castillo;
import colocables.edificios.Ciudad;
import colocables.edificios.Cuartel;
import colocables.edificios.Edificio;
import colocables.estados.Estado;
import colocables.unidades.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class AtaqueArmaDeAsedio {

    @Test
    public void test01ArmaDeAsedioMontadaAvanzaUnTurnoAtacaAUnColocableCuartel(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unArmaAsedio.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);

        Posiciones posicionesArmaAsedio = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.terminarTurno();

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArmaAsedio.atacar(unColocable);
        assertEquals(175,unEdificio.obtenerVidaActual());
    }

    @Test(expected = MovimientoInvalidoException.class)
    public void test02ArmaDeAsedioDesmontadaAtacaAUnColocableCuartelDevuelveArmaDeAsedioSinMonturaException(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        Edificio unEdificio = new Cuartel();

        Posiciones posicionesArmaAsedio = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArmaAsedio.atacar(unColocable);
    }

    @Test (expected = MovimientoInvalidoException.class)
    public void test03ArmaDeAsedioMontadaNoAvanzaUnTurnoAtacaAUnColocableCuartelLevantaMovimientoInvalidoException(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        Edificio unEdificio = new Cuartel();

        Posiciones posicionesArmaAsedio = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);
        unArmaAsedio.cambiarMontura();

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        unArmaAsedio.atacar(unColocable);

    }

    @Test(expected = MovimientoInvalidoException.class)
    public void test04ArmaDeAsedioMontadaAvanzaUnTurnoAtacaAUnCuartelDelMismoEquipoLevantaMovimientoInvalidoException(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        Edificio unEdificio = new Cuartel();

        Equipo equipo = new Equipo();
        equipo.agregarCiudad(new Ciudad());
        equipo.agregarPoblacion(new Poblacion(50));

        unArmaAsedio.asignarEquipo(equipo);
        unEdificio.asignarEquipo(equipo);

        equipo.agregar(unEdificio);
        equipo.agregar(unArmaAsedio);

        Posiciones posicionesArmaAsedio = new Posiciones();
        Posiciones posicionesEdificio = new Posiciones();

        posicionesArmaAsedio.agregar(new Posicion(10,10));

        posicionesEdificio.agregar(new Posicion(11,11));
        posicionesEdificio.agregar(new Posicion(12,11));
        posicionesEdificio.agregar(new Posicion(11,12));
        posicionesEdificio.agregar(new Posicion(12,12));


        mapa.colocar(unEdificio, posicionesEdificio);
        mapa.colocar(unArmaAsedio, posicionesArmaAsedio);

        unArmaAsedio.cambiarMontura();
        unArmaAsedio.terminarTurno();

        Colocable unColocable = mapa.obtener(new Posicion(11,11));
        assertEquals(250,unEdificio.obtenerVidaActual());
        unArmaAsedio.atacar(unColocable);
    }

    @Test
    public void test05ArmaDeAsedioCambiaTipoDemonturaYaNoEsLibreParaHacerOtroMovimiento(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        unArmaAsedio.cambiarMontura();
        Estado estado = unArmaAsedio.obtenerEstado();
        assertFalse (estado.validarQueSeaLibre());

    }

    @Test(expected = MovimientoInvalidoException.class)
    public void test06ArmaDeAsedioSeMontaYSeDesmontaIntentaAtacarLevantaMovimientoInvalidaException(){
        Mapa mapa = new Mapa(64);

        ArmaDeAsedio unArmaAsedio = new ArmaDeAsedio();
        unArmaAsedio.cambiarMontura();
        unArmaAsedio.terminarTurno();
        unArmaAsedio.cambiarMontura();
        Castillo castillo = new Castillo();
        unArmaAsedio.atacar(castillo);

    }



}
