package CreadoresDeUnidadesDeAtaqueTest;

import creadores.CreadorDeArqueros;
import jugadores.Equipo;
import org.junit.Test;

import colocables.unidades.Arquero;
import colocables.unidades.Poblacion;
import colocables.unidades.Unidad;
import colocables.unidades.UnidadDeAtaque;

public class CreadorDeArquerosTest {

    @Test
    public void test01CrearUnArqueros(){
        CreadorDeArqueros _creadorDeArqueros = new CreadorDeArqueros();
        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        _creadorDeArqueros.asignarEquipo(equipo);
        Unidad unEspadachin = _creadorDeArqueros.crearUnidad();
        assert (unEspadachin instanceof Arquero);
    }
}
