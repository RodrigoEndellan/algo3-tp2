package CreadoresDeUnidadesDeAtaqueTest;

import creadores.CreadorDeArmaDeAsedio;
import jugadores.Equipo;
import org.junit.Test;

import colocables.unidades.ArmaDeAsedio;
import colocables.unidades.Poblacion;
import colocables.unidades.Unidad;
import colocables.unidades.UnidadDeAtaque;

public class CreadorDeArmaDeAsedioTest {

    @Test
    public void test01CrearArmaDeAsedio(){
        CreadorDeArmaDeAsedio _creadorDeArmaDeAsedio = new CreadorDeArmaDeAsedio();
        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        _creadorDeArmaDeAsedio.asignarEquipo(equipo);
        Unidad unArmaDeAsedio = _creadorDeArmaDeAsedio.crearUnidad();
        assert (unArmaDeAsedio instanceof ArmaDeAsedio);
    }
}
