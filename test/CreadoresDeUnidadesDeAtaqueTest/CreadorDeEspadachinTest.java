package CreadoresDeUnidadesDeAtaqueTest;

import creadores.CreadorDeEspadachines;
import jugadores.Equipo;
import org.junit.Test;

import colocables.unidades.Espadachin;
import colocables.unidades.Poblacion;
import colocables.unidades.Unidad;
import colocables.unidades.UnidadDeAtaque;

public class CreadorDeEspadachinTest {

    @Test
    public void test01CrearUnEspadachin(){
        CreadorDeEspadachines creadorDeEspadachines = new CreadorDeEspadachines();
        Equipo equipo = new Equipo();
        equipo.agregarPoblacion(new Poblacion(50));
        creadorDeEspadachines.asignarEquipo(equipo);
        Unidad unEspadachin = creadorDeEspadachines.crearUnidad();
        assert (unEspadachin instanceof Espadachin);
    }


}
